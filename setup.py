# -*- coding: utf-8 -*-
"""
This module contains the tool of communities.practice
"""
import os
from setuptools import setup, find_packages


def read(*rnames):
    return open(os.path.join(os.path.dirname(__file__), *rnames)).read()

version = '4.3'

long_description = (
    read('README.txt')
    + '\n' +
    'Change history\n'
    '**************\n'
    + '\n' +
    read('docs', 'CHANGES.txt')
    + '\n' +
    'Contributors\n'
    '************\n'
    + '\n' +
    read('docs', 'CONTRIBUTORS.txt'))

setup(name='communities.practice',
      version=version,
      description="The Communities.practice product provides a generic platform for building virtual communities of practice for social learning applications through a set of communication and collaboration tools integrated into a common environment for sharing knowledge.",
      long_description=long_description,
      classifiers=[
        'Framework :: Plone',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: GNU General Public License v2 (GPLv2)',
        ],
      keywords='Plataform, Digital Habitat, Communities of Practice, Virtual Communities, Framework, Educacional Plataform , Learning Platform',
      author='Luciano Camargo Cruz',
      author_email='luciano@lccruz.net',
      url='https://bitbucket.org/communitas/communities.practice',
      license='GPLv2',
      packages=find_packages(exclude=['ez_setup']),
      namespace_packages=['communities', ],
      include_package_data=True,
      zip_safe=False,
      install_requires=[
          'setuptools',
          'Products.Ploneboard>=3.6dev_cop',
          'five.pt',
          'Products.Archetypes == 1.10.2',
          'plone.app.form == 2.3.0',
          ],
      extras_require={
          'test': ['plone.app.testing'],
      },
      entry_points="""
      # -*- entry_points -*-
      [z3c.autoinclude.plugin]
      target = plone
      """,
      )
