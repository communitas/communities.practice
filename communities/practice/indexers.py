# -*- coding: utf-8 -*-
from plone.indexer.decorator import indexer
from communities.practice.interfaces import ICoP
from communities.practice.interfaces import ICoPContent
from communities.practice.interfaces import ICoPShare
from communities.practice.generics.generics import getSubCoPDepth
from plone.app.discussion.interfaces import IConversation
from DateTime import DateTime

def subCoPDepth(object, **kw):
    """ Retorna nivel da CoP/SubCoP
    """
    cop_context = object
    depth = getSubCoPDepth(cop_context)
    return depth

@indexer(ICoP)
def subCoPDepthCoP(object, **kw):
    return subCoPDepth(object, **kw)

@indexer(ICoPContent)
def subCoPDepthContent(object, **kw):
    return subCoPDepth(object, **kw)

@indexer(ICoPShare)
def subCoPDepthShare(object, **kw):
    return subCoPDepth(object, **kw)

def copLastModification(object, **kw):
    modification_date = object.modification_date
    conversation = IConversation(object)
    last_comment_date = conversation.last_comment_date
    if last_comment_date:
        zope_time = DateTime(last_comment_date.isoformat())
        last_comment_date = zope_time.toZone(zope_time.localZone())
        modification_date = max(modification_date, last_comment_date)
    return modification_date.utcdatetime()

@indexer(ICoPContent)
def copLastModificationContent(object, **kw):
    return copLastModification(object, **kw)

@indexer(ICoPShare)
def copLastModificationShare(object, **kw):
    return copLastModification(object, **kw)
