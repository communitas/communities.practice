# -*- coding: utf-8 -*-
"""Common configuration constants
"""

PROJECTNAME = 'communities.practice'

ADD_PERMISSIONS = {
    'CoPCommunityFolder': 'communities.practice: Add CoPCommunityFolder',
    'CoP': 'communities.practice: Add CoP',
    'CoPMenu': 'communities.practice: Add CoPMenu',
    'CoPFolder': 'communities.practice: Add CoPFolder',
    'CoPDocument': 'communities.practice: Add CoPDocument',
    'CoPFile': 'communities.practice: Add CoPFile',
    'CoPLink': 'communities.practice: Add CoPLink',
    'CoPShare': 'communities.practice: Add CoPShare',
    'CoPEvent': 'communities.practice: Add CoPEvent',
    'CoPImage': 'communities.practice: Add CoPImage',
    'CoPPortfolio': 'communities.practice: Add CoPPortfolio',
    'CoPUpload': 'communities.practice: Add CoPUpload',
    'CoPATA': 'communities.practice: Add CoPATA',
    'CoPPost': 'communities.practice: Add CoPPost',
}

# Opcoes do input radio do Type ComunidadePratica
OPCOES = ['Habilitar', 'Desabilitar', 'Excluir']

# Nomes dos inputs do formulario de ComunidadePratica
INPUTS_FORM_IDS = [
    'acervo_input', 'calendario_input', 'forum_input',
    'portfolio_input', 'tarefas_input', 'subcop_input',
    'participar_input', 'gestao_input', 'portlet_input',
    'moderar_input',
]

USE_MEMCACHED = False
