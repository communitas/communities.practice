from Products.CMFCore.utils import getToolByName
PROFILE_ID = 'communities.practice:default'


def createIndexes(context):
    """Method to add our wanted indexes to the portal_catalog.

    http://maurits.vanrees.org/weblog/archive/2009/12/catalog
    """
    # Only run step if a flag file is present
    if context.readDataFile('communities.practice.marker'):
        site = context.getSite()

        logger = context.getLogger('communities.practice')

        catalog = getToolByName(site, 'portal_catalog')
        indexes = catalog.indexes()
        # Specify the indexes you want, with ('index_name', 'index_type')
        wanted = (('subCoPDepth', 'FieldIndex'),
                  ('copLastModification', 'DateIndex'),
                  ('timeline_listed', 'FieldIndex'),
                )
        indexables = []
        for name, meta_type in wanted:
            if name not in indexes:
                catalog.addIndex(name, meta_type)
                indexables.append(name)
                logger.info("Added %s for field %s.", meta_type, name)
        if len(indexables) > 0:
            logger.info("Indexing new indexes %s.", ', '.join(indexables))
            catalog.manage_reindexIndex(ids=indexables)
    return
