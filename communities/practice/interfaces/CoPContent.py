# -*- coding: utf-8 -*-

from zope.interface import Interface

class ICoPContent(Interface):
    """ Marker base interface for cop content
        [CoPAta, CoPDocument, CoPEvent, CoPFile, CoPImage, CoPLink]
    """
