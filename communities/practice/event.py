# -*- coding: utf-8 -*-
from zope.component.interfaces import ObjectEvent
from zope.interface import implements

from communities.practice.interfaces import ICoPLocalRolesChangedEvent
from communities.practice.interfaces import ICoPMasterRolesChangedEvent

class CoPLocalRolesChangedEvent(ObjectEvent):
    implements(ICoPLocalRolesChangedEvent)

    def __init__(self, context, member_id, role=None):
        super(CoPLocalRolesChangedEvent, self).__init__(context)
        self.member_id = member_id
        self.role = role

class CoPMasterRolesChangedEvent(ObjectEvent):
    implements(ICoPMasterRolesChangedEvent)

    def __init__(self, context, member_id, role=None):
        super(CoPMasterRolesChangedEvent, self).__init__(context)
        self.member_id = member_id
        self.role = role
