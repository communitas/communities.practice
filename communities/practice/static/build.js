({
    baseUrl: "javascripts",
    name: "main",
    paths: {
        'requireLib': "require",
        'bootstrap': 'bootstrap',
        'jquery': 'jquery',
    },
    out: "../browser/javascripts/coppla.js",
    optimize: "none",
    include: "requireLib",
    map: {
        '*': { 'jquery': 'jquery-private' },
        'jquery-private': { 'jquery': 'jquery' },
    },
    shim: {
        'bootstrap/affix':      { deps: ['jquery'], exports: '$.fn.affix' },
        'bootstrap/dropdown':   { deps: ['jquery'], exports: '$.fn.dropdown' },
        'bootstrap/modal':      { deps: ['jquery'], exports: '$.fn.modal' },
        'bootstrap/tooltip':    { deps: ['jquery'], exports: '$.fn.tooltip' },
        'bootstrap/popover':    { deps: ['jquery'], exports: '$.fn.popover' },
    },
})
