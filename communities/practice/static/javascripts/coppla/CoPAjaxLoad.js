//Ajax go back render.
window.addEventListener("popstate", function(e) {
    if(e.state){
        var url = window.location.href;
        $(".cop-container").load(url + ' .cop-container>*');
    }
}, false);

function loadCoPContents(url){
    window.history.pushState({path:url}, '', url);
    $(".cop-contents").load(url + ' .cop-contents>*');
}

//FolderContents Pagination.
$(document).on("click", ".cop-contents .cop-file-pagination-static a", function(e){
    e.preventDefault();
    var url = $(this).attr('href');
    $('html,body').animate({scrollTop: $('.cop-contents').offset().top}, 100);
    loadCoPContents(url);
});

//FolderContents order.
$(document).on("click", ".cop-contents #file_table .cop-file-header a", function(e){
    e.preventDefault();
    var url = $(this).attr('href');
    window.history.pushState({path:url}, '', url);
    //$("#file_table").load(url + ' #file_table>*');
    $(".cop-container").load(url + ' .cop-container>*');
});

//Workflow transitions.
$(document).on("click", ".cop-contents .dropdown-workflow a", function(e){
    e.preventDefault();
    var response_url = $(this).attr("href");
    var context_url = window.location.href;
    var response = $("<div/>").load(response_url + ' .cop-portal-message', function(e){
        $(".cop-container").load(context_url + ' .cop-container>*', function(e){
            $('html,body').animate({scrollTop: $('.cop-container').offset().top}, 100);
            $(".cop-portal-message").replaceWith($(response).find('.cop-portal-message'));
        });
    });
});

//Tab user config
$(document).on("click", ".cop-contents #users ul li a", function(e){
    e.preventDefault();
    var url = $(this).attr("href");
    loadCoPContents(url);
});

//Save user config
$(document).on("click", ".cop-contents #users #update_roles", function(e){
    e.preventDefault();
    var form = $(this).closest("form[name=copsearchform]");
    var str = form.serialize();
    str += "&update_roles=Salvar";
    var newurl = window.location.protocol + "//" +
        window.location.host + window.location.pathname;
    var post_url = "";
    if (newurl.indexOf("viewCoPCommunityFolderConfig") > -1){
        post_url = "viewCoPCommunityFolderConfig"
    }
    $.post(post_url, str, function(data){
        window.history.pushState({path:newurl},'',newurl);
        $(".cop-contents").replaceWith($(data).find('.cop-contents'));
        $(".cop-portal-message").hide();
        $('html,body').animate({scrollTop: $('.cop-container').offset().top}, 100, function(){
            $(".cop-portal-message").show(200);
        });
    });
});
