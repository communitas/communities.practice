/*Notificações*/
$(document).ready( function(){
    $('.cop-message-members .portalMessage.error').hide();
});

$(document).on('click', '.cop-message-members #selectAll', function() {
    if(this.checked == true){
        $("input[type=checkbox]").each(function() {
            this.checked = true;
        });
    } else {
        $("input[type=checkbox]").each(function() {
            this.checked = false;
        });
    }
});
$(document).on('click', '.cop-message-members #submit_send_message_members', function(e){
    teste = false
    $("input[type=checkbox]").each(function() {
        if(this.checked == true)
            teste = true;
    })

    if($('#assunto').val().length == 0 || $('#mensagem').val().length == 0 || teste == false){
        e.preventDefault();
        $('.cop-contents .portalMessage.info').hide();
        $('.cop-message-members .portalMessage.error').show();
        $('html,body').animate({scrollTop: $('.cop-contents').offset().top});
    }
    else {
        $('.cop-message-members .portalMessage.error').hide();
    }
});
