+function ($) {
    $(document).on('hide.bs.modal', '.modal', function() {
       $(this).removeData('bs.modal');
    }); 
    //popover initialization.
    //due to plone js incompatibilities,
    //call it manually using a newer js version: $(selector).popover('show');
    $('[data-toggle="popover"]').popover();
}(jQuery)
