//Fecha overlay ao clicar fora do contexto
$('html').click(function(event){
    if($(event.target).closest(".cop_timeline_popup").length <= 0){
        $(".cop-timeline-popaction div").hide(200, function(){
            this.remove();
            return 0;
        });
    }
});

//Carrega overlay comentario
$(document).on("click", ".cop-timeline-action-comment", function(e){
    e.preventDefault();
    if( $(this).find("div").length <= 0){
        $(".cop-timeline-popaction div").hide(200).delay(200).remove();
        $(this).append("<div class='cop_comment_popup cop_timeline_popup''></div>").find("div").show(200);
        var container = $(this).find("div");
        $("<div/>").load($(this).attr("href") + ' #commenting', function(){
            $(this).find("div").first().contents().appendTo(container);
            $('.cop_comment_popup form #form-buttons-cancel.hide').toggleClass('hide');
            return false;
        } );
    }
    return false;
});

//Cria comentario e atualiza timeline
$(document).on("click", ".cop-timeline-action-comment fieldset #form-buttons-comment", function(e){
    e.preventDefault();
    var container = $(this).closest('.cop-timeline-item').find('.cop-timeline-comments')
    var current_url = $(this).closest('a.cop-timeline-news-action').attr('href')

    content_url = current_url + 'CoPComment';
    $(this).attr("disabled", "disabled");
    $($(this).closest('.formControls')).hide();
    form = $(this).closest("form");
    array = $(form[0]).serializeArray();
    params = {'form.buttons.comment' : "Comment"};
    $(array).each(function(){ params[this.name] = this.value })
    $.post(form[0].action, params, function(data){
        $(".cop-timeline-popaction div").hide(200).delay(200).remove();
        $('<div/>').load(content_url + ' .cop-timeline-comments', function(){
            container.replaceWith($(this));
            $(this).hide();
            $(this).fadeIn(500);
            $(".cop-container").removeClass("wait");
        })
    })
});

//Carrega overlay para actions da timeline (Categorizar)
$(document).on("click", ".cop-timeline-popaction", function(e){
    e.preventDefault();
    if($(this).hasClass("cop-timeline-action-comment")) return;
    if( $(this).find("div").length <= 0){
        $(".cop-timeline-popaction div").hide(200).delay(200).remove();
        $(this).append("<div class='cop_timeline_popup'></div>").find("div").show(200);
        var container = $(this).find("div");
        $("<div/>").load( $(this).attr("href"), function(){
            $(this).find("div").first().contents().appendTo(container);
        } );
    }
    return 0;
});

//Categorização do conteúdo
$(document).on("click", ".cop-timeline-popaction form[name='edit_form'] [name='form.button.save']", function(e){
    e.preventDefault();
    var form = $(this).closest("form");
    var action_url = form.attr("action");
    $(this).attr("disabled", "disabled");
    form.attr( "enctype", "multipart/form-data" ).attr( "encoding", "multipart/form-data" );
    str = form.serialize();
    pop_parent = $(this).closest(".cop-timeline-popaction").find("div").first();
    $.post(action_url,
            str,
            function(data){
              pop_parent.hide(200, function(){ pop_parent.remove()});
            })
    return false;
});

//Fecha overlay ao clicar em cancelar
$(document).on("click", ".cop-timeline-popaction form[name='edit_form'] [name='form.button.cancel'], .cop-timeline-popaction [name='form.buttons.cancel']", function(e){
    e.preventDefault();
    pop_parent = $(this).closest(".cop-timeline-popaction").find("div").first();
    pop_parent.hide(200, function(){ pop_parent.remove()});
});
