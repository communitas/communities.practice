$(document).on('click', '#cop-load-container a.cop-load-button', function(e){
    e.preventDefault();
    var container = $(this).closest('div#cop-load-container').find("#cop-load-content");
    container.attr('id', 'content');
    content_url = $(this).attr('href') + ' #content form[name="edit_form"]';
    button = $(this);
    container.load(content_url, function(){
        initializePloneScripts();
        ploneFormTabbing.initialize();
        transformTabToDropdown();
        addIconCoPTools();
        CopClassColor();
        button.hide(200);
        container.show(200);
        $('#subject').multiSelect(); //initialize multiselection tag
        input_title = $("#cop-load-content input[name=title]");
        input_title.focus();
        input_title.val(input_title.val()); //place cursor at end of text
    });
});
