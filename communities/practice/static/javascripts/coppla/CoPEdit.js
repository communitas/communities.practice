$(document).on("click", "form[name=edit_form] div.btn-group.dropdown a", function(e){
    $($("form[name=edit_form] .btn-group.dropdown button span").first()).text(
        $(e.currentTarget).find("span").text()
    )
});

function transformTabToDropdown(){
    if($("ul.formTabs").length > 0){
        $("form[name=edit_form]").prepend(
            "<div class='btn-group dropdown dropdown-edit'><button class='cop-dropdown-button-small' role='button' data-toggle='dropdown'><span>"
            + $($("ul.formTabs a span").first()).text() +
            "</span><span class='caret'></span></button></div>")
        $("form[name=edit_form] div.btn-group.dropdown").append($("ul.formTabs"));
        $("form[name=edit_form] div.btn-group.dropdown .formTabs").addClass("dropdown-menu dropdown-editForm");
        $("form[name=edit_form] div.btn-group.dropdown .formTabs").attr("role", "menu");
        $("form[name=edit_form] div.btn-group.dropdown .formTabs").removeClass("formTabs");
        $("form[name=edit_form] div.btn-group.dropdown .formTab").removeClass("formTab");
    }
}


function addIconCoPTools(){
    if ($("#archetypes-fieldname-subcop_input").length){
        $("#archetypes-fieldname-subcop_input").before("<label class='formQuestion' id='cop-config-tools'>Ferramentas<span class='formHelp'>Ferramentas disponíveis na comunidade.</span></label>");
        $("#subcop_input").prepend("<span class='icon cop-icon cop-icon-subcop'></span>");
        $("#acervo_input").prepend("<span class='icon cop-icon cop-icon-acervo'></span>");
        $("#calendario_input").prepend("<span class='icon cop-icon cop-icon-calendario'></span>");
        $("#forum_input").prepend("<span class='icon cop-icon cop-icon-forum'></span>");
        $("#portfolio_input").prepend("<span class='icon cop-icon cop-icon-portfolio'></span>");
        $("#tarefas_input").prepend("<span class='icon cop-icon cop-icon-tarefas'></span>");
        $('#archetypes-fieldname-subcop_input, #archetypes-fieldname-acervo_input, #archetypes-fieldname-calendario_input, #archetypes-fieldname-forum_input, #archetypes-fieldname-portfolio_input, #archetypes-fieldname-tarefas_input').append("<input type='checkbox' />").each(function(){
            if( $(this).find("input[type='radio']").first().attr("checked") ){
                $(this).find("input[type='checkbox']").attr("checked", true);
            }
        });
    }
}


$(document).on("click",
    "#archetypes-fieldname-subcop_input input[type='checkbox'], #archetypes-fieldname-acervo_input input[type='checkbox'], #archetypes-fieldname-calendario_input input[type='checkbox'], #archetypes-fieldname-forum_input input[type='checkbox'], #archetypes-fieldname-portfolio_input input[type='checkbox'], #archetypes-fieldname-tarefas_input input[type='checkbox']",
    function(e){
        check=$(this);
        changeCoPToolStatus(check);
    }
);

$(document).on(
    'click',
    '#archetypes-fieldname-subcop_input, #archetypes-fieldname-acervo_input, #archetypes-fieldname-calendario_input, #archetypes-fieldname-forum_input, #archetypes-fieldname-portfolio_input, #archetypes-fieldname-tarefas_input',
    function(e){
        check = $(this).find("input[type='checkbox']")
        changeCoPToolStatus(check);
});

function changeCoPToolStatus(check){
    if(check.attr("checked")) {
        check.attr("checked", false);
        check.closest("div").find("input[type='radio']").first().click();
    }
    else{
        check.attr("checked", true);
        check.closest("div").find("input[type='radio']").last().click();
    }
}

function initializePloneScripts(){
    //Load TinyMCE on RichEdit fields
    if(typeof window.initTinyMCE === 'function') {
        window.initTinyMCE(document)
    }
    else{
        $(".mce_editable").each(function(){
            id = $(this).attr("id");
            if(id){
                InitializedTinyMCEInstances[id] = undefined;
                var textBox = new TinyMCEConfig(id);
                textBox.init();
            }
        });
    }


    //Load reference field pop-up
    $.getScript("referencebrowser.js", function(data){
        eval(data.responseText);
    })

    //Load plone calendar pop-up
    $('.plone-jscalendar-popup').each(function() {
        var jqt = $(this),
        widget_id = this.id.replace('_popup', ''),
        year_start = $('#' + widget_id + '_yearStart').val(),
        year_end = $('#' + widget_id + '_yearEnd').val();
        plone.jscalendar.init();
        if (year_start && year_end) {
            jqt.css('cursor', 'pointer')
            .show()
            .click(function(e) {
            return plone.jscalendar.show('#' + widget_id, year_start, year_end);
            });
        }
    });
}
