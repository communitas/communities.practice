$(document).on("click", ".cop-menu-column .cop-arrow-menu-mobile", function(e){
    $(this).closest(".cop-menu-column").toggleClass('cop-menu-open');
});

$(window).load(function(){
    var cop_menu = $('.cop-menu-full');
    var menu_top_value = 0;
    if (cop_menu.length){
        var menu_bottom_screen = cop_menu.offset().top + cop_menu.height() - $(window).scrollTop();
        if (menu_bottom_screen < 0){
            var menu_height = cop_menu.height();
            var menu_max_position = $('.cop-contents').outerHeight() - menu_height;
            var offset = $(window).scrollTop() - menu_height;
            if (offset > menu_max_position) offset = menu_max_position;
            cop_menu.animate({top: offset + "px"},{duration:200,queue:false});
            menu_top_value = offset;
        }
    }
    menuSlider(menu_top_value);
});

function menuSlider(menu_top_value) {
    var lastScrollTop = 0;
    var menu_top_value = 0;
    $(window).scroll(function(e){
        var cop_menu = $('.cop-menu-full');
        if(cop_menu.length){
            var menu_height = cop_menu.height();
            if (menu_height > $('.cop-contents').height()){
                cop_menu.css('top', 0);
            }
            var screen_height = window.innerHeight;
            var menu_max_position = $('.cop-contents').outerHeight() - menu_height;
            var menu_top_screen = cop_menu.offset().top - $(window).scrollTop();
            var menu_bottom_screen = cop_menu.offset().top + cop_menu.height() - $(window).scrollTop();
            var screen_bottom = window.innerHeight;
            var st = $(this).scrollTop();
            var diff = st - lastScrollTop;
            if (st > lastScrollTop){ //scroll down
                if(menu_bottom_screen + diff < screen_bottom && menu_height > screen_height){ //menu larger than screen
                    menu_top_value += diff;
                    if (menu_top_value > menu_max_position) menu_top_value = menu_max_position;
                }
                else if(menu_top_screen < 0 && menu_height < screen_height){ //menu smaller than screen
                    menu_top_value += diff;
                    if (menu_top_value > menu_max_position) menu_top_value = menu_max_position;
                }
            }
            else{ //scroll up
                if (menu_top_screen + diff > 0 && menu_height > screen_height){ //menu larger than screen
                    menu_top_value += diff;
                    if (menu_top_value < 0) menu_top_value = 0;
                }
                else if(menu_top_value > 0 && menu_top_screen > 0  && menu_top_screen + diff > 0
                        && menu_height < screen_height){ //menu smaller than screen
                    menu_top_value += diff;
                    if (menu_top_value < 0) menu_top_value = 0;
                }
            }
            //cop_menu.css('top', menu_top_value);

            if ($(window).scrollTop() < $('.cop-contents').offset().top ){
                //cop_menu.css('top', 0);
                //cop_menu.animate({top: menu_top_value + "px"},{duration:200,queue:false});
                lastScrollTop = 0;
                menu_top_value = 0;
            }
            cop_menu.animate({top: menu_top_value + "px"},{duration:200,queue:false});
        }
        lastScrollTop = st;
    });
}
