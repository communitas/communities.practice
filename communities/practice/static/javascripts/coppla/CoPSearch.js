$(document).on("click", ".cop-searchbox li a", function(e){
    e.preventDefault();
    var selected_text = $(this).text();
    var span_title = $(this).closest('.cop-input-group-btn').find(
      '.cop-selected-text'
    );
    span_title.html(selected_text);
    filter_term = this.getAttribute('id');
    var filter_term_input = $(this).closest('.cop-input-group-btn').find(
        'input[name=filter_term]'
    );
    filter_term_input.val(filter_term);

    str = getSearchString($(this));
    var post_url = window.location.protocol + "//" +
        window.location.host + window.location.pathname;
    var new_url = post_url + '?' + str;
    $.post(post_url, str, function(data){
        window.history.pushState({path:new_url},'',new_url);
        $(".cop-contents").replaceWith($(data).find('.cop-contents'));
        initializePloneScripts()
    });
});

$(document).on("submit", "form[name=copsearchform]", function(e){
    e.preventDefault();
    var str = getSearchString($(this));
    var post_url = window.location.protocol + "//" +
        window.location.host + window.location.pathname;
    var new_url = post_url + '?' + str;
    $.post(post_url, str, function(data){
        window.history.pushState({path:new_url},'',new_url);
        $(".cop-contents").replaceWith($(data).find('.cop-contents'));
        //somente para pesquisa de usuários nas configurações
        if($('.cop-panel#users').length){
            $('html,body').animate({scrollTop: $('.cop-panel#users').offset().top}, 100);
        }
        initializePloneScripts()
    });
});

function getSearchString(context){
    if(context.closest(".cop-form-activity").length){
        return $(
            "form[name=copsearchform] [name=filter_term], " +
            "form[name=copsearchform] [name=SearchableText], " +
            "form[name=copsearchform] [name=start_date_copactivity], " +
            "form[name=copsearchform] [name=end_date_copactivity], "
        ).serialize();
    }
    else{
        return $("form[name=copsearchform]").serialize();
    }
}
