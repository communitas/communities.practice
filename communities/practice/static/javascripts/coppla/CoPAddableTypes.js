$(document).on('click', '.cop-addable-types .cop-addable-links a', function(e){
    e.preventDefault();
    add_button = $(this);
    if(!add_button.hasClass('cop-active')){
        var addable_component = add_button.closest('div .cop-addable-types');
        addable_component.find('a.cop-active').removeClass('cop-active')
        add_button.addClass('cop-active');
        var container = addable_component.find('#addable_content');
        container.hide();
        content_url = add_button.attr('href') + ' #content';
        container.load(content_url, function(){
            initializePloneScripts();
            ploneFormTabbing.initialize();
            addIconCoPTools();
            CopClassColor();
            transformTabToDropdown();
            $('#subject').multiSelect(); //initialize multiselection tag
            container.show(200);
            input_title = $("#addable_content input[name=title]")
            input_title.focus();
            input_title.val(input_title.val()); //place cursor at end of text
        });
    }
    else{
        add_button.closest('div .cop-addable-types').find('#addable_content').hide(100);
        add_button.removeClass('cop-active');
    }
});

$(document).on("click", ".cop-addable-types input[name='form.button.cancel']", function(e){
    e.preventDefault();
    form = $(this).closest("form[name=edit_form]");
    str = form.serialize();
    $.post("", str, function(data){
        $(this).closest('div .cop-addable-types').find('#addable_content').hide(100);
        $('html,body').animate({scrollTop: $('.cop-contents').offset().top}, 100);
        $(".cop-contents").replaceWith($(data).find('.cop-contents'));
    });
});

$(document).on("click", ".cop-addable-types#ata-creation input[name='form.button.save']", function(e){
  e.preventDefault();
  form = $(this).closest("form[name=edit_form]");
  str = form.serialize();
  str += '&CoPEvent=' + $('#ata-creation').attr('event_id')
  action = form.attr('action')
  $.post(action, str, function(data){
    $(this).closest('div .cop-addable-types').find('#addable_content').hide(100);
    $('html,body').animate({scrollTop: $('.cop-contents').offset().top}, 100);
    $(".cop-contents").replaceWith($(data).find('.cop-contents'));
  });
});
