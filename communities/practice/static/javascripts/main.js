define([
    "bootstrap/affix",
    "bootstrap/dropdown",
    "bootstrap/modal",
    "bootstrap/tooltip",
    "bootstrap/popover",
    "coppla/CoPEdit.js",
    "coppla/CoPPostForm.js",
    "coppla/CoPCommunities.js",
    "coppla/CoPTimeline.js",
    "coppla/CoPMessageMembers.js",
    "coppla/CoPSearch.js",
    "coppla/CoPModal.js",
    "coppla/CoPShare.js",
    "coppla/CoPAddableTypes.js",
    "coppla/CoPAjaxLoad.js",
    "coppla/CoPConfig.js",
    "coppla/CoPMenuMobile",
    "coppla/CoPDiscussion.js",
    "coppla/CoPColorSelect.js",
    "coppla/CoPjQuery.js",
], function($){
    // $(window).trigger("load");
});
