#-*- coding: utf-8 -*-
import random
from zope.interface import implements

from Acquisition import aq_inner
from plone.memoize.instance import memoize
from plone.portlets.interfaces import IPortletDataProvider
from plone.app.form.widgets.uberselectionwidget import UberSelectionWidget
from plone.app.portlets.portlets import base
from plone.app.vocabularies.catalog import SearchableTextSourceBinder
from plone.memoize.compress import xhtml_compress
from plone.memoize import ram

from zope import schema
from zope.formlib import form
from Products.Five.browser.pagetemplatefile import ViewPageTemplateFile
from Products.CMFCore.utils import getToolByName
from Products.CMFPlone import PloneMessageFactory as _

from communities.practice.generics.generics import * 
from communities.practice.generics.cache import getCoPLocalRoles
from communities.practice.generics.cache import listMemberRoleCommunitiesParticipating
from communities.practice.interfaces import ICoPCommunityFolder
from Products.ATContentTypes.interface import IATBTreeFolder
from zope.component import getMultiAdapter
from time import time

def _cachekey_path(method, self):
    """Time, language, settings based cache
    XXX: If you need to publish private items you should probably
    include the member id in the cache key.
    """
    portal_state = getMultiAdapter((self.context, self.request),
        name=u'plone_portal_state')
    portal_url = portal_state.portal_url()
    return hash((portal_url, self.data,
                 time() // self.data.cache_reload))

def _cachekey_content(method, self, path):
    portal_state = getMultiAdapter((self.context, self.request),
        name=u'plone_portal_state')
    portal_url = portal_state.portal_url()
    return hash((portal_url, self.data, path,
                 time() // self.data.cache_reload))

class IPortletCoPNavigation(IPortletDataProvider):
    """A portlet

    It inherits from IPortletDataProvider because for this portlet, the
    data that is being rendered and the portlet assignment itself are the
    same.
    """

    some_field = schema.TextLine(title=_(u"Titulo"),
                                 description=_(u""),
                                 required=True,
                 )

    community_folder = schema.Choice(title=_(u"Selecione pasta de comunidades"),
                           description=_(u"Pasta onde será feita a busca por comunidades."),
                           required=True,
                           source=SearchableTextSourceBinder(
                               {'object_provides' : (ICoPCommunityFolder.__identifier__)},
                               default_query='path:'
                           )
                       )

    cache_reload = schema.Int(
                        title=_(u"Atualização do Cache"),
                        description=_(u"Tempo para atualização do cache em segundos."),
                        default = 600,
                        required = True,
                   )

class Assignment(base.Assignment):
    """Portlet assignment.

    This is what is actually managed through the portlets UI and associated
    with columns.
    """

    implements(IPortletCoPNavigation)

    some_field = u""
    community_folder = None
    cache_reload = 600

    def __init__(self, some_field=u"", community_folder=None, cache_reload=600):
        self.some_field = some_field
        self.community_folder = community_folder
        self.cache_reload = cache_reload

    @property
    def title(self):
        """This property is used to give the title of the portlet in the
        "manage portlets" screen.
        """
        return self.some_field


class Renderer(base.Renderer):
    """Portlet renderer.

    This is registered in configure.zcml. The referenced page template is
    rendered, and the implicit variable 'view' will refer to an instance
    of this class. Other methods can be added and referenced in the template.
    """

    _template = ViewPageTemplateFile('portletCoPNavigation.pt')
    recurse = ViewPageTemplateFile('recurseCoPNavigation.pt')

    def __init__(self, *args):
        base.Renderer.__init__(self, *args)
        self.comunidade = getCoPContext(self.context,['CoP',])

    def render(self):
        return xhtml_compress(self._template())

    def get_recurse_cop_navigation(self, path):
        return self.recurse(path = path)

    @ram.cache(_cachekey_content)
    def get_recursive_content(self, path):
        catalog = getToolByName(self.context, 'portal_catalog')
        query = {}
        query['path'] = {'query': path, 'depth': 1}
        query['portal_type'] = 'CoP'
        brain = catalog(query)
        return brain

    @ram.cache(_cachekey_path)
    def get_root_path(self):
        portal_state = getMultiAdapter((self.context, self.request),
                                        name=u'plone_portal_state')
        navigation_root_path = '%s%s' % (portal_state.navigation_root_path(),
                                         self.data.community_folder)
        return navigation_root_path

class AddForm(base.AddForm):
    """Portlet add form.

    This is registered in configure.zcml. The form_fields variable tells
    zope.formlib which fields to display. The create() method actually
    constructs the assignment that is being added.
    """
    form_fields = form.Fields(IPortletCoPNavigation)
    form_fields['community_folder'].custom_widget = UberSelectionWidget

    def create(self, data):
        return Assignment(**data)


class EditForm(base.EditForm):
    """Portlet edit form.

    This is registered with configure.zcml. The form_fields variable tells
    zope.formlib which fields to display.
    """
    form_fields = form.Fields(IPortletCoPNavigation)
    form_fields['community_folder'].custom_widget = UberSelectionWidget
