#-*- coding: utf-8 -*-
import random
from zope.interface import implements

from Acquisition import aq_inner
from plone.memoize.instance import memoize
from plone.portlets.interfaces import IPortletDataProvider
from plone.app.portlets.portlets import base
from plone.memoize.compress import xhtml_compress
from plone.memoize import ram

from zope import schema
from zope.formlib import form
from Products.Five.browser.pagetemplatefile import ViewPageTemplateFile
from Products.CMFCore.utils import getToolByName
from Products.CMFPlone import PloneMessageFactory as _

from communities.practice.generics.generics import *
from communities.practice.generics.cache import getCoPLocalRoles, listMemberRoleCommunitiesParticipating
from zope.component import getMultiAdapter
from time import time

def _cachekey(method, self):
    """Time, language, settings based cache
    XXX: If you need to publish private items you should probably
    include the member id in the cache key.
    """
    comunidade = getCoPContext(self.context,['CoP',])
    portal_state = getMultiAdapter((self.context, self.request),
        name=u'plone_portal_state')
    portal_url = portal_state.portal_url()
    return hash((portal_url, self.data, comunidade,
                 time() // self.data.cache_reload))

class IPortletCoPMembers(IPortletDataProvider):
    """A portlet

    It inherits from IPortletDataProvider because for this portlet, the
    data that is being rendered and the portlet assignment itself are the
    same.
    """

    some_field = schema.TextLine(title=_(u"Titulo"),
                                 description=_(u""),
                                 required=True)

    cache_reload = schema.Int(
                        title=_(u"Atualização do Cache"),
                        description=_(u"Tempo para atualização do cache em segundos."),
                        default = 600,
                        required = True,
                   )

class Assignment(base.Assignment):
    """Portlet assignment.

    This is what is actually managed through the portlets UI and associated
    with columns.
    """

    implements(IPortletCoPMembers)

    some_field = u""
    cache_reload = 600

    def __init__(self, some_field=u"", cache_reload=600):
        self.some_field = some_field
        self.cache_reload = cache_reload

    @property
    def title(self):
        """This property is used to give the title of the portlet in the
        "manage portlets" screen.
        """
        return self.some_field


class Renderer(base.Renderer):
    """Portlet renderer.

    This is registered in configure.zcml. The referenced page template is
    rendered, and the implicit variable 'view' will refer to an instance
    of this class. Other methods can be added and referenced in the template.
    """

    _template = ViewPageTemplateFile('portletCoPMembers.pt')

    def __init__(self, *args):
        base.Renderer.__init__(self, *args)
        self.comunidade = getCoPContext(self.context,['CoP',])

    def render(self):
        return xhtml_compress(self._template())

    @property
    def available(self):
        cop = getCoPContext(self.context, ['CoP'])
        allow_portlet = cop.get_portlet_habilitado()
        if allow_portlet:
            return True
        portal = self.context.portal_url.getPortalObject()
        user = portal.portal_membership.getAuthenticatedMember()
        user_id = user.getId()
        member_communities = listMemberRoleCommunitiesParticipating(
            user.getId()
        ).get(user_id)
        roles = ['Owner', 'Moderador', 'Participante', 'Observador']
        if member_communities.get(cop.UID()) in roles:
            return True
        if user_id in getMMGroupMembers(self.context) or user_id in getOMGroupMembers(self.context):
            return True
        return False

    @ram.cache(_cachekey)
    def get_participantes(self):
        """ Define lista de participantes
            retornando um dicionario
        """
        members = getCoPLocalRoles(self.comunidade, False)
        members = members.get(self.comunidade.UID())
        sample = random.sample(
            members["Participante"],
            min(len(members["Participante"]),20)
        )
        members_sample = [user[0] for user in sample]
        participantes = getMembersData(self.comunidade, members_sample)
        return participantes

    @ram.cache(_cachekey)
    def get_moderadores(self):
        """ Define lista de moderadores,
            retornando um dicionario
        """
        members = getCoPLocalRoles(self.comunidade, False)
        members = members.get(self.comunidade.UID())
        moderadores = getMembersData(self.comunidade, [user[0] for user in members["Moderador"]] )
        return moderadores

    @ram.cache(_cachekey)
    def get_total_participantes(self):
        """ Retorna o total de participantes
        """
        members = getCoPLocalRoles(self.comunidade, False)
        members = members.get(self.comunidade.UID())
        total_participantes = len(members["Participante"]) + len(members["Moderador"])
        return total_participantes

    def get_communitie_participating(self):
        cop = getCoPContext(self.context, ['CoP'])
        if cop.get_participar_habilitado():
            portal = self.context.portal_url.getPortalObject()
            user = portal.portal_membership.getAuthenticatedMember()
            member_communities = listMemberRoleCommunitiesParticipating(user.getId()).get(user.getId())
            if member_communities.get(self.comunidade.UID()):
                return True
        return False

    def get_search_parameter(self):
        return 'user_search'


class AddForm(base.AddForm):
    """Portlet add form.

    This is registered in configure.zcml. The form_fields variable tells
    zope.formlib which fields to display. The create() method actually
    constructs the assignment that is being added.
    """
    form_fields = form.Fields(IPortletCoPMembers)

    def create(self, data):
        return Assignment(**data)


class EditForm(base.EditForm):
    """Portlet edit form.

    This is registered with configure.zcml. The form_fields variable tells
    zope.formlib which fields to display.
    """
    form_fields = form.Fields(IPortletCoPMembers)
