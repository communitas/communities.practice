# -*- coding: utf-8 -*-

from communities.practice.tests.CoPBase import IntegrationTestCase

from StringIO import StringIO
from zptlogo import zptlogo

class CoPFileTestCase(IntegrationTestCase):

    def test_CoPFile_addable(self):
        self.factoryCoPFolder()
        
        stringio = StringIO(zptlogo)
        self.copfolder.invokeFactory('CoPFile', 'copfile',
                                     title="CoPFile Title",
                                     description="CoPFile Description",
                                     file=stringio,)

        copfile = self.copfolder.copfile
        self.assertEquals(copfile.Title(), "CoPFile Title")
        self.assertEquals(copfile.Description(), "CoPFile Description")
        self.assertEquals(copfile.getFile().data, stringio.getvalue())

    def test_CoPFile_addable_CoPPortfolio(self):
        self.factoryCoPPortfolio()

        stringio = StringIO(zptlogo)
        self.copportfolio.invokeFactory('CoPFile', 'copfile',
                                        title="CoPFile Title",
                                        description="CoPFile Description",
                                        file=stringio,)

        copfile = self.copportfolio.copfile
        self.assertEquals(copfile.Title(), "CoPFile Title")
        self.assertEquals(copfile.Description(), "CoPFile Description")
        self.assertEquals(copfile.getFile().data, stringio.getvalue())

    def test_CoPFile_addable_CoPUpload(self):
        self.factoryCoPUpload()

        stringio = StringIO(zptlogo)
        self.copupload.invokeFactory('CoPFile', 'copfile',
                                     title="CoPFile Title",
                                     description="CoPFile Description",
                                     file=stringio,)

        copfile = self.copupload.copfile
        self.assertEquals(copfile.Title(), "CoPFile Title")
        self.assertEquals(copfile.Description(), "CoPFile Description")
        self.assertEquals(copfile.getFile().data, stringio.getvalue())
