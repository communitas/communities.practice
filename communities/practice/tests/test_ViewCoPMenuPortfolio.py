# -*- coding: utf-8 -*-
from plone.app.testing import TEST_USER_ID
from communities.practice.tests.CoPBase import IntegrationTestCase


class ViewCoPMenuPortfolioTestCase(IntegrationTestCase):

    def setUp(self):
        super(ViewCoPMenuPortfolioTestCase, self).setUp()
        self.factoryCoPPortfolio()
        self.view = self.getView(
            self.copfolderportfolio, 'viewCoPMenuPortfolio'
        )

    def test_get_authenticated_user_portfolio(self):
        self.assertTrue(self.view.get_authenticated_user_portfolio())
        self.createUsers(['member'])
        self.setRoleInCoP(self.cop, 'member', 'Participante')
        self.changeLoggedMember('member')
        self.assertFalse(self.view.get_authenticated_user_portfolio())

    def test_create_portfolio(self):
        self.createUsers(['member'])
        self.setRoleInCoP(self.cop, 'member', 'Participante')
        self.changeLoggedMember('member')
        self.assertFalse(self.view.create_portfolio())
        self.request.form = {
            'submit_criar_portfolio': 'Criar meu portfolio na Comunidade'
        }
        url = self.view.create_portfolio()
        expected_url = self.view.get_authenticated_user_portfolio().getURL()
        self.assertEqual(url, expected_url)

    def test_unlock_portfolio(self):
        self.setMembersData([TEST_USER_ID])
        self.assertFalse(self.view.unlock_portfolio())
        portfolio = self.copportfolio
        self.workflow_tool.doActionFor(portfolio, 'privado')
        self.workflow_tool.doActionFor(portfolio, 'bloqueado')
        review_state = self.workflow_tool.getInfoFor(
            portfolio, 'review_state'
        )
        self.assertEqual(review_state, 'bloqueado')
        self.request.form = {
            'submit_desbloquear_portfolio': 'Desbloquear portfolio'
        }
        self.assertEqual(
            self.view.unlock_portfolio(),
            self.copfolderportfolio.absolute_url()
        )
        review_state = self.workflow_tool.getInfoFor(
            portfolio, 'review_state'
        )
        self.assertEqual(review_state, 'aguardando')
        self.view.request.response.setStatus('400')
        messages = self.view.context.plone_utils.showPortalMessages()
        self.assertEqual(
            messages[0].message, u'Solicitação de desbloqueio efetuada.'
        )

    def test_get_mail_list(self):
        self.assertFalse(self.view.get_mail_list())
        self.request.form = {
            'submit_criar_portfolio': 'Criar meu portfolio na Comunidade'
        }
        self.setMembersData(
            [self.portal.portal_membership.getAuthenticatedMember().getId()]
        )
        self.assertEqual(
            self.view.get_mail_list(),
            ['test@test.com.br']
        )

    def test_send_mail(self):
        self.view.send_mail()

    def test_ViewCoPMenuPortfolio_is_participant(self):
        self.assertTrue(self.view.is_participant())
        self.changeLoggedMember()
        self.assertFalse(self.view.is_participant())
        self.createUsers(['member'])
        self.changeLoggedMember('member')
        self.assertFalse(self.view.is_participant())
        self.setRoleInCoP(self.cop, 'member', 'Participante')
        self.assertTrue(self.view.is_participant())
