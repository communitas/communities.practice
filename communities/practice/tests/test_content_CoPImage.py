# -*- coding: utf-8 -*-

from communities.practice.tests.CoPBase import IntegrationTestCase
from StringIO import StringIO
from zptlogo import zptlogo

class CoPImageTestCase(IntegrationTestCase):

    def test_CoPImage_addable(self):
        self.factoryCoPFolder()
        
        stringio = StringIO(zptlogo)
        self.copfolder.invokeFactory('CoPImage', 'copimage',
                                     title="CoPImage Title",
                                     description="CoPImage Description",
                                     image=stringio,)

        copimage = self.copfolder.copimage
        self.assertEquals(copimage.Title(), "CoPImage Title")
        self.assertEquals(copimage.Description(), "CoPImage Description")
        self.assertEquals(copimage.getImage().data, stringio.getvalue())

    def test_CoPImage_addable_CoPPortfolio(self):
        self.factoryCoPPortfolio()
        
        stringio = StringIO(zptlogo)
        self.copportfolio.invokeFactory('CoPImage', 'copimage',
                                        title="CoPImage Title",
                                        description="CoPImage Description",
                                        image=stringio,)

        copimage = self.copportfolio.copimage
        self.assertEquals(copimage.Title(), "CoPImage Title")
        self.assertEquals(copimage.Description(), "CoPImage Description")
        self.assertEquals(copimage.getImage().data, stringio.getvalue())
