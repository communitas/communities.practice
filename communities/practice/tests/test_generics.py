# -*- coding: utf-8 -*-
from communities.practice.tests.CoPBase import IntegrationTestCase
from communities.practice.generics.generics import *
from communities.practice.browser.ViewCoPBase import ViewCoPBase
from communities.practice.browser.ViewCoPCommunityFolder import ViewCoPCommunityFolder
from communities.practice.browser.ViewCoP import ViewCoP
from communities.practice.config import OPCOES
from communities.practice.tests.CoPUtils import installPseudoProduct
from communities.practice.subscribers import setCoPGroup
from communities.practice.subscribers import createCoPGroup
from communities.practice.generics.cache import updateCommunityCache
from StringIO import StringIO
from zptlogo import zptlogo
from AccessControl import Unauthorized

from plone.app.testing import TEST_USER_ID
from plone.app.testing import TEST_USER_NAME
from plone.app.testing import login, logout

from zope.component import queryUtility
from plone.registry.interfaces import IRegistry
from plone.app.discussion.interfaces import IDiscussionSettings

from zope.component import createObject
from plone.app.discussion.interfaces import IConversation
from communities.practice.generics.cache import cleanMemberCache
from communities.practice.generics.cache import listMemberRoleCommunitiesParticipating
from communities.practice.subscribers import initialCreatedContents
from communities.practice.subscribers import checkCanDelete


class GenericsTestCase(IntegrationTestCase):

    def test_generics_getCoPContext(self):
        self.factoryCoPLink()
        stringio = StringIO(zptlogo)
        self.cop.setImagem(stringio)
        self.copcommunityfolder.setImagem(stringio)
        viewLink = ViewCoPBase(self.coplink, self.request)
        community = getCoPContext(viewLink.context)
        viewCommunityFolder = ViewCoPCommunityFolder(self.copcommunityfolder, self.request)
        viewCoP = ViewCoP(self.cop, self.request)
        community_folder = getCoPContext(viewCommunityFolder.context)
        self.assertEqual(community.Type(), u'Comunidade')
        self.assertEqual(community_folder.Type(),u'FolderComunidades')
        viewLink = ViewCoPBase(self.coplink, self.request)
        community = getCoPContext(viewLink.context, ['CoP',])
        self.assertEqual(community.Type(), u'Comunidade')
        community_folder = getCoPContext(viewLink.context, ['CoPCommunityFolder',])
        self.assertEqual(community_folder.Type(),u'FolderComunidades')
        viewLink = ViewCoPBase(self.coplink, self.request)
        self.assertFalse(getCoPContext(viewLink.context, ['Communitas',]))
        self.assertEqual(getCoPContext(viewLink.context, ['CoP',]).portal_type,'CoP')
        self.assertEqual(getCoPContext(viewLink.context, ['CoPCommunityFolder',]).portal_type,'CoPCommunityFolder')
        self.assertEqual(getCoPContext(viewLink.context, ['CoPMenu','CoP','CoPCommunityFolder']).portal_type,'CoPMenu')
        self.assertEqual(getCoPContext(viewCoP.context, ['CoPMenu','CoP','CoPCommunityFolder']).portal_type,'CoP')
        self.assertEqual(getCoPContext(viewCommunityFolder.context, ['CoPMenu','CoP','CoPCommunityFolder']).portal_type,'CoPCommunityFolder')

    def test_generics_getMemberData(self):
        self.factoryCoP()
        member_data = getMemberData(self.cop, self.cop.Creator())
        self.assertEqual(member_data[0], self.cop.Creator())
        self.assertFalse(getMemberData(self.cop, 'Communitas'))

    def test_generics_getMembersData(self):
        self.factoryCoP()
        user = self.portal.portal_membership.getMemberById(self.cop.Creator())
        user_author_url = "%s/author/%s" % (self.portal.absolute_url(), user.getId())
        member_data = getMembersData(self.cop, [user.getId(),])
        self.assertEqual(member_data[0].get('id'), user.getId())
        self.assertEqual(member_data[0].get('author_url'), user_author_url)
        self.assertEqual(member_data[0].get('img_url'), 'http://nohost/plone/defaultUser.png')
        self.assertFalse(getMembersData(self.cop, ['Communitas',]))

    def test_generics_encodeUTF8(self):
        self.assertEqual(encodeUTF8('João'), 'João')

    def test_generics_allowTypes(self):
        pass

    def test_generics_getCoPUpdates(self):
        stringio = StringIO(zptlogo)
        self.factoryCoPDocument()
        self.factoryCoPImage()
        self.factoryCoPEvent()
        self.factoryCoPLink()
        self.factoryCoPShare()
        self.copfolder.invokeFactory('CoPFile', 'copfile',
                                    title="",
                                    description="CoPFile Description",
                                    file=stringio,)
        self.copfile = self.copfolder.copfile
        self.cop.setImagem(stringio)
        self.copcommunityfolder.setImagem(stringio)
        member = self.portal.portal_membership.getAuthenticatedMember()
        member.setProperties(fullname=TEST_USER_NAME)

        viewCoP = self.getView(self.cop, 'viewCoP')
        objetos = dict(CoPLink= dict(titulo = encodeUTF8(self.coplink.Title()),
                                      descricao = encodeUTF8(self.coplink.Description()),
                                      UID = self.coplink.UID(),
                                      url = self.coplink.absolute_url(),
                                      tipo = self.coplink.portal_type,
                                      id_criador = self.coplink.Creator()))

        objetos['CoPEvent'] = dict(titulo = encodeUTF8(self.copevent.Title()),
                                   descricao = encodeUTF8(self.copevent.Description()),
                                   UID = self.copevent.UID(),
                                   url = self.copevent.absolute_url(),
                                   tipo = self.copevent.portal_type,
                                   id_criador = self.copevent.Creator())

        objetos['CoPImage'] = dict(titulo = encodeUTF8(self.copimage.Title()),
                                   descricao = encodeUTF8(self.copimage.Description()),
                                   UID = self.copimage.UID(),
                                   url = self.copimage.absolute_url(),
                                   tipo = self.copimage.portal_type,
                                   id_criador = self.copimage.Creator(),
                                   url_imagem = self.copimage.absolute_url())

        objetos['CoPDocument'] = dict(titulo = encodeUTF8(self.copdocument.Title()),
                                      descricao = encodeUTF8(self.copdocument.Description()),
                                      UID = self.copdocument.UID(),
                                      url = self.copdocument.absolute_url(),
                                      tipo = self.copdocument.portal_type,
                                      id_criador = self.copdocument.Creator(),
                                      nome_criador = TEST_USER_NAME,
                                      url_imagem = '',
                                      corpo_texto = encodeUTF8(self.copdocument.Description()))

        objetos['CoPFile'] = dict(titulo = encodeUTF8(self.copfile.getId()),
                                  descricao = encodeUTF8(self.copfile.Description()),
                                  UID = self.copfile.UID(),
                                  url = self.copfile.absolute_url(),
                                  tipo = self.copfile.portal_type,
                                  id_criador = self.copfile.Creator())

        objetos['CoPShare'] = dict(titulo = encodeUTF8(self.copshare.Title()),
                                   descricao = encodeUTF8(self.copshare.Description()),
                                   UID = self.copshare.UID(),
                                   url = self.copshare.absolute_url(),
                                   tipo = self.copshare.portal_type,
                                   id_criador = self.copshare.Creator())

        for obj in objetos.items():
            tipo = filter(lambda button: button['tipo'] == obj[0], viewCoP.get_brain_values(viewCoP.get_cop_search()))
            tipo = tipo[0]
            if tipo:
                if obj[0] == 'CoPImage':
                    self.assertEqual(tipo['titulo'], obj[1]['titulo'])
                    self.assertEqual(tipo['descricao'], obj[1]['descricao'])
                    self.assertEqual(tipo['UID'], obj[1]['UID'])
                    self.assertEqual(tipo['url'], obj[1]['url'])
                    self.assertEqual(tipo['tipo'], obj[1]['tipo'])
                    self.assertEqual(tipo['id_criador'], obj[1]['id_criador'])
                    self.assertEqual(tipo['url_imagem'], obj[1]['url_imagem'])

                elif obj[0] == 'CopDocument':
                    self.assertEqual(tipo['titulo'], obj[1]['titulo'])
                    self.assertEqual(tipo['descricao'], obj[1]['descricao'])
                    self.assertEqual(tipo['UID'], obj[1]['UID'])
                    self.assertEqual(tipo['url'], obj[1]['url'])
                    self.assertEqual(tipo['tipo'], obj[1]['tipo'])
                    self.assertEqual(tipo['id_criador'], obj[1]['id_criador'])
                    self.assertEqual(tipo['nome_criador'], TEST_USER_NAME)
                    self.assertEqual(tipo['url_imagem'], '')
                    self.assertEqual(tipo['corpo_texto'], obj[1]['corpo_texto'])

                else:
                    self.assertEqual(tipo['titulo'], obj[1]['titulo'])
                    self.assertEqual(tipo['descricao'], obj[1]['descricao'])
                    self.assertEqual(tipo['UID'], obj[1]['UID'])
                    self.assertEqual(tipo['url'], obj[1]['url'])
                    self.assertEqual(tipo['tipo'], obj[1]['tipo'])
                    self.assertEqual(tipo['id_criador'], obj[1]['id_criador'])
        self.assertEqual(len(getCoPUpdates(viewCoP.context)), 6)
        self.assertFalse(getCoPUpdates(viewCoP.context, timeline_listed=False))
        self.copfile.setTimeline_listed(False)
        self.copfile.reindexObject()
        self.assertEqual(len(getCoPUpdates(viewCoP.context)), 5)
        self.assertTrue(getCoPUpdates(viewCoP.context, timeline_listed=False))

    def test_generics_getCoPParticipantes(self):
        self.factoryCoP()
        self.portal.acl_users.userFolderAddUser('user2','user2',['Authenticated'],[])
        self.cop.manage_setLocalRoles('user2',['Participante'])
        updateCommunityCache(self.cop, 'user2', 'Participante')
        self.portal.acl_users.userFolderAddUser('user3','user3',['Authenticated'],[])
        self.cop.manage_setLocalRoles('user3',['Owner'])
        updateCommunityCache(self.cop, 'user3', 'Owner')
        self.portal.acl_users.userFolderAddUser('user4','user4',['Authenticated'],[])
        self.cop.manage_setLocalRoles('user4',['Moderador'])
        updateCommunityCache(self.cop, 'user4', 'Moderador')
        self.assertEqual(len(getCoPParticipants(self.cop)), 4)
        roles = getCoPRole(self.cop.UID(), 'user2')
        self.assertEqual(roles, 'Participante')
        roles = getCoPRole(self.cop.UID(), 'user3')
        self.assertEqual(roles, 'Owner')
        roles = getCoPRole(self.cop.UID(), 'user4')
        self.assertEqual(roles, 'Moderador')

    def test_generics_getLocalRole(self):
        self.factoryCoP()
        self.portal.acl_users.userFolderAddUser('user2','user2',['Authenticated'],[])
        self.copcommunityfolder.manage_setLocalRoles('user2',['Participante'])
        self.portal.acl_users.userFolderAddUser('user3','user3',['Authenticated'],[])
        self.copcommunityfolder.manage_setLocalRoles('user3',['Owner'])
        self.portal.acl_users.userFolderAddUser('user4','user4',['Authenticated'],[])
        self.copcommunityfolder.manage_setLocalRoles('user4',['Moderador'])
        self.assertEqual(len(getCoPParticipants(self.copcommunityfolder)), 4)
        roles = getLocalRole(self.copcommunityfolder, 'user2')
        self.assertEqual(roles, ('Participante',))
        roles = getLocalRole(self.copcommunityfolder, 'user3')
        self.assertEqual(roles, ('Owner',))
        roles = getLocalRole(self.copcommunityfolder, 'user4')
        self.assertEqual(roles, ('Moderador',))

    def test_generics_getUserByRole(self):
        self.factoryCoP()
        member = self.portal.portal_membership.getAuthenticatedMember()
        member.setProperties(email="test@test.com.br")
        self.cop.manage_setLocalRoles(member.getId(), ['Participante'])
        member_data = getMemberData(self.cop, member.getId())
        users = getUsersByRole(self.cop, ['Participante','Observador_Master',])
        participant = users['Participante'][0]
        self.assertEquals(participant['id'], member.getId())
        self.assertEquals(participant['full_name'], member_data[0])
        self.assertEquals(participant['email'], member_data[1])
        self.assertEquals(participant['img_url'], self.portal.portal_membership.getPersonalPortrait(member_data[0]).absolute_url())
        self.assertEquals(participant['author_url'], "%s/author/%s" % (self.portal.absolute_url(), member.getId()))

    def test_cleanhtml(self):
        self.assertEqual(cleanhtml("<p>COP</p>"),"COP")
        self.assertEqual(cleanhtml("<p><i>COP</i></p>"),"COP")

    def test_limit_description(self):
        description = limit_description(
            "A Comunidade de Prática é uma plataforma para a construção de comunidades virtuais de práticas" 
            "voltadas para aplicações de aprendizagem social. Oferece um conjunto de ferramentas de comunicação e colaboração"
            "integrados em um ambiente comum para compartilhar conhecimentos.", limit=190)
        self.assertEqual(description, "A Comunidade de Prática é uma plataforma para a construção de comunidades virtuais de práticas" 
                                      "voltadas para aplicações de aprendizagem social. Oferece um conjunto de ferramentas de\xe2\x80\xa6")

        self.assertEqual(limit_description(''), "") 

    def test_getCommunitiesParticipating(self):
        self.factoryCoP()
        portal_membership = getToolByName(self.cop, 'portal_membership')
        user = portal_membership.getAuthenticatedMember()
        communities = getCommunitiesParticipating(user, self.portal)
        self.assertEqual(len(communities), 1)
        self.copcommunityfolder.invokeFactory(  'CoP', 'cop2',
                                                title="CoP Title2",
                                                description="CoP Description2",
                                                subject=("CoPSubject2",),
                                                acervo_input=OPCOES[0],
                                                calendario_input=OPCOES[0],
                                                forum_input=OPCOES[0],
                                                portfolio_input=OPCOES[0],
                                                tarefas_input=OPCOES[1],
                                                exibir_todo_conteudo_input=OPCOES[0],)
        self.cop2 = self.copcommunityfolder.cop2
        communities = getCommunitiesParticipating(user, self.portal)
        self.assertEqual(len(communities), 2)
        self.cop2.manage_setLocalRoles(user.getId(),['Bloqueado'])
        communities = getCommunitiesParticipating(user, self.portal)
        self.assertEqual(len(communities), 1)
        self.cop2.manage_setLocalRoles(user.getId(),['Aguardando'])
        communities = getCommunitiesParticipating(user, self.portal)
        self.assertEqual(len(communities), 1)

    def test_getCommentaries(self):
        self.factoryCoP()
        self.factoryCoPEvent()
        registry = queryUtility(IRegistry)
        settings = registry.forInterface(IDiscussionSettings)
        settings.globally_enabled = True
        conversation = IConversation(self.copevent)
        comment = createObject('plone.Comment')
        comment.creator = TEST_USER_ID
        conversation.addComment(comment)
        comments = getCommentaries(self.cop, '/'.join(self.copevent.getPhysicalPath()))
        self.assertEqual(len(comments), 1)

    def test_isDiscussionAllowed(self):
        cleanMemberCache(TEST_USER_ID)
        self.factoryCoP()
        self.assertFalse(isDiscussionAllowed(self.cop, self.cop.UID()))
        registry = queryUtility(IRegistry)
        settings = registry.forInterface(IDiscussionSettings)
        settings.globally_enabled = True
        self.assertTrue(isDiscussionAllowed(self.cop, self.cop.UID()))

    def test_getProductInstalled(self):
        installPseudoProduct("cop.forms", "new")
        self.assertFalse(getProductInstalled("cop.forms"))
        installPseudoProduct("cop.forms", "installed")
        self.assertTrue(getProductInstalled("cop.forms"))
        installPseudoProduct("cop.forms", "new")

    def test_changeCoPOwnership(self):
        self.factoryCoPMenu()
        self.factorySubCoPMenu()
        self.portal.acl_users.userFolderAddUser('user2','user2',[],[])
        self.cop.manage_setLocalRoles('user2',['Moderador'])
        login(self.portal, 'user2')
        self.factorySubCoP()
        self.factoryCoPFolder()
        logout()
        login(self.portal, TEST_USER_NAME)

        self.assertEqual(self.copfolder.Creator(), 'user2')
        self.assertEqual(self.copfolder.getOwner().getId(), 'user2')
        self.assertEqual(self.subcop.Creator(), 'user2')
        self.assertEqual(self.subcop.getOwner().getId(), 'user2')

        changeCoPOwnership(self.cop, 'user2')
        self.assertEqual(self.copfolder.Creator(), TEST_USER_ID)
        self.assertEqual(self.copfolder.getOwner().getId(), TEST_USER_ID)
        self.assertEqual(self.subcop.Creator(), TEST_USER_ID)
        self.assertEqual(self.subcop.getOwner().getId(), TEST_USER_ID)

    def test_blockPortfolio(self):
        self.factoryCoP()
        self.factoryCoPMenu()
        self.factoryCoPFolderPortfolio()
        self.cop.manage_setLocalRoles(self.cop.Creator(), ["Owner", "Moderador"])
        self.portal.acl_users.userFolderAddUser('user2','user2',[],[])
        member = self.portal.portal_membership.getMemberById('user2')
        member.setProperties(email="test@test.com.br")
        self.cop.manage_setLocalRoles('user2',['Moderador'])
        login(self.portal, 'user2')
        self.factoryCoPDocument()
        self.factoryCoPPortfolio()
        logout()
        login(self.portal, TEST_USER_NAME)
        portfolio = self.cop.copfolderportfolio.copportfolio
        review_state = self.portal.portal_workflow.getInfoFor(portfolio, 'review_state')
        self.assertEqual(review_state, "restrito")
        blockPortfolio(self.cop, 'user2')
        review_state = self.portal.portal_workflow.getInfoFor(portfolio, 'review_state')
        self.assertEqual(review_state, "bloqueado")
        #teste para estado publico
        self.cop.manage_setLocalRoles('user2',['Moderador'])
        self.portal.portal_workflow.doActionFor(portfolio,'publico')
        review_state = self.portal.portal_workflow.getInfoFor(portfolio, 'review_state')
        self.assertEqual(review_state, "publico")
        blockPortfolio(self.cop, 'user2')
        review_state = self.portal.portal_workflow.getInfoFor(portfolio, 'review_state')
        self.assertEqual(review_state, "bloqueado")
        #teste para estado privado
        self.cop.manage_setLocalRoles('user2',['Moderador'])
        self.portal.portal_workflow.doActionFor(portfolio,'privado')
        review_state = self.portal.portal_workflow.getInfoFor(portfolio, 'review_state')
        self.assertEqual(review_state, "privado")
        blockPortfolio(self.cop, 'user2')
        review_state = self.portal.portal_workflow.getInfoFor(portfolio, 'review_state')
        self.assertEqual(review_state, "bloqueado")
        #teste para estado bloqueado
        review_state = self.portal.portal_workflow.getInfoFor(portfolio, 'review_state')
        blockPortfolio(self.cop, 'user2')
        review_state = self.portal.portal_workflow.getInfoFor(portfolio, 'review_state')
        self.assertEqual(review_state, "bloqueado")

    def test_isSuBCoP(self):
        self.factorySubCoP()
        self.assertTrue(isSubCoP(self.subcop))
        self.assertFalse(isSubCoP(self.cop))

    def test_getSubCoPDepth(self):
        self.factorySubCoP()
        self.assertEqual(getSubCoPDepth(self.cop), 0)
        self.assertEqual(getSubCoPDepth(self.subcop), 1)

    def test_hasSuperRole(self):
        self.factorySubCoP()
        self.assertTrue(hasSuperRole(self.subcop, TEST_USER_ID))
        self.portal.acl_users.userFolderAddUser('user2','user2',[],[])
        self.portal.acl_users.userFolderAddUser('user3','user3',[],[])
        self.subcop.manage_setLocalRoles('user2',['Participante'])
        self.assertFalse(hasSuperRole(self.subcop, 'user2'))
        self.cop.manage_setLocalRoles('user2',['Participante'])
        self.assertFalse(hasSuperRole(self.subcop, 'user2'))
        self.cop.manage_setLocalRoles('user2',['Moderador'])
        self.assertTrue(hasSuperRole(self.subcop, 'user2'))
        self.subcop.manage_setLocalRoles('user2',['Moderador'])
        self.assertTrue(hasSuperRole(self.subcop, 'user2'))
        self.cop.manage_setLocalRoles('user2',['Participante'])
        self.assertFalse(hasSuperRole(self.subcop, 'user2'))
        self.cop.manage_setLocalRoles('user3',['Observador'])
        self.assertTrue(hasSuperRole(self.subcop, 'user3'))
        self.cop.manage_setLocalRoles('user3',['Participante'])
        self.assertFalse(hasSuperRole(self.subcop, 'user3'))

    def test_setSuperCoPRoles(self):
        self.factorySubCoP()
        self.portal.acl_users.userFolderAddUser('user2','user2',[],[])
        self.cop.manage_setLocalRoles('user2',['Moderador'])
        roles = self.subcop.get_local_roles_for_userid('user2')
        self.assertFalse(roles)
        member_roles = listMemberRoleCommunitiesParticipating('user2')
        self.assertFalse(self.subcop.UID() in member_roles['user2'])

        setSuperCoPRoles(self.subcop)
        roles = self.subcop.get_local_roles_for_userid('user2')
        self.assertEqual(roles, ('Moderador',))
        member_roles = listMemberRoleCommunitiesParticipating('user2')
        self.assertEqual(member_roles['user2'][self.subcop.UID()], 'Moderador')

        self.cop.manage_delLocalRoles(['user2'])
        updateCommunityCache(self.cop, 'user2', None)
        roles = self.subcop.get_local_roles_for_userid('user2')
        self.assertEqual(roles, ('Moderador',))

        self.portal.acl_users.userFolderAddUser('user3','user3',[],[])
        self.cop.manage_setLocalRoles('user3',['Observador'])
        roles = self.subcop.get_local_roles_for_userid('user3')
        self.assertFalse(roles)
        setSuperCoPRoles(self.subcop)
        roles = self.subcop.get_local_roles_for_userid('user3')
        self.assertEqual(roles, ('Observador',))
        member_roles = listMemberRoleCommunitiesParticipating('user3')
        self.assertEqual(member_roles['user3'][self.subcop.UID()], 'Observador')
        self.cop.manage_delLocalRoles(['user3'])
        updateCommunityCache(self.cop, 'user3', None)
        roles = self.subcop.get_local_roles_for_userid('user3')
        self.assertEqual(roles, ('Observador',))

    def test_setMembersCoPRole(self):
        self.factorySubCoP()
        self.portal.acl_users.userFolderAddUser('user2','user2',[],[])
        self.portal.acl_users.userFolderAddUser('user3','user3',[],[])
        self.portal.acl_users.userFolderAddUser('user4','user4',[],[])
        setMembersCoPRole(self.cop, ['user2'], 'Moderador')
        setMembersCoPRole(self.cop, ['user3', 'user4'], 'Observador')
        roles = self.cop.get_local_roles_for_userid('user2')
        self.assertEqual(roles, ('Moderador',))
        roles = self.subcop.get_local_roles_for_userid('user2')
        self.assertEqual(roles, ('Moderador',))

        roles = self.cop.get_local_roles_for_userid('user3')
        self.assertEqual(roles, ('Observador',))
        roles = self.cop.get_local_roles_for_userid('user4')
        self.assertEqual(roles, ('Observador',))
        roles = self.subcop.get_local_roles_for_userid('user3')
        self.assertEqual(roles, ('Observador',))
        roles = self.subcop.get_local_roles_for_userid('user4')
        self.assertEqual(roles, ('Observador',))

    def test_getRootCoP(self):
        self.factorySubCoP()
        root_cop = getRootCoP(self.subcop)
        self.assertEqual(root_cop, self.cop)
        root_cop = getRootCoP(self.cop)
        self.assertEqual(root_cop, self.cop)
        root_cop = getRootCoP(self.copcommunityfolder)
        self.assertFalse(root_cop)

    def test_deleteCoPShareUser(self):
        """Testa remocao do tipo cop_share para usuarios retirados da CoP"""
        self.factorySubCoP()
        self.factoryCoPShare()
        self.subcop.invokeFactory('CoPMenu', 'copmenu',
            title="CoPMenu Title",
            description="CoPMenu Description",)

        self.subcop.copmenu.invokeFactory('CoPShare', 'copshare',
            title="CoPShare Title",
            description="CoPShare Description",
            remoteUrl="http://www.otics.org/otics",)

        cop_shares = self.portal.portal_catalog(portal_type='CoPShare',
                                               Creator=TEST_USER_ID)
        self.assertEqual(len(cop_shares), 2)

        #Remove usuario da subcop
        deleteCoPShareUser(self.subcop, TEST_USER_ID)
        cop_shares = self.portal.portal_catalog(portal_type='CoPShare',
                                               Creator=TEST_USER_ID)
        self.assertEqual(len(cop_shares), 1)

        self.subcop.copmenu.invokeFactory('CoPShare', 'copshare',
            title="CoPShare Title",
            description="CoPShare Description",
            remoteUrl="http://www.otics.org/otics",)

        cop_shares = self.portal.portal_catalog(portal_type='CoPShare',
                                               Creator=TEST_USER_ID)
        self.assertEqual(len(cop_shares), 2)

        #Remove usuario da cop
        deleteCoPShareUser(self.cop, TEST_USER_ID)
        cop_shares = self.portal.portal_catalog(portal_type='CoPShare',
                                               Creator=TEST_USER_ID)
        self.assertEqual(len(cop_shares), 0)

    def test_addParticipantsInBlock(self):
        self.portal.acl_users.userFolderAddUser('user2','user2',['Authenticated'],[])
        self.portal.acl_users.userFolderAddUser('user3','user3',['Authenticated'],[])
        self.factoryCoP()
        self.factorySubCoP()
        createCoPGroup(self.cop)
        setCoPGroup(self.cop)
        addParticipantsInBlock(self.subcop, ['user2', 'user3'])
        roles = getLocalRole(self.subcop, 'user2')
        self.assertEqual(roles, ('Participante',))
        roles = getLocalRole(self.subcop, 'user3')
        self.assertEqual(roles, ('Participante',))

    def test_generateContentId(self):
        self.factoryCoPEvent()
        self.assertEqual(generateContentId(self.cop, "test@test.com"), "test-test-com")
        self.assertEqual(generateContentId(self.cop, "copevent"), "copevent")
        self.assertEqual(generateContentId(self.cop.copmenu, "copevent"), "copevent2")

    def test_canDeleteCoPFolder(self):
        self.portal.acl_users.userFolderAddUser('user2','user2',['Authenticated'],[])
        self.portal.acl_users.userFolderAddUser('user3','user3',['Authenticated'],[])
        self.factoryCoP()
        initialCreatedContents(self.cop, False)
        self.cop.manage_setLocalRoles('user2',['Participante'])
        self.cop.manage_setLocalRoles('user3',['Participante'])
        login(self.portal, "user2")
        copfolder_user2 = self.cop.acervo.invokeFactory(
            "CoPFolder", 
            "copfolder_user2",
            title="CoPFolder user2",
        )
        copfolder_user2 = self.cop.acervo.copfolder_user2
        logout()
        login(self.portal, "user3")
        self.assertFalse(canDeleteCoPFolder(copfolder_user2))
        self.assertRaises(Unauthorized, checkCanDelete, self.cop.acervo.copfolder_user2, False)
        self.assertRaises(Unauthorized, self.cop.acervo.manage_delObjects, ["copfolder_user2"])
        logout()
        login(self.portal, "user2")
        self.assertTrue(canDeleteCoPFolder(copfolder_user2))
        try:
            checkCanDelete(self.cop.acervo.copfolder_user2, False)
            self.cop.acervo.manage_delObjects(["copfolder_user2"])
        except:
            self.fail("test_canDeleteCoPFolder failed")

    def test_exportCSVDownload(self):
        request = self.request
        header = ["A","B","C"]
        data = [
            ["testeA1","testeB1","testeC1"],
            ["testeA2","testeB2","testeC2"],
            ["testeA3","testeB3","testeC3"],
        ]
        filename = "export_test.csv"
        exportCSVDownload(request, header, data, filename)
        self.assertEqual(
            request.response.headers, 
            {'content-length': '0', 'Content-Type': 'text/csv', 'content-disposition': 'attachment; filename="export_test.csv"'}
        )
        self.assertEqual(request.response.status, 200)

    def test_createCoPMenuPost(self):
        self.factoryCoP()
        self.portal.acl_users.userFolderAddUser('user2','user2',['Authenticated'],[])
        logout()
        login(self.portal, 'user2')
        createCoPMenuPost(self.cop)
        self.assertTrue(self.cop.has_key('posts'))
        self.assertEqual(self.cop.posts.getOwner().getId(), TEST_USER_ID)
        self.assertEqual(self.cop.get_local_roles_for_userid(TEST_USER_ID),('Owner', 'Moderador'))
        self.assertFalse(self.cop.get_local_roles_for_userid('user2'))

    def test_canCutObjects(self):
        self.portal.acl_users.userFolderAddUser('user2','user2',['Authenticated'],[])
        self.portal.acl_users.userFolderAddUser('user3','user3',['Authenticated'],[])
        self.factoryCoP()
        initialCreatedContents(self.cop, False)
        self.cop.manage_setLocalRoles('user2',['Participante'])
        self.cop.manage_setLocalRoles('user3',['Participante'])
        login(self.portal, "user2")

        #tenta recortar no contexto de CoP, o que retorna True para que o Plone se vire
        self.assertTrue(canCutObjects(self.cop, 'test', self.portal.REQUEST))
        #tenta recortar algo que nao existe, retorna True para deixar com o Plone
        self.assertTrue(canCutObjects(self.cop.acervo, 'test_not_exist', self.portal.REQUEST))

        self.cop.acervo.invokeFactory(
            "CoPDocument", 
            "copdocument_user2",
            title="CoPDocument user2",
        )
        self.assertTrue(canCutObjects(self.cop.acervo, 'copdocument_user2', self.portal.REQUEST))

        self.cop.acervo.invokeFactory(
            "CoPFolder", 
            "copfolder_user2",
            title="CoPFolder user2",
        )
        copfolder_user2 = self.cop.acervo.copfolder_user2
        copfolder_user2.invokeFactory(
            "CoPDocument",
            "copdocument_user2",
            title="CoPDocument user2",
        )
        copfolder_user2.reindexObject()
        self.assertTrue(canCutObjects(copfolder_user2, 'copdocument_user2', self.portal.REQUEST))

        logout()
        login(self.portal, "user3")

        copfolder_user2.invokeFactory(
            "CoPDocument",
            "copdocument_user3",
            title="CoPDocument user3",
        )
        copfolder_user2.reindexObject()
        self.assertTrue(canCutObjects(copfolder_user2, "copdocument_user3", self.portal.REQUEST))
        self.assertFalse(canCutObjects(copfolder_user2, "copdocument_user2", self.portal.REQUEST))
        self.assertFalse(canCutObjects(self.cop.acervo, "copfolder_user2", self.portal.REQUEST))
        self.assertFalse(canCutObjects(self.cop.acervo, "copdocument_user2", self.portal.REQUEST))

        self.assertRaises(Unauthorized, self.cop.acervo.manage_cutObjects, ["copfolder_user2", self.portal.REQUEST])
        self.assertRaises(Unauthorized, self.cop.acervo.manage_cutObjects, ["copdocument_user2", self.portal.REQUEST])
        self.assertRaises(Unauthorized, copfolder_user2.manage_cutObjects, ["copdocument_user2", self.portal.REQUEST])

        #flag _p_jar precisa ser setada para que o objeto seja moveable
        copfolder_user2._p_jar = 1
        copfolder_user2.copdocument_user2._p_jar = 1
        copfolder_user2.copdocument_user3._p_jar = 1
        self.cop.acervo.copdocument_user2._p_jar = 1
        try:
            copfolder_user2.manage_cutObjects("copdocument_user3", self.portal.REQUEST)            
        except:
            self.fail("test_canCutObjects failed")
        logout()

        login(self.portal, "user2")
        try:
            self.cop.acervo.manage_cutObjects("copdocument_user2", self.portal.REQUEST)
            copfolder_user2.manage_cutObjects("copdocument_user2", self.portal.REQUEST)
            self.cop.acervo.manage_cutObjects("copfolder_user2", self.portal.REQUEST)
        except:
            self.fail("test_canCutObjects failed")
        logout()

    def test_getMMGroup(self):
        """
        """
        self.factoryCoP()
        self.assertTrue(getMMGroup(self.portal) is None)
        mm_group = getMMGroup(self.cop)
        expected_id = "MM%s" % self.copcommunityfolder.UID()
        self.assertEquals(expected_id, mm_group.getId())

    def test_getMMGroupMembers(self):
        """
        """
        self.factoryCoP()
        self.assertEquals(getMMGroupMembers(self.portal), [])
        setMasterRole(self.cop, TEST_USER_ID, "Moderador_Master")
        mm_members = getMMGroupMembers(self.cop)
        mm_group = getMMGroup(self.cop)
        self.assertItemsEqual(mm_members, mm_group.getGroupMemberIds())

    def test_getOMGroup(self):
        """
        """
        self.factoryCoP()
        self.assertTrue(getOMGroup(self.portal) is None)
        om_group = getOMGroup(self.cop)
        expected_id = "OM%s" % self.copcommunityfolder.UID()
        self.assertEquals(expected_id, om_group.getId())

    def test_getOMGroupMembers(self):
        """
        """
        self.factoryCoP()
        self.assertEquals(getOMGroupMembers(self.portal), [])
        setMasterRole(self.cop, TEST_USER_ID, "Observador_Master")
        om_members = getOMGroupMembers(self.cop)
        om_group = getOMGroup(self.cop)
        self.assertItemsEqual(om_members, om_group.getGroupMemberIds())

    def test_searchPortalUsers(self):
        """
        """
        self.factoryCoP()

        self.portal.acl_users.userFolderAddUser('user2','user2',['Authenticated'],[])
        self.portal.acl_users.userFolderAddUser('user3','user3',['Authenticated'],[])
        self.portal.acl_users.userFolderAddUser('user4','user4',['Authenticated'],[])
        self.portal.acl_users.userFolderAddUser('member5','member5',['Authenticated'],[])
        self.portal.acl_users.userFolderAddUser('member6','member6',['Authenticated'],[])

        user_list = searchPortalUsers(self.cop, self.request, "user")
        self.assertEqual(len(user_list), 4)

        user_list = searchPortalUsers(self.cop, self.request, "member")
        self.assertEqual(len(user_list), 2)

        user_list = searchPortalUsers(self.cop, self.request, "user", ["user2"])
        self.assertEqual(len(user_list), 3)

    def test_getCoPSettings(self):
        settings = getCoPSettings()
        self.assertEqual(settings.hostname, u'smtp.gmail.com')
        self.assertEqual(settings.port, 587)
        self.assertEqual(settings.username, u'testeplone@gmail.com')
        self.assertEqual(settings.password, u'testeplone1234')
        self.assertEqual(settings.force_tls, False)
