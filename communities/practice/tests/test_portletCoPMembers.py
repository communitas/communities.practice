# -*- coding: utf-8 -*-
from communities.practice.tests.CoPBase import IntegrationTestCase
from zope.component import getMultiAdapter, getUtility
from plone.portlets.interfaces import \
    IPortletManager, IPortletRenderer, IPortletType
from communities.practice.portlets import portletCoPMembers
from communities.practice.config import OPCOES


class portletCoPMembersTestCase(IntegrationTestCase):

    def setUp(self):
        super(portletCoPMembersTestCase, self).setUp()
        self.factoryCoP()
        self.view = self.getView(self.cop, 'viewCoP')
        self.manager = getUtility(
            IPortletManager, name='plone.rightcolumn', context=self.portal
        )
        self.assignment = portletCoPMembers.Assignment('Participantes')
        self.renderer = getMultiAdapter(
            (self.cop, self.request, self.view, self.manager, self.assignment),
            IPortletRenderer
        )

    def test_available(self):
        self.assertTrue(self.renderer.available)
        self.createUsers(['user1',])
        self.setMembersData(['user1',])
        self.changeLoggedMember('user1')
        self.assertFalse(self.renderer.available)
        self.cop.setPortlet_input(OPCOES[0])
        self.assertTrue(self.renderer.available)

    def test_invoke_add_view(self):
        portlet = getUtility(
            IPortletType,
            name='communities.practice.portlets.portletCoPMembers'
        )
        mapping = self.portal.restrictedTraverse(
            '++contextportlets++plone.rightcolumn'
        )
        for m in mapping.keys():
            del mapping[m]
        addview = mapping.restrictedTraverse('+/' + portlet.addview)
        addview.createAndAdd(data={})
        self.assertEqual(len(mapping), 1)
        self.assertTrue(
            isinstance(mapping.values()[0], portletCoPMembers.Assignment)
        )

    def test_portletCoPMembers_renderPortlet(self):
        """Render test.
        """
        self.createUsers(['user2', 'user3', 'user4'])
        self.setRoleInCoP(self.cop, 'user2', 'Participante')
        self.setRoleInCoP(self.cop, 'user3', 'Participante')
        self.setRoleInCoP(self.cop, 'user4', 'Moderador')
        self.assertEqual(len(self.renderer.get_moderadores()), 2)
        self.assertEqual(len(self.renderer.get_participantes()), 2)
        self.assertEqual(self.renderer.get_total_participantes(), 4)
        self.assertTrue(self.renderer.get_communitie_participating())
        self.assertEqual(self.assignment.title, 'Participantes')
        self.assertIsNotNone(self.renderer.render())
        self.changeLoggedMember('user2')
        self.assertTrue(self.renderer.get_communitie_participating())
        self.cop.participar_input = OPCOES[1]
        self.assertFalse(self.renderer.get_communitie_participating())

    def test_get_search_parameter(self):
        self.assertEqual(self.renderer.get_search_parameter(), 'user_search')
