# -*- coding: utf-8 -*-

from communities.practice.tests.CoPBase import IntegrationTestCase
from zope.component import getMultiAdapter, queryMultiAdapter
from StringIO import StringIO
from zptlogo import zptlogo
from plone.app.testing import TEST_USER_ID
from zope.component import queryUtility
from plone.registry.interfaces import IRegistry
from plone.app.discussion.interfaces import IDiscussionSettings
from zope.component import createObject
from plone.app.discussion.interfaces import IConversation
from communities.practice.generics.cache import updateCommunityCache

class ViewCoPCommentTestCase(IntegrationTestCase):

    def test_get_commentaries(self):
        self.factoryCoP()
        stringio = StringIO(zptlogo)
        self.cop.setImagem(stringio)
        self.factoryCoPEvent()
        updateCommunityCache(self.cop, TEST_USER_ID, 'Owner')
        registry = queryUtility(IRegistry)
        settings = registry.forInterface(IDiscussionSettings)
        view = getMultiAdapter((self.copevent, self.request), name='viewCoPComment')
        context_values = view.get_context_values()
        self.assertEqual(context_values['url'], self.copevent.absolute_url())
        self.assertEqual(context_values['path'], '/'.join(self.copevent.getPhysicalPath()))
        self.assertFalse(view.is_discussion_allowed(self.cop.UID()))
        settings.globally_enabled = True
        self.assertTrue(view.is_discussion_allowed(self.cop.UID()))
        conversation = IConversation(self.copevent)
        comment = createObject('plone.Comment')
        comment.creator = TEST_USER_ID
        conversation.addComment(comment)
        comments = view.get_commentaries('/'.join(self.copevent.getPhysicalPath()))
        self.assertEqual(len(comments), 1)
        self.assertFalse(view.see_more)
        for i in range(5):
            comment = createObject('plone.Comment')
            comment.creator = TEST_USER_ID
            conversation.addComment(comment)
        comments = view.get_commentaries('/'.join(self.copevent.getPhysicalPath()), 4)
        self.assertEqual(len(comments), 4)
        self.assertTrue(view.see_more)
        comments = view.get_commentaries('/'.join(self.copevent.getPhysicalPath()))
        self.assertEqual(len(comments), 6)
        self.assertFalse(view.see_more)


