# -*- coding: utf-8 -*-
from Products.validation import validation
from communities.practice.content.validators import \
    FileExtensionValidator, DuplicadoIdValidator
from communities.practice.tests.CoPBase import IntegrationTestCase


class TestValidation(IntegrationTestCase):

    def setUp(self):
        super(TestValidation, self).setUp()
        self.factoryCoPFolderPortfolio()
        FileExtensionValidator(
            'isValidExtension', extension=['jpg', 'jpeg', 'png', 'bmp', 'gif']
        )
        DuplicadoIdValidator('isDuplicadoId')

    def test_file_extension_ok(self):
        from cgi import FieldStorage
        from ZPublisher.HTTPRequest import FileUpload
        from tempfile import TemporaryFile
        fp = TemporaryFile('w+b')
        fp.write('x' * (1 << 19))
        fp.seek(0)
        env = {'REQUEST_METHOD': 'PUT'}
        headers = {
            'content-type': 'text/plain',
            'content-length': 1 << 19,
            'content-disposition': 'attachment; filename=test.jpg'
        }
        fs = FieldStorage(fp=fp, environ=env, headers=headers)
        f = FileUpload(fs)
        v = validation.validatorFor('isValidExtension')
        self.assertFalse(v(f))

    def test_file_extension_fail(self):
        from cgi import FieldStorage
        from ZPublisher.HTTPRequest import FileUpload
        from tempfile import TemporaryFile
        fp = TemporaryFile('w+b')
        fp.write('x' * (1 << 19))
        fp.seek(0)
        env = {'REQUEST_METHOD': 'PUT'}
        headers = {
            'content-type': 'text/plain',
            'content-length': 1 << 19,
            'content-disposition': 'attachment; filename=test.txt'
        }
        fs = FieldStorage(fp=fp, environ=env, headers=headers)
        f = FileUpload(fs)
        v = validation.validatorFor('isValidExtension')
        self.assertTrue(v(f))

    def test_duplicate_obj_and_objtemp_ok(self):
        self.copfolderportfolio.invokeFactory(
            'CoPPortfolio', 'copportfolio',
            title="CoPPortfolio Title",
            description="CoPPortfolio Description",
        )

        copportfolio = self.copfolderportfolio.copportfolio
        v = validation.validatorFor('isDuplicadoId')
        self.assertTrue(v(copportfolio, instance=copportfolio))

        temp = self.copfolderportfolio.createObject(type_name="CoPPortfolio")
        copportfolio_temp = self.copfolderportfolio.restrictedTraverse(
            temp[temp.index('portal_factory'):temp.rindex('/')]
        )
        v = validation.validatorFor('isDuplicadoId')
        self.assertEquals(
            v(copportfolio_temp, instance=copportfolio_temp),
            u"Você já possui um %s cadastrado!" % (copportfolio_temp.Type())
        )

    def test_duplicate_temp_obj_ok(self):
        temp = self.copfolderportfolio.createObject(type_name="CoPPortfolio")
        copportfolio_temp = self.copfolderportfolio.restrictedTraverse(
            temp[temp.index('portal_factory'):temp.rindex('/')]
        )
        v = validation.validatorFor('isDuplicadoId')
        self.assertFalse(v(copportfolio_temp, instance=copportfolio_temp))
