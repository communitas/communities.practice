# -*- coding: utf-8 -*-
import shutil

from communities.practice.tests.CoPBase import IntegrationTestCase
from communities.practice.generics.asyncmail import *
from communities.practice.generics import asyncmail_config
from communities.practice.generics.generics import getCoPSettings

class AsyncmailTestCase(IntegrationTestCase):

    def setUp(self):
        super(AsyncmailTestCase, self).setUp()
        self.factoryCoP()

    def test_asyncmail_methods(self):        
        #test method configureSMTPMailer
        mailer = configureSMTPMailer()
        settings = getCoPSettings()
        self.assertEquals(mailer.hostname, settings.hostname)
        self.assertEquals(mailer.port, settings.port)
        self.assertEquals(mailer.username, settings.username)
        self.assertEquals(mailer.password, settings.password)

        #test do method configureMaildir
        maildir = configureMaildir()

        paths = []
        for path in ('/tmp', '/cur', '/new'):
            paths.append(maildir.path + path)

        self.assertTrue(os.path.exists(maildir.path))
        for path in paths:
            self.assertTrue(os.path.exists(path))
            shutil.rmtree(path)

        maildir = configureMaildir()
        for path in paths:
            self.assertTrue(os.path.exists(path))

        shutil.rmtree(maildir.path)
        asyncmail_config.DIRECTORY = ''
        maildir = configureMaildir()
        for path in ('', '/tmp', '/cur', '/new'):
            self.assertTrue(os.path.exists(maildir.path + path))

        #test methods createMessage and sendAsyncMail
        mail_from = settings.username
        list_mail_to = settings.username
        subject = 'Subject'
        message = 'Content-Type: text/plain; charset="UTF-8"\nMIME-Version: 1.0\n\n'
        origin = self.portal.absolute_url()
        createMessage(mail_from, list_mail_to, subject, message, origin)
        sendAsyncMail()
