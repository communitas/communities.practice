# -*- coding: utf-8 -*-
from communities.practice.tests.CoPBase import IntegrationTestCase


class ViewCoPFolderTestCase(IntegrationTestCase):

    def setUp(self):
        super(ViewCoPFolderTestCase, self).setUp()
        self.factoryCoPFolder()
        self.view = self.getView(self.copfolder, 'viewCoPFolder')

    def test_set_context_actions(self):
        self.createUsers(['participante'])
        self.setRoleInCoP(self.cop, 'participante', 'Participante')
        context_actions = self.view.set_context_actions()
        actions_ids = [i['id'] for i in context_actions]
        self.assertIn('delete', actions_ids)
        self.assertIn('cut', actions_ids)
        self.changeLoggedMember('participante')
        context_actions = self.view.set_context_actions()
        actions_ids = [i['id'] for i in context_actions]
        self.assertNotIn('delete', actions_ids)
        self.assertNotIn('cut', actions_ids)
        self.createTestContent(self.copfolder, 'CoPFolder', 'copfolder')
        self.view = self.getView(self.copfolder.copfolder_1, 'viewCoPFolder')
        context_actions = self.view.set_context_actions()
        actions_ids = [i['id'] for i in context_actions]
        self.assertIn('delete', actions_ids)
        self.assertIn('cut', actions_ids)
