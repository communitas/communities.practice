# -*- coding: utf-8 -*-
from communities.practice.tests.CoPBase import IntegrationTestCase
from communities.practice.generics.generics import setMasterRole
from communities.practice.subscribers import deleteMastersGroups


class ViewCoPCommunityFolderConfigTestCase(IntegrationTestCase):

    def setUp(self):
        super(ViewCoPCommunityFolderConfigTestCase, self).setUp()
        self.factoryCoPCommunityFolder()
        self.view = self.getView(
            self.copcommunityfolder, 'viewCoPCommunityFolderConfig'
        )

    def test_get_filter_terms(self):
        self.assertEqual(
            self.view.get_filter_terms(), [('', 'Pesquisar')]
        )

    def test_set_cop_menu_selected(self):
        self.assertEqual(self.view.set_cop_menu_selected(), 'configuracoes')

    def test_set_coppla_workflow(self):
        coppla_workflow = self.view.set_coppla_workflow()
        self.assertEqual(
            coppla_workflow['review_state']['id'], 'publico'
        )
        transitions = [t['id'] for t in coppla_workflow['transitions']]
        self.assertEqual(
            transitions, ['autenticado']
        )
        self.workflow_tool.doActionFor(self.copcommunityfolder, 'autenticado')
        coppla_workflow = self.view.set_coppla_workflow()
        self.assertEqual(
            coppla_workflow['review_state']['id'], 'autenticado'
        )
        transitions = [t['id'] for t in coppla_workflow['transitions']]
        self.assertEqual(
            transitions, ['publico']
        )

    def test_get_cop_edit(self):
        self.assertEqual(
            self.view.get_cop_edit()['url'],
            '%s/edit' % self.view.context.absolute_url()
        )

    def test_set_context_title(self):
        self.assertEqual(
            self.view.set_context_title()['title'], 'Configurações'
        )

    def test_can_edit(self):
        self.addMasterUsers()
        self.assertTrue(self.view.can_edit())
        self.changeLoggedMember('user_master1')
        self.assertFalse(self.view.can_edit())
        self.changeLoggedMember('user_master5')
        self.assertFalse(self.view.can_edit())

    def test_get_users(self):
        self.addMasterUsers()
        self.assertEqual(
            self.view.get_users(),
            self.view.get_moderators()
        )
        self.view.request['user_config'] = 'add'
        self.view.request['SearchableText'] = 'master'
        self.assertEqual(
            self.view.get_users(),
            self.view.get_add_moderators()
        )

    def test_set_users(self):
        self.view.request['update_roles'] = True
        self.request.form = {
            'user_master1': ['Excluir', 'Moderador_Master'],
            'user_master2': ['Observador_Master', 'Moderador_Master'],
            'user_master5': ['Moderador_Master', 'Observador_Master'],
        }
        self.addMasterUsers()
        self.assertEqual(
            self.view.set_users(),
            self.view.set_moderators()
        )
        self.view.request['user_config'] = 'add'
        self.view.request.form = {
            'user_master3': ['Moderador_Master', 'Selecionar'],
            'user_master4': ['Observador_Master', 'Selecionar'],
        }
        self.assertEqual(
            self.view.set_users(),
            self.view.set_add_moderators()
        )

    def test_get_add_moderators(self):
        self.addMasterUsers()
        self.view.get_add_moderators()
        self.assertEqual(self.view.get_add_moderators(), [])
        self.view.request['SearchableText'] = 'master'
        add_moderators = [
            item['id'] for item in self.view.get_add_moderators()
        ]
        self.assertEqual(
            add_moderators,
            ['user_master3', 'user_master4', 'user_master6']
        )
        setMasterRole(
            self.copcommunityfolder, 'user_master3', 'Moderador_Master'
        )
        add_moderators = [
            item['id'] for item in self.view.get_add_moderators()
        ]
        self.assertEqual(
            add_moderators,
            ['user_master4', 'user_master6']
        )
        deleteMastersGroups(self.view.context, False)
        self.assertEqual(len(self.view.get_add_moderators()),6)

    def test_set_add_moderators(self):
        self.addMasterUsers()
        self.view.request['update_roles'] = True
        self.view.request.form = {
            'user_master3': ['Moderador_Master', 'Selecionar'],
            'user_master4': ['Observador_Master', 'Selecionar'],
        }
        self.view.set_add_moderators()
        moderators = [
            (item['id'], item['role']) for item in self.view.get_moderators()
        ]
        self.assertIn(('user_master3', 'Moderador_Master'), moderators)
        self.assertIn(('user_master4', 'Observador_Master'), moderators)

    def test_get_moderators(self):
        self.addMasterUsers()
        moderators = [
            moderator['id'] for moderator in self.view.get_moderators()
        ]
        self.assertEqual(
            moderators, ['user_master1', 'user_master2', 'user_master5']
        )
        setMasterRole(self.copcommunityfolder, 'user_master5', 'Excluir')
        moderators = [
            moderator['id'] for moderator in self.view.get_moderators()
        ]
        self.assertEqual(moderators, ['user_master1', 'user_master2'])

    def test_set_moderators(self):
        self.addMasterUsers()
        setMasterRole(
            self.copcommunityfolder, 'user_master3', 'Moderador_Master'
        )
        self.view.request['update_roles'] = True
        self.request.form = {
            'user_master1': ['Excluir', 'Moderador_Master'],
            'user_master2': ['Observador_Master', 'Moderador_Master'],
            'user_master3': ['Moderador_Master', 'Moderador_Master'],
            'user_master5': ['Moderador_Master', 'Observador_Master'],
        }
        self.view.set_moderators()
        moderators = [
            (item['id'], item['role']) for item in self.view.get_moderators()
        ]
        self.assertNotIn(('user_master1', 'Moderador_Master'), moderators)
        self.assertIn(('user_master2', 'Observador_Master'), moderators)
        self.assertIn(('user_master3', 'Moderador_Master'), moderators)
        self.assertIn(('user_master5', 'Moderador_Master'), moderators)

    def addMasterUsers(self):
        """Adds users and add them to CoPCommunityFolder
        """
        users_ids = [
            'user_master1', 'user_master2', 'user_master3',
            'user_master4', 'user_master5', 'user_master6',
        ]
        self.createUsers(users_ids)
        users_ids.append(
            self.portal.portal_membership.getAuthenticatedMember().getId()
        )
        self.setMembersData(users_ids)
        setMasterRole(
            self.copcommunityfolder, 'user_master1', 'Moderador_Master'
        )
        setMasterRole(
            self.copcommunityfolder, 'user_master2', 'Moderador_Master'
        )
        setMasterRole(
            self.copcommunityfolder, 'user_master5', 'Observador_Master'
        )
