# -*- coding: utf-8 -*-

from communities.practice.tests.CoPBase import IntegrationTestCase
from DateTime import DateTime

class CoPPostTestCase(IntegrationTestCase):

    def setUp(self):
        super(CoPPostTestCase, self).setUp()
        self.factoryCoPPost()

    def test_CoPPost_addable(self):
        self.assertEquals(self.coppost.Title(), "Post")
        self.assertEquals(self.coppost.Description(), "CoPPost Description")
