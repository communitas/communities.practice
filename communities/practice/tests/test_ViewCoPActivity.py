# -*- coding: utf-8 -*-
from communities.practice.tests.CoPBase import IntegrationTestCase
from communities.practice.config import OPCOES
from communities.practice.generics.atividade_generics import \
    getCoPAtividadeMembers, getCoPAtividadeContent, getStartEndDate,\
    getCoPAtividadeTypes, getCoPAtividadeTotalTypes, getCoPAtividadeLenMembers


class ViewCoPActivityTestCase(IntegrationTestCase):

    def setUp(self):
        super(ViewCoPActivityTestCase, self).setUp()
        self.factoryCoPMenu()
        self.cop.setTarefas_input(OPCOES[0])
        self.addUsersCoPActivity(self.cop)
        self.createContentActivity(self.cop)
        self.view = self.getView(self.copmenu, 'viewCoPActivity')

    def test_get_community_members(self):
        self.assertEqual(
            self.view.get_community_members(),
            getCoPAtividadeMembers(self.cop)
        )
        self.view.request['SearchableText'] = 'user'
        self.assertEqual(
            self.view.get_community_members(),
            getCoPAtividadeMembers(self.cop, 'user')
        )

    def test_get_community_content(self):
        member_list = self.view.get_community_members()
        folder = 'comunidade'
        period = getStartEndDate(self.view.request.form)
        self.assertEqual(
            self.view.get_community_content(member_list),
            getCoPAtividadeContent(self.cop, member_list, folder, period)
        )
        folder = 'acervo'
        self.view.request['filter_term'] = folder
        self.assertEqual(
            self.view.get_community_content(member_list),
            getCoPAtividadeContent(self.cop, member_list, folder, period)
        )

    def test_get_types(self):
        folder = 'comunidade'
        self.assertEqual(
            self.view.get_types(),
            getCoPAtividadeTypes(folder, True, self.cop)
        )
        folder = 'acervo'
        self.view.request['filter_term'] = folder
        self.assertEqual(
            self.view.get_types(),
            getCoPAtividadeTypes(folder, True, self.cop)
        )

    def test_check_permission(self):
        self.createUsers(['participante', 'observador'])
        self.setRoleInCoP(self.cop, 'participante', 'Participante')
        self.setRoleInCoP(self.cop, 'observador', 'Observador')
        self.changeLoggedMember('participante')
        self.assertFalse(self.view.check_permission())
        self.changeLoggedMember('observador')
        self.assertTrue(self.view.check_permission())

    def test_get_total_types(self):
        period = getStartEndDate(self.view.request.form)
        folder = 'comunidade'
        self.assertEqual(
            self.view.get_total_types(),
            getCoPAtividadeTotalTypes(self.cop, folder, period)
        )
        folder = 'acervo'
        self.view.request['filter_term'] = folder
        self.assertEqual(
            self.view.get_total_types(),
            getCoPAtividadeTotalTypes(self.cop, folder, period)
        )

    def test_get_filter_terms(self):
        filter_terms = self.view.get_filter_terms()
        self.assertEqual(len(filter_terms), 5)
        terms = ['comunidade', 'acervo', 'portfolio', 'forum', 'tarefas']
        for idx, term in enumerate(terms):
            self.assertEqual(filter_terms[idx][0], term)
        self.cop.setAvailable_forms((u'form_type'))
        filter_terms = self.view.get_filter_terms()
        self.assertEqual(len(filter_terms), 6)
        terms.append('formularios')
        for idx, term in enumerate(terms):
            self.assertEqual(filter_terms[idx][0], term)

    def test_get_len_members(self):
        self.assertEqual(
            self.view.get_len_members(),
            getCoPAtividadeLenMembers(self.cop)
        )

    def test_quote(self):
        self.assertEqual(
            self.view.quote("Luciano Camargo Cruz"),
            "Luciano%20Camargo%20Cruz"
        )
