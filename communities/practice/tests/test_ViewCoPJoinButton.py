# -*- coding: utf-8 -*-
from communities.practice.tests.CoPBase import IntegrationTestCase
from plone.app.testing import TEST_USER_NAME, TEST_USER_ID
from communities.practice.generics.generics import getCoPRole


class ViewCoPJoinTestCase(IntegrationTestCase):

    def setUp(self):
        super(ViewCoPJoinTestCase, self).setUp()
        self.factoryCoP()
        self.view = self.getView(self.cop, 'viewCoPJoin')
        self.createUsers(['member'])
        self.setMembersData(['member'])

    def test_send_participation_request(self):
        self.setMembersData([TEST_USER_ID])
        self.changeLoggedMember('member')
        self.assertFalse(self.view.send_participation_request())
        self.request.form = {}
        self.request.form['submit'] = 'Enviar solicitacao'
        self.request.form['message'] = 'mensagem'
        self.assertEqual(
            self.view.send_participation_request(),
            self.cop.absolute_url()
        )
        self.assertEqual(
            getCoPRole(self.cop.UID(), 'member'),
            'Aguardando'
        )
        self.view.request.response.setStatus('400')
        messages = self.view.context.plone_utils.showPortalMessages()
        self.assertEqual(
            messages[0].message.split(),
            u"""Sua solicitação de participação
                foi encaminhada para o moderador da comunidade
            """.split()
        )
        self.setRoleInCoP(self.cop, 'member', 'Excluir')
        self.changeLoggedMember(TEST_USER_NAME)
        self.workflow_tool.doActionFor(self.cop, 'publico')
        self.changeLoggedMember('member')
        self.assertEqual(
            self.view.send_participation_request(),
            self.cop.absolute_url()
        )
        self.assertEqual(
            getCoPRole(self.cop.UID(), 'member'),
            'Participante'
        )
        self.view.request.response.setStatus('400')
        messages = self.view.context.plone_utils.showPortalMessages()
        self.assertEqual(
            messages[0].message, u'Você foi adicionado como participante'
        )

    def test_send_notification_mail(self):
        self.view.send_notification_mail('message')
