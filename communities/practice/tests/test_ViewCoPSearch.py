# -*- coding: utf-8 -*-
from communities.practice.tests.CoPBase import IntegrationTestCase


class ViewCoPSearchTestCase(IntegrationTestCase):

    def setUp(self):
        super(ViewCoPSearchTestCase, self).setUp()
        self.factoryCoP()
        self.view = self.getView(self.cop, 'viewCoPSearch')

    def test_get_filter_terms(self):
        self.assertEqual(
            self.view.get_filter_terms(),
            [
                ('', 'Ver Todos'),
                ('CoPFile', 'Arquivos'),
                ('CoPImage', 'Imagens'),
                ('CoPDocument', 'Páginas'),
                ('CoPPost', 'Posts'),
            ]
        )
