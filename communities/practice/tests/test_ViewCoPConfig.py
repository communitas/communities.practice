# -*- coding: utf-8 -*-
from communities.practice.tests.CoPBase import IntegrationTestCase
from plone.app.testing import TEST_USER_ID


class ViewCoPConfigTestCase(IntegrationTestCase):

    def setUp(self):
        super(ViewCoPConfigTestCase, self).setUp()
        self.factoryCoPMenu()
        self.factorySubCoP()
        self.view = self.getView(self.copmenu, 'viewCoPConfig')
        users = ['user1', 'user2', 'user3', 'user4']
        self.createUsers(users)
        self.setMembersData(users)

    def test_get_users(self):
        self.assertEqual(self.view.get_users(), self.view.get_participants())
        self.view.request['user_config'] = 'add'
        self.view.request['SearchableText'] = 'user'
        self.assertEqual(
            self.view.get_users(), self.view.get_add_participants()
        )
        self.view.request['user_config'] = 'pending'
        self.assertEqual(
            self.view.get_users(), self.view.get_pending_participants()
        )

    def test_set_users(self):
        self.assertEqual(self.view.set_users(), self.view.set_participants())
        self.view.request['user_config'] = 'add'
        self.assertEqual(
            self.view.set_users(), self.view.set_add_participants()
        )
        self.view.request['user_config'] = 'pending'
        self.assertEqual(
            self.view.set_users(), self.view.set_pending_participants()
        )

    def test_get_participants(self):
        self.setRoleInCoP(self.cop, 'user1', 'Participante')
        self.setRoleInCoP(self.cop, 'user2', 'Aguardando')
        participants = self.view.get_participants()
        self.assertEqual(len(participants), 2)
        self.assertTrue(participants[0]['super'])
        self.assertFalse(participants[1]['super'])
        self.assertFalse(participants[0]['transitions'])
        self.assertTrue(participants[1]['transitions'])

        self.createTestContent(self.subcop, 'CoPMenu', 'copmenu')
        self.view = self.getView(self.subcop.copmenu_1, 'viewCoPConfig')
        self.setRoleInCoP(self.cop, 'user2', 'Moderador')
        participants = self.view.get_participants()
        self.assertEqual(len(participants), 2)
        self.assertTrue(participants[1]['super'])
        self.assertFalse(participants[1]['transitions'])

    def test_get_add_participants(self):
        self.assertFalse(self.view.get_add_participants())
        self.view.request['SearchableText'] = 'user'
        self.assertEqual(len(self.view.get_add_participants()), 4)

        self.createTestContent(self.subcop, 'CoPMenu', 'copmenu')
        self.view = self.getView(self.subcop.copmenu_1, 'viewCoPConfig')
        self.setRoleInCoP(self.cop, 'user1', 'Moderador')
        self.setRoleInCoP(self.cop, 'user2', 'Participante')
        user2 = self.portal.portal_membership.getMemberById('user2')
        user2.setMemberProperties(mapping={'fullname': 'Member2'})
        participants = self.view.get_add_participants()
        self.assertEqual(len(participants), 1)
        self.assertEqual(participants[0]['id'], 'user2')

    def test_get_pending_participants(self):
        self.assertFalse(self.view.get_pending_participants())
        self.setRoleInCoP(self.cop, 'user1', 'Aguardando')
        self.assertTrue(self.view.get_pending_participants())

    def test_set_participants(self):
        """Test method set_participants
        """
        self.setRoleInCoP(self.cop, 'user1', 'Participante')
        self.setRoleInCoP(self.cop, 'user1', 'Bloqueado')
        self.setRoleInCoP(self.cop, 'user2', 'Participante')
        self.setRoleInCoP(self.cop, 'user3', 'Moderador')
        self.setRoleInCoP(self.cop, 'user4', 'Observador')
        self.assertFalse(self.view.set_participants())
        self.request['update_roles'] = True
        self.request.form = {
            'user1': ['Participante', 'Bloqueado'],
            'user2': ['Moderador', 'Participante'],
            'user3': ['Bloqueado', 'Moderador'],
            'user4': ['Observador', 'Observador'],
        }
        self.assertTrue(self.view.set_participants())
        participants = [
            (i['id'], i['role']) for i in self.view.get_participants()
        ]
        self.assertIn(('user1', 'Participante'), participants)
        self.assertIn(('user2', 'Moderador'), participants)
        self.assertIn(('user3', 'Bloqueado'), participants)
        self.assertIn(('user4', 'Observador'), participants)
        self.request.form = {'user1': ['Excluir', 'Participante']}
        self.assertTrue(self.view.set_participants())
        participants = [
            i['id'] for i in self.view.get_participants()
        ]
        self.assertNotIn('user1', participants)

    def test_set_add_participants(self):
        self.assertFalse(self.view.set_add_participants())
        self.request['update_roles'] = True
        self.request.form = {
            'user1': ['Participante', 'Selecionar'],
            'user2': ['Observador', 'Selecionar'],
            'user3': 'Selecionar',
        }
        self.assertTrue(self.view.set_add_participants())
        participants = [
            (i['id'], i['role']) for i in self.view.get_participants()
        ]
        self.assertIn(('user1', 'Participante'), participants)
        self.assertIn(('user2', 'Observador'), participants)
        self.assertNotIn('user3', [item[0] for item in participants])
        self.createTestContent(self.subcop, 'CoPMenu', 'copmenu')
        self.view = self.getView(self.subcop.copmenu_1, 'viewCoPConfig')
        self.request['update_roles'] = True
        self.request.form = {
            'user1': ['Participante', 'Selecionar'],
            'user3': ['Participante', 'Selecionar'],
        }
        self.assertTrue(self.view.set_add_participants())
        participants = [
            (i['id'], i['role']) for i in self.view.get_participants()
        ]
        self.assertIn(('user1', 'Participante'), participants)
        self.assertIn(('user2', 'Observador'), participants)
        self.assertNotIn('user3', [item[0] for item in participants])

    def test_set_pending_participants(self):
        for user_id in ['user1', 'user2', 'user3']:
            self.setRoleInCoP(self.cop, user_id, 'Aguardando')
        self.assertFalse(self.view.set_pending_participants())
        self.request['update_roles'] = True
        self.request['refused_message'] = 'refused message'
        self.request.form = {
            'user1': ['Aprovar', 'Selecionar'],
            'user2': ['Negar', 'Selecionar'],
            'user3': 'Aguardando',
        }
        self.assertTrue(self.view.set_pending_participants())
        participants = [
            (i['id'], i['role']) for i in self.view.get_participants()
        ]
        self.assertIn(('user1', 'Participante'), participants)
        self.assertNotIn('user2', [item[0] for item in participants])
        pending = [
            i['id'] for i in self.view.get_pending_participants()
        ]
        self.assertIn('user3', pending)
        self.assertNotIn('user2', pending)

    def test_get_filter_terms(self):
        self.assertEqual(
            self.view.get_filter_terms(),
            [('', 'Pesquisar')]
        )

    def test_get_cop_role(self):
        self.assertEqual(self.view.get_cop_role(TEST_USER_ID), 'Owner')
        self.assertFalse(self.view.get_cop_role('user1'))
        self.setRoleInCoP(self.cop, 'user1', 'Participante')
        self.assertEqual(self.view.get_cop_role('user1'), 'Participante')

    def test_check_permission(self):
        self.setRoleInCoP(self.cop, 'user1', 'Moderador')
        self.changeLoggedMember('user1')
        self.assertTrue(
            self.view.check_permission('communities.practice.ModerateCoP')
        )
        self.assertFalse(
            self.view.check_permission(
                'communities.practice.ModerateCoP', True
            )
        )

    def test_can_edit(self):
        self.assertTrue(self.view.can_edit())
        self.setRoleInCoP(self.cop, 'user1', 'Participante')
        self.setRoleInCoP(self.cop, 'user2', 'Moderador')
        self.changeLoggedMember('user1')
        self.assertFalse(self.view.can_edit())
        self.changeLoggedMember('user2')
        self.assertFalse(self.view.can_edit())

    def test_can_review_state(self):
        self.assertTrue(self.view.can_review_state())
        self.setRoleInCoP(self.cop, 'user1', 'Participante')
        self.setRoleInCoP(self.cop, 'user2', 'Moderador')
        self.changeLoggedMember('user1')
        self.assertFalse(self.view.can_review_state())
        self.changeLoggedMember('user2')
        self.assertFalse(self.view.can_review_state())

    def test_can_moderate(self):
        self.setRoleInCoP(self.cop, 'user1', 'Participante')
        self.setRoleInCoP(self.cop, 'user2', 'Moderador')
        self.changeLoggedMember('user1')
        self.assertFalse(self.view.can_moderate())
        self.changeLoggedMember('user2')
        self.assertTrue(self.view.can_moderate())

    def test_set_coppla_workflow(self):
        coppla_workflow = self.view.set_coppla_workflow()
        self.assertEqual(coppla_workflow['review_state']['id'], 'restrito')
        transitions = [t['id'] for t in coppla_workflow['transitions']]
        self.assertItemsEqual(transitions, ['privado', 'publico'])
        self.workflow_tool.doActionFor(self.cop, 'publico')
        coppla_workflow = self.view.set_coppla_workflow()
        self.assertEqual(coppla_workflow['review_state']['id'], 'publico')
        transitions = [t['id'] for t in coppla_workflow['transitions']]
        self.assertItemsEqual(transitions, ['privado', 'restrito'])

    def test_get_cop_edit(self):
        cop_edit = self.view.get_cop_edit()
        self.assertEqual(cop_edit['title'], 'Editar')
        self.assertEqual(cop_edit['url'], '%s/edit' % self.cop.absolute_url())

    def test_get_leave_cop_url(self):
        self.assertEqual(
            self.view.get_leave_cop_url(),
            '%s/viewCoPLeave' % self.cop.absolute_url()
        )

    def test_can_leave_cop(self):
        self.createTestContent(self.subcop, 'CoPMenu', 'copmenu')
        self.assertFalse(self.view.can_leave_cop())
        self.setRoleInCoP(self.cop, 'user1', 'Moderador')
        self.changeLoggedMember('user1')
        self.assertTrue(self.view.can_leave_cop())
        self.view = self.getView(self.subcop.copmenu_1, 'viewCoPConfig')
        self.assertFalse(self.view.can_leave_cop())
