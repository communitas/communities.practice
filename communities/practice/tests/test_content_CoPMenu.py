# -*- coding: utf-8 -*-

from communities.practice.tests.CoPBase import IntegrationTestCase

class CoPMenuTestCase(IntegrationTestCase):

    def setUp(self):
        super(CoPMenuTestCase, self).setUp()
        self.factoryCoP()

    def test_CoPMenu_addable(self):
        self.cop.invokeFactory('CoPMenu', 'copmenu',
                               title="CoPMenu Title",
                               description="CoPMenu Description",)

        copmenu = self.cop.copmenu
        
        self.assertEquals(copmenu.Title(), "CoPMenu Title")
        self.assertEquals(copmenu.Description(), "CoPMenu Description")
