# -*- coding: utf-8 -*-

from communities.practice.tests.CoPBase import IntegrationTestCase

class CoPLinkTestCase(IntegrationTestCase):

    def setUp(self):
        super(CoPLinkTestCase, self).setUp()
        self.factoryCoPLink()

    def test_CoPLink_addable(self):
        self.assertEquals(self.coplink.Title(), "CoPLink Title")
        self.assertEquals(self.coplink.Description(), "CoPLink Description")
        self.assertEquals(self.coplink.getRemoteUrl(), "http://www.otics.org/otics")
