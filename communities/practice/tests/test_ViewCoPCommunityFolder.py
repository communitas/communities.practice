# -*- coding: utf-8 -*-
from plone.app.testing import TEST_USER_NAME
from Products.CMFPlone.utils import getToolByName
from communities.practice.tests.CoPBase import IntegrationTestCase
from communities.practice.browser.ViewCoPCommunityFolder import \
    ViewCoPCommunityFolder


class ViewCoPCommunityFolderTestCase(IntegrationTestCase):

    def setUp(self):
        super(ViewCoPCommunityFolderTestCase, self).setUp()
        self.factoryCoPCommunityFolder()
        self.view = ViewCoPCommunityFolder(
            self.copcommunityfolder, self.request
        )

    def test_get_filter_terms(self):
        self.view.get_filter_terms()
        terms_ids = [item[0] for item in self.view.filter_terms]
        self.assertEqual(
            terms_ids, ['', 'all', 'alphabetic', 'latest']
        )

    def test_set_cop_menu(self):
        cop_menu = [item['id'] for item in self.view.set_cop_menu()]
        self.assertEqual(
            cop_menu, [
                'home', 'copcommunityfolderatividade',
                'copcommunityneverlogged', 'configuracoes'
            ]
        )
        self.changeLoggedMember()
        cop_menu = [item['id'] for item in self.view.set_cop_menu()]
        self.assertEqual(
            cop_menu, ['home']
        )

    def test_get_addable_types(self):
        self.assertEqual(len(self.view.get_addable_types()), 1)
        self.changeLoggedMember()
        self.assertEqual(len(self.view.get_addable_types()), 0)

    def test_get_sub_cop_number(self):
        self.factorySubCoP()
        user_id = 'user1'
        self.createUsers([user_id])
        self.changeLoggedMember(user_id)
        self.view = ViewCoPCommunityFolder(
            self.copcommunityfolder, self.request
        )
        path = '/'.join(self.cop.getPhysicalPath())
        cop_number = self.view.get_sub_cop_number(path)
        self.assertEqual(cop_number, 0)
        self.changeLoggedMember(TEST_USER_NAME)
        self.setRoleInCoP(self.cop, user_id, 'Participante')
        self.setRoleInCoP(self.subcop, user_id, 'Participante')
        self.changeLoggedMember(user_id)
        self.view = ViewCoPCommunityFolder(
            self.copcommunityfolder, self.request
        )
        cop_number = self.view.get_sub_cop_number(path)
        self.assertEqual(cop_number, 1)
        cop_number = self.view.get_sub_cop_number(path,1)
        self.assertEqual(cop_number, 1)

    def test_sub_cop_menu_available(self):
        self.factorySubCoP()
        user_id = 'user1'
        self.createUsers([user_id])
        self.setRoleInCoP(self.cop, user_id, 'Participante')
        self.setRoleInCoP(self.subcop, user_id, 'Participante')
        self.changeLoggedMember(user_id)
        self.view = ViewCoPCommunityFolder(
            self.copcommunityfolder, self.request
        )
        path = '/'.join(self.cop.getPhysicalPath())
        sub_cop_menu = self.view.sub_cop_menu_available(path)
        self.assertTrue(sub_cop_menu)
        self.changeLoggedMember(TEST_USER_NAME)
        self.portal.portal_workflow.doActionFor(self.cop.subcop, 'privado')
        self.changeLoggedMember(user_id)
        self.view = ViewCoPCommunityFolder(
            self.copcommunityfolder, self.request
        )
        sub_cop_menu = self.view.sub_cop_menu_available(path)
        self.assertFalse(sub_cop_menu)

    def test_get_communities(self):
        self.createTestContent(self.copcommunityfolder, 'CoP', 'test_cop', 2)
        self.assertEqual(
            self.view.get_communities(), self.view.get_my_communities(None)
        )
        self.view.request['filter_term'] = 'my'
        self.assertEqual(
            self.view.get_communities(),
            self.view.get_all_communities(None)
        )
        self.view.request['filter_term'] = 'alphabetic'
        self.assertEqual(
            self.view.get_communities(),
            self.view.get_alphabetic_communities(None)
        )
        self.view.request['filter_term'] = 'latest'
        self.assertEqual(
            self.view.get_communities(),
            self.view.get_latest_communities(None)
        )

        self.view.request['filter_term'] = 'all'
        self.assertEqual(
            self.view.get_communities(),
            self.view.get_all_communities(None)
        )

    def test_get_all_communities(self):
        self.createTestContent(self.copcommunityfolder, 'CoP', 'test_cop', 4)
        self.assertEqual(len(self.view.get_all_communities(None)), 4)
        self.assertEqual(len(self.view.get_all_communities('test_cop_4')), 1)

    def test_get_my_communities(self):
        self.createTestContent(self.copcommunityfolder, 'CoP', 'test_cop', 2)
        self.assertEqual(len(self.view.get_my_communities(None)), 2)
        test_cop_1 = self.copcommunityfolder.test_cop_1
        self.createUsers(['member1'])
        self.setRoleInCoP(test_cop_1, 'member1', 'Participante')
        self.changeLoggedMember('member1')
        self.assertEqual(len(self.view.get_my_communities(None)), 1)

    def test_get_alphabetic_communities(self):
        self.createTestContent(self.copcommunityfolder, 'CoP', 'test_cop', 2)
        cops = self.view.get_alphabetic_communities(None)
        self.assertEqual(cops[0]['title'], 'test_cop_1')
        self.copcommunityfolder.test_cop_1.setTitle('z last cop')
        self.copcommunityfolder.test_cop_1.reindexObject()
        cops = self.view.get_alphabetic_communities(None)
        self.assertEqual(cops[0]['title'], 'test_cop_2')

    def test_get_latest_communities(self):
        self.createTestContent(self.copcommunityfolder, 'CoP', 'test_cop', 2)
        test_cop_1 = self.copcommunityfolder.test_cop_1
        test_cop_2 = self.copcommunityfolder.test_cop_2
        self.createTestContent(test_cop_1, 'CoPMenu', 'copmenu', 1)
        self.createTestContent(test_cop_2, 'CoPMenu', 'copmenu', 1)
        self.createTestContent(test_cop_1.copmenu_1, 'CoPDocument', 'copdoc', 1)
        self.createTestContent(test_cop_2.copmenu_1, 'CoPDocument', 'copdoc', 1)
        cops = self.view.get_latest_communities(None)
        self.assertEqual(cops[0]['title'], 'test_cop_2')
        self.createTestContent(test_cop_1.copmenu_1, 'CoPDocument', 'copdoc2', 1)
        cops = self.view.get_latest_communities(None)
        self.assertEqual(cops[0]['title'], 'test_cop_1')

    def test_get_communities_values(self):
        self.createTestContent(self.copcommunityfolder, 'CoP', 'test_cop', 2)
        test_cop_1 = self.copcommunityfolder.test_cop_1
        self.createUsers(['member1'])
        self.setRoleInCoP(test_cop_1, 'member1', 'Participante')
        ct = getToolByName(self.copcommunityfolder, 'portal_catalog')
        cop_brains = ct(
            path='/'.join(self.copcommunityfolder.getPhysicalPath()),
            portal_type='CoP',
        )
        cops = self.view.get_communities_values(cop_brains)
        participation = [cop['participation'] for cop in cops]
        self.assertItemsEqual(participation, ['Criador', 'Criador'])
        self.changeLoggedMember('member1')
        cops = self.view.get_communities_values(cop_brains)
        participation = [cop['participation'] for cop in cops]
        self.assertItemsEqual(participation, ['Participante', 'Autenticado'])
