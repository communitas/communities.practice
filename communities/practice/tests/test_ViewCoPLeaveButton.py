# -*- coding: utf-8 -*-
from communities.practice.tests.CoPBase import IntegrationTestCase
from plone.app.testing import TEST_USER_NAME, TEST_USER_ID
from communities.practice.generics.generics import getCoPRole


class ViewCoPLeaveButtonTestCase(IntegrationTestCase):

    def setUp(self):
        super(ViewCoPLeaveButtonTestCase, self).setUp()
        self.factorySubCoP()
        self.view = self.getView(self.cop, 'viewCoPLeave')

    def test_get_review_state(self):
        self.assertEqual(self.view.get_review_state(), 'restrito')
        self.workflow_tool.doActionFor(self.cop, 'publico')
        self.assertEqual(self.view.get_review_state(), 'publico')

    def test_send_request(self):
        self.setMembersData([TEST_USER_ID])
        self.assertFalse(self.view.send_request())
        self.changeLoggedMember()
        self.assertFalse(self.view.send_request())
        self.changeLoggedMember(TEST_USER_NAME)
        self.createUsers(['member'])
        self.setMembersData(['member'])
        self.setRoleInCoP(self.cop, 'member', 'Participante')
        self.changeLoggedMember('member')
        self.request.form = {
            'message': 'mensagem', 'submit': 'Enviar mensagem'
        }
        self.assertEqual(
            self.view.send_request(),
            self.copcommunityfolder.absolute_url()
        )
        self.assertEqual(
            getCoPRole(self.cop.UID(), 'member'),
            'Bloqueado'
        )
        self.view.request.response.setStatus('400')
        messages = self.view.context.plone_utils.showPortalMessages()
        self.assertEqual(
            messages[0].message.split(),
            u"""Sua solicitação para deixar de
                participar da comunidade foi encaminhada aos moderadores.
            """.split()
        )
        self.changeLoggedMember(TEST_USER_NAME)
        self.workflow_tool.doActionFor(self.cop, 'publico')
        self.setRoleInCoP(self.cop, 'member', 'Participante')
        self.changeLoggedMember('member')
        self.assertEqual(
            self.view.send_request(),
            self.copcommunityfolder.absolute_url()
        )
        self.assertFalse(getCoPRole(self.cop.UID(), 'member'))
        self.view.request.response.setStatus('400')
        messages = self.view.context.plone_utils.showPortalMessages()
        expected_message = \
            u'Você não participa mais da comunidade %s' % self.cop.Title()
        self.assertEqual(
            messages[0].message.split(),
            expected_message.split()
        )
        self.changeLoggedMember(TEST_USER_NAME)
        self.setRoleInCoP(self.cop, 'member', 'Participante')
        self.setRoleInCoP(self.subcop, 'member', 'Participante')
        self.view = self.getView(self.subcop, 'viewCoPLeave')
        self.changeLoggedMember('member')
        self.request.form = {
            'message': 'mensagem', 'submit': 'Enviar mensagem'
        }
        self.assertEqual(
            self.view.send_request(),
            self.cop.absolute_url()
        )

    def test_send_notification_mail(self):
        self.view.send_notification_mail('message')
