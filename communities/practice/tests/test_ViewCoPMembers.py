# -*- coding: utf-8 -*-
from communities.practice.tests.CoPBase import IntegrationTestCase
from communities.practice.generics.generics import getMembersData


class ViewCoPMembersTestCase(IntegrationTestCase):

    def setUp(self):
        super(ViewCoPMembersTestCase, self).setUp()
        self.factoryCoP()
        self.view = self.getView(self.cop, 'viewCoPMembers')
        users = ['user1', 'user2', 'user3', 'user4']
        self.createUsers(users)
        self.setMembersData(users)
        self.setRoleInCoP(self.cop, 'user1', 'Moderador')
        self.setRoleInCoP(self.cop, 'user2', 'Participante')
        self.setRoleInCoP(self.cop, 'user3', 'Participante')
        self.setRoleInCoP(self.cop, 'user4', 'Observador')

    def test_set_context_title(self):
        context_title = self.view.set_context_title()
        self.assertEqual(context_title['title'], 'Participantes')

    def test_get_cop_search(self):
        self.assertEqual(
            len(self.view.get_cop_search()), 4
        )
        self.request[self.view.get_search_parameter()] = '3'
        self.assertEqual(
            len(self.view.get_cop_search()), 1
        )

    def test_get_members_data(self):
        members = self.view.get_cop_search()
        self.assertEqual(
            self.view.get_members_data(members),
            getMembersData(self.cop, [user[0] for user in members])
        )

    def test_get_search_parameter(self):
        self.assertEqual(self.view.get_search_parameter(), 'user_search')
