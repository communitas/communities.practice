# -*- coding: utf-8 -*-
from StringIO import StringIO
from zptlogo import zptlogo
from communities.practice.tests.CoPBase import IntegrationTestCase
from communities.practice.browser.ViewCoPBase import ViewCoPBase
from communities.practice.generics.generics import getBrainValues
from communities.practice.generics.generics import isDiscussionAllowed
from communities.practice.generics.generics import getCoPUpdates


class ViewCoPBaseTestCase(IntegrationTestCase):

    def setUp(self):
        super(ViewCoPBaseTestCase, self).setUp()
        self.factoryCoPMenu()
        self.view = ViewCoPBase(self.cop, self.request)

    def test_get_cop_context(self):
        self.assertEqual(self.view.get_cop_context(), self.cop)

    def test_set_palette_color(self):
        self.assertEquals(
            self.view.set_palette_color(),
            (u'gray', u'Cinza')
        )

    def test_set_cop_menu_selected(self):
        self.assertEqual(
            self.view.set_cop_menu_selected('selected'), 'selected'
        )
        self.assertEqual(self.view.set_cop_menu_selected(), 'home')
        self.copmenu.setTitlemenu('titlemenu')
        self.view = ViewCoPBase(self.copmenu, self.request)
        self.assertEqual(self.view.set_cop_menu_selected(), 'titlemenu')

    def test_get_menu_index(self):
        ordered_menu = [
            'home', 'subcop', 'acervo',
            'calendario', 'portfolio', 'forum',
            'curso', 'formularios', 'chat',
            'tarefas', 'atividade', 'notificacoes',
            'configuracoes',
        ]
        for idx, menu in enumerate(ordered_menu):
            self.assertEqual(self.view.get_menu_index(menu), idx)
        self.assertEqual(self.view.get_menu_index('not'), -1)

    def test_set_cop_menu(self):
        self.copmenu.setTitlemenu('acervo')
        self.copmenu.reindexObject()
        self.view.get_cop_context()
        cop_menu = []
        cop_menu.append({
            'id': 'home',
            'url': self.view.cop_context.absolute_url(),
            'title': 'Início',
            'titlemenu': 'home',
            'order': self.view.get_menu_index('home'),
        })
        cop_menu.append({
            'id': self.copmenu.getId(),
            'url': self.copmenu.absolute_url(),
            'title': self.copmenu.Title(),
            'titlemenu': self.copmenu.titlemenu,
            'order': self.view.get_menu_index(self.copmenu.titlemenu),
        })
        self.assertEqual(self.view.set_cop_menu(), cop_menu)

    def test_set_cop_banner(self):
        banner = {}
        banner['title'] = self.cop.Title()
        banner['description'] = self.cop.Description()
        banner['image_url'] = ''
        self.assertEqual(self.view.set_cop_banner(), banner)
        stringio = StringIO(zptlogo)
        self.copcommunityfolder.setImagem(stringio)
        banner['image_url'] = \
            self.copcommunityfolder.getImagem().absolute_url()
        self.assertEqual(self.view.set_cop_banner(), banner)

    def test_set_cop_participation_info(self):
        self.view.get_cop_context()
        self.assertEqual(self.view.set_cop_participation_info(), 'Owner')
        self.changeLoggedMember()
        self.assertEqual(self.view.set_cop_participation_info(), '')

    def test_set_banner_review_state(self):
        self.assertEqual(
            self.view.set_banner_review_state(),
            {'id': 'restrito', 'title': 'Restrito'},
        )
        self.portal.portal_workflow.doActionFor(self.cop, 'publico')
        self.assertEqual(
            self.view.set_banner_review_state(),
            {'id': 'publico', 'title': 'Publico'},
        )

    def test_set_context_actions(self):
        self.view = ViewCoPBase(self.copmenu, self.request)
        ids = [item['id'] for item in self.view.set_context_actions()]
        self.assertItemsEqual(
            ids, ['edit', 'cut', 'copy', 'delete', 'rename']
        )

    def test_set_context_workflow(self):
        context_workflow = self.view.set_context_workflow()
        review_state = context_workflow['review_state']
        transitions = [t['id'] for t in context_workflow['transitions']]
        self.assertEqual(
            review_state, {'id': 'restrito', 'title': 'Restrito'},
        )
        self.assertItemsEqual(
            transitions, ['privado', 'publico']
        )

        self.portal.portal_workflow.doActionFor(self.cop, 'publico')
        context_workflow = self.view.set_context_workflow()
        review_state = context_workflow['review_state']
        transitions = [t['id'] for t in context_workflow['transitions']]
        self.assertEqual(
            review_state, {'id': 'publico', 'title': 'Publico'},
        )
        self.assertItemsEqual(
            transitions, ['privado', 'restrito']
        )

    def test_set_context_info(self):
        context_info = self.view.set_context_info()
        self.assertEqual(
            context_info['url'], self.view.context.absolute_url()
        )
        self.view = ViewCoPBase(self.copcommunityfolder, self.request)
        context_info = self.view.set_context_info()
        self.assertEqual(
            context_info['url'], self.view.context.absolute_url()
        )

    def test_set_context_title(self):
        context_title = self.view.set_context_title()
        self.assertEqual(context_title['title'], self.view.context.Title())
        self.view.context.setTitle('New title')
        context_title = self.view.set_context_title()
        self.assertEqual(context_title['title'], self.view.context.Title())

    def test_get_addable_types(self):
        addable_types = [item['id'] for item in self.view.get_addable_types()]
        self.assertEqual(addable_types, ['CoPMenu'])
        self.view = ViewCoPBase(self.copcommunityfolder, self.request)
        addable_types = [item['id'] for item in self.view.get_addable_types()]
        self.assertItemsEqual(addable_types, ['CoP'])

    def test_get_filter_terms(self):
        self.assertIn(('', 'Pesquisar'), self.view.get_filter_terms())
        self.view = ViewCoPBase(self.copmenu, self.request)
        self.assertIn(('', 'Ver todos'), self.view.get_filter_terms())

    def test_get_selected_filter(self):
        self.assertEqual(self.view.get_selected_filter(), 'Pesquisar')
        self.view = ViewCoPBase(self.copmenu, self.request)
        self.assertEqual(self.view.get_selected_filter(), 'Ver todos')
        self.assertEqual(
            self.view.get_selected_filter('CoPDocument'), u'Pagina'
        )
        self.assertEqual(self.view.get_selected_filter('Dio'), 'Ver todos')

    def test_get_cop_search(self):
        self.create_basic_content()
        self.assertEqual(len(self.view.get_cop_search()), 3)
        self.assertEqual(
            [i.UID for i in self.view.get_cop_search()],
            [i.UID for i in getCoPUpdates(self.view.context)]
        )
        self.view.request['filter_term'] = 'CoPDocument'
        self.assertEqual(len(self.view.get_cop_search()), 1)
        self.assertEqual(
            [i.UID for i in self.view.get_cop_search()],
            [i.UID for i in getCoPUpdates(
                self.view.context, 0, 'CoPDocument'
            )]
        )
        self.view.request['SearchableText'] = 'CoPDocument'
        self.view.request['UID'] = self.copdocument.UID()
        self.assertEqual(
            [i.UID for i in self.view.get_cop_search(1)],
            [i.UID for i in getCoPUpdates(
                self.view.context, 1, 'CoPDocument',
                'CoPDocument', self.copdocument.UID()
            )]
        )

    def test_get_folder_content(self):
        self.create_basic_content()
        self.copdocument.setTitle('New page')
        self.copdocument.reindexObject()
        self.copimage.setTitle('Image test')
        self.copimage.reindexObject()
        self.view = ViewCoPBase(self.copfolder, self.request)
        folder_content = self.view.get_folder_content()
        self.assertEqual(len(folder_content), 3)
        self.view.request['SearchableText'] = 'New'
        folder_content = self.view.get_folder_content()
        self.assertEqual(folder_content[0].portal_type, 'CoPDocument')
        self.view.request['SearchableText'] = None
        self.view.request['filter_term'] = 'CoPImage'
        folder_content = self.view.get_folder_content()
        self.assertEqual(folder_content[0].Title, 'Image test')

    def test_get_state_name(self):
        self.assertEqual(
            self.view.get_state_name('CoP', 'restrito'), 'Restrito'
        )
        self.assertEqual(
            self.view.get_state_name('CoPPortfolio', 'bloqueado'), 'Bloqueado'
        )

    def test_get_brain_values(self):
        self.create_basic_content()
        self.view = ViewCoPBase(self.copfolder, self.request)
        cop_search = self.view.get_cop_search()
        brain_values_view = self.view.get_brain_values(cop_search)
        brain_values = getBrainValues(self.view.context, cop_search)
        self.assertEqual(brain_values_view, brain_values)

    def test_is_discussion_allowed(self):
        self.create_basic_content()
        self.view = ViewCoPBase(self.copdocument, self.request)
        self.assertEqual(
            self.view.is_discussion_allowed(self.cop.UID()),
            isDiscussionAllowed(self.copdocument, self.cop.UID())
        )
        self.changeLoggedMember()
        self.assertEqual(
            self.view.is_discussion_allowed(self.cop.UID()),
            isDiscussionAllowed(self.copdocument, self.cop.UID())
        )

    def test_get_commentaries(self):
        self.create_basic_content()
        self.createComments(self.copdocument, 6)
        path = '/'.join(self.copdocument.getPhysicalPath())
        commentaries = self.view.get_commentaries(path, 4)
        self.assertEqual(len(commentaries), 4)
        commentaries = self.view.get_commentaries(path)
        self.assertEqual(len(commentaries), 6)

    def test_get_folder_contents_header(self):
        header = self.view.get_folder_contents_header()
        self.assertEqual(header[0]['sort_order'], 'ascending')
        self.view.request['sort_on'] = 'sortable_title'
        self.view.request['sort_order'] = 'ascending'
        header = self.view.get_folder_contents_header()
        self.assertEqual(header[0]['sort_order'], 'reverse')

    def test_get_search_parameter(self):
        self.assertEqual(self.view.get_search_parameter(), 'SearchableText')

    def test_is_anonymous(self):
        self.assertFalse(self.view.is_anonymous())
        self.changeLoggedMember()
        self.assertTrue(self.view.is_anonymous())
