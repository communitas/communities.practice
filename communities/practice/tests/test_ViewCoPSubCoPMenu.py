# -*- coding: utf-8 -*-
from communities.practice.tests.CoPBase import IntegrationTestCase
from communities.practice.browser.ViewCoPBase import ViewCoPBase


class ViewCoPSubCoPMenuTestCase(IntegrationTestCase):

    def setUp(self):
        super(ViewCoPSubCoPMenuTestCase, self).setUp()
        self.factorySubCoPMenu()
        self.view = self.getView(self.subcopmenu, 'viewCoPSubCoPMenu')

    def test_set_cop_menu(self):
        context_menu = self.view.set_cop_menu()
        base_menu = ViewCoPBase.set_cop_menu(self.view)
        self.assertEqual(context_menu, base_menu)
