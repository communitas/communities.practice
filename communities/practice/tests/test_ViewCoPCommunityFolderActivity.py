# -*- coding: utf-8 -*-
from datetime import datetime

from communities.practice.config import OPCOES
from communities.practice.generics.atividade_generics import \
    getCoPCommunityFolderAtividadeTypes, getCoPAtividadeCommunities
from communities.practice.tests.CoPBase import IntegrationTestCase


class ViewCoPCommunityFolderActivityTestCase(IntegrationTestCase):

    def setUp(self):
        super(ViewCoPCommunityFolderActivityTestCase, self).setUp()
        self.factorySubCoP()
        self.cop.setTarefas_input(OPCOES[0])
        self.addUsersCoPActivity(self.cop)
        self.createContentActivity(self.cop)
        self.view = self.getView(
            self.copcommunityfolder, 'viewCoPCommunityFolderActivity'
        )

    def test_set_cop_menu_selected(self):
        self.assertEqual(
            self.view.set_cop_menu_selected(), 'copcommunityfolderatividade'
        )

    def test_set_context_title(self):
        self.view.set_context_title()
        self.assertEqual(self.view.context_title['title'], 'Atividade')

    def test_get_types(self):
        self.assertItemsEqual(
            self.view.get_types(),
            getCoPCommunityFolderAtividadeTypes()
        )

    def test_check_permission(self):
        self.assertTrue(self.view.check_permission())
        self.changeLoggedMember()
        self.assertFalse(self.view.check_permission())

    def test_get_communities(self):
        communities = self.view.get_communities()
        uids_view = [community['brain'].UID for community in communities]
        communities = getCoPAtividadeCommunities(self.view.context)
        uids_generics = [community['brain'].UID for community in communities]
        self.assertEqual(uids_view, uids_generics)

    def test_get_total_types(self):
        total_types = self.view.get_total_types()
        self.assertEqual(
            total_types[0]['total_content'][('total', u'Total')], 5,
        )
        self.assertEqual(
            total_types[0]['total_content'][('CoPDocument', u'Páginas')], 3,
        )
        self.assertEqual(
            total_types[0]['total_content'][('CoPFile', u'Arquivos')], 1,
        )

    def test_export_data(self):
        self.request.form = {'submit_export_atividades': 'Exportar'}
        self.view.export_data()
        filename = "Atividades_%s_%s.csv" % (
            self.copcommunityfolder.Title(),
            datetime.now().strftime("%d_%m_%Y"),
        )
        headers = self.request.response.headers
        self.assertEqual(headers.get("content-length", None), "0")
        self.assertEqual(headers.get("Content-Type", None), "text/csv")
        self.assertEqual(
            headers.get("content-disposition", None),
            'attachment; filename="%s"' % filename,
        )
        self.assertEqual(self.request.response.status, 200)
