# -*- coding: utf-8 -*-
from communities.practice.tests.CoPBase import IntegrationTestCase
from communities.practice.config import OPCOES
from communities.practice.generics.atividade_generics import \
    getCoPAtividadeTypes
from DateTime import DateTime
from datetime import datetime
from datetime import timedelta
from Products.CMFPlone.utils import getToolByName


class ViewCoPActivityContentTestCase(IntegrationTestCase):

    def setUp(self):
        super(ViewCoPActivityContentTestCase, self).setUp()
        self.factoryCoPMenu()
        self.cop.setTarefas_input(OPCOES[0])
        self.addUsersCoPActivity(self.cop)
        self.createContentActivity(self.cop, 'user_participante1')
        self.view = self.getView(self.copmenu, 'viewCoPActivityContent')
        self.request['user'] = ''
        self.request['type_id'] = ''
        self.request['type_title'] = ''
        self.request['folder'] = ''
        self.request['period_start'] = ''
        self.request['period_end'] = ''

    def test_get_conteudo(self):
        portal_catalog = getToolByName(self.cop, 'portal_catalog')
        cop_path = '/'.join(self.cop.getPhysicalPath())
        query = {}
        query['path'] = cop_path
        tipos_consulta = [
            tipo[0] for tipo in getCoPAtividadeTypes('comunidade', False)
        ]
        query['portal_type'] = tipos_consulta
        conteudo_catalog = portal_catalog(query)

        # total de conteudos na comunidade
        self.request['folder'] = ''
        self.request['type_id'] = 'total'
        self.request['type_title'] = 'Total'
        conteudo = self.view.get_conteudo()
        self.assertEqual(len(conteudo), len(conteudo_catalog))

        # total no acervo
        self.request['folder'] = 'acervo'
        tipos_consulta = [
            tipo[0] for tipo in getCoPAtividadeTypes('acervo', False)
        ]
        query['portal_type'] = tipos_consulta
        query['path'] = '%s/acervo' % cop_path
        conteudo_catalog = portal_catalog(query)
        conteudo = self.view.get_conteudo()
        self.assertEqual(len(conteudo), len(conteudo_catalog))

        # total no acervo com periodo
        old_date = self.cop.acervo.created()
        self.cop.acervo.boas_vindas.setCreationDate(
            DateTime(datetime.now() - timedelta(days=1))
        )
        self.cop.acervo.boas_vindas.reindexObject()
        today = datetime.today()
        today = DateTime('%s/%s/%s' % (today.year, today.month, today.day))
        start_period = today
        end_period = today + 1
        query['created'] = {
            'query': [start_period, end_period],
            'range': 'min:max',
        }
        conteudo_catalog = portal_catalog(query)
        self.request['period_start'] = '%s-%s-%s 00:00' % (
            start_period.year(), start_period.mm(), start_period.dd()
        )
        self.request['period_end'] = '%s-%s-%s 00:00' % (
            end_period.year(), end_period.mm(), end_period.dd()
        )
        conteudo = self.view.get_conteudo()
        self.assertEqual(len(conteudo), len(conteudo_catalog))
        query.pop('created')
        self.request['period_start'] = ''
        self.request['period_end'] = ''
        self.cop.acervo.boas_vindas.setCreationDate(old_date)
        self.cop.acervo.boas_vindas.reindexObject()

        # total no portfolio
        self.request['folder'] = 'portfolio'
        tipos_consulta = [
            tipo[0] for tipo in getCoPAtividadeTypes('portfolio', False)
        ]
        query['portal_type'] = tipos_consulta
        query['path'] = '%s/portfolio' % cop_path
        conteudo_catalog = portal_catalog(query)
        conteudo = self.view.get_conteudo()
        self.assertEqual(len(conteudo), len(conteudo_catalog))

        # total nas tarefas
        self.request['folder'] = 'tarefas'
        tipos_consulta = [
            tipo[0] for tipo in getCoPAtividadeTypes('tarefas', False)
        ]
        query['portal_type'] = tipos_consulta
        query['path'] = '%s/tarefas' % cop_path
        conteudo_catalog = portal_catalog(query)
        conteudo = self.view.get_conteudo()
        self.assertEqual(len(conteudo), len(conteudo_catalog))

        # total de tipo na comunidade
        self.request['folder'] = 'comunidade'
        self.request['type_id'] = 'CoPDocument'
        self.request['type_title'] = u'Página'
        query['path'] = cop_path
        query['portal_type'] = "CoPDocument"
        conteudo_catalog = portal_catalog(query)
        conteudo = self.view.get_conteudo()
        self.assertEqual(len(conteudo), len(conteudo_catalog))

        # total de tipo no acervo
        self.request['folder'] = 'acervo'
        query['path'] = '%s/acervo' % cop_path
        conteudo_catalog = portal_catalog(query)
        conteudo = self.view.get_conteudo()
        self.assertEqual(len(conteudo), len(conteudo_catalog))

        # total de tipo no portfolio
        self.request['folder'] = 'portfolio'
        query['path'] = '%s/portfolio' % cop_path
        conteudo_catalog = portal_catalog(query)
        conteudo = self.view.get_conteudo()
        self.assertEqual(len(conteudo), len(conteudo_catalog))

        # total de tipo nas tarefas
        self.request['folder'] = 'tarefas'
        self.request['type_id'] = 'CoPFile'
        self.request['type_title'] = u'Arquivos'
        query['path'] = '%s/tarefas' % cop_path
        query['portal_type'] = 'CoPFile'
        conteudo_catalog = portal_catalog(query)
        conteudo = self.view.get_conteudo()
        self.assertEqual(len(conteudo), len(conteudo_catalog))

        # total de member na comunidade
        self.request['folder'] = 'comunidade'
        self.request['type_id'] = 'total'
        self.request['type_title'] = u'Total'
        self.request['user'] = 'user_participante1'
        query['path'] = cop_path
        tipos_consulta = [
            tipo[0] for tipo in getCoPAtividadeTypes('comunidade', False)
        ]
        query['portal_type'] = tipos_consulta
        query['Creator'] = 'user_participante1'
        conteudo_catalog = portal_catalog(query)
        conteudo = self.view.get_conteudo()
        self.assertEqual(len(conteudo), len(conteudo_catalog))

        # total de member no acervo
        self.request['folder'] = 'acervo'
        query['path'] = '%s/acervo' % cop_path
        tipos_consulta = [
            tipo[0] for tipo in getCoPAtividadeTypes('acervo', False)
        ]
        query['portal_type'] = tipos_consulta
        conteudo_catalog = portal_catalog(query)
        conteudo = self.view.get_conteudo()
        self.assertEqual(len(conteudo), len(conteudo_catalog))

        # total de member no portfolio
        self.request['folder'] = 'portfolio'
        query['path'] = '%s/portfolio' % cop_path
        tipos_consulta = [
            tipo[0] for tipo in getCoPAtividadeTypes('portfolio', False)
        ]
        query['portal_type'] = tipos_consulta
        conteudo_catalog = portal_catalog(query)
        conteudo = self.view.get_conteudo()
        self.assertEqual(len(conteudo), len(conteudo_catalog))

        # total de member nas tarefas
        self.request['folder'] = 'tarefas'
        query['path'] = '%s/tarefas' % cop_path
        tipos_consulta = [
            tipo[0] for tipo in getCoPAtividadeTypes('tarefas', False)
        ]
        query['portal_type'] = tipos_consulta
        conteudo_catalog = portal_catalog(query)
        conteudo = self.view.get_conteudo()
        self.assertEqual(len(conteudo), len(conteudo_catalog))

        # total de tipo member na comunidade
        self.request['folder'] = 'comunidade'
        self.request['type_id'] = 'CoPDocument'
        self.request['type_title'] = u'Páginas'
        query['path'] = cop_path
        query['portal_type'] = 'CoPDocument'
        conteudo_catalog = portal_catalog(query)
        conteudo = self.view.get_conteudo()
        self.assertEqual(len(conteudo), len(conteudo_catalog))

        # total de tipo member no acervo
        self.cop.acervo.copdocument_1.setTitle('')
        self.cop.acervo.copdocument_1.reindexObject()
        self.request['folder'] = 'acervo'
        query['path'] = '%s/acervo' % cop_path
        conteudo_catalog = portal_catalog(query)
        conteudo = self.view.get_conteudo()
        self.assertEqual(len(conteudo), len(conteudo_catalog))

        # total de tipo member no portfolio
        self.request['folder'] = 'portfolio'
        query['path'] = '%s/portfolio' % cop_path
        conteudo_catalog = portal_catalog(query)
        conteudo = self.view.get_conteudo()
        self.assertEqual(len(conteudo), len(conteudo_catalog))

        # total de tipo member nas tarefas
        self.request['folder'] = 'tarefas'
        self.request['type_id'] = 'CoPFile'
        self.request['type_title'] = u'Arquivos'
        query['path'] = '%s/tarefas' % cop_path
        query['portal_type'] = 'CoPFile'
        conteudo_catalog = portal_catalog(query)
        conteudo = self.view.get_conteudo()
        self.assertEqual(len(conteudo), len(conteudo_catalog))
