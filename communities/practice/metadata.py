# -*- coding: utf-8 -*-
from Acquisition import aq_parent, aq_inner, aq_chain
from plone.indexer.decorator import indexer

from communities.practice.generics.generics import getCoPContext
from communities.practice.interfaces import ICoP
from communities.practice.interfaces import ICoPContent
from communities.practice.interfaces import ICoPFolder
from communities.practice.interfaces import ICoPMenu
from communities.practice.interfaces import ICoPPortfolio
from communities.practice.interfaces import ICoPUpload


def partOfCommunity(object, **kw):
    """ Retorna uma tupla com ID,URL de uma Comunidade ou False
    """
    cop = getCoPContext(object, ['CoP', ])
    if cop:
        return (cop.UID(), cop.Title(), cop.absolute_url())
    else:
        return ''


@indexer(ICoPMenu)
def partOfCommunityICoPMenu(object, **kw):
    return partOfCommunity(object, **kw)


@indexer(ICoPFolder)
def partOfCommunityICoPFolder(object, **kw):
    return partOfCommunity(object, **kw)


@indexer(ICoPContent)
def partOfCommunityICoPContent(object, **kw):
    return partOfCommunity(object, **kw)


@indexer(ICoPPortfolio)
def partOfCommunityICoPPortfolio(object, **kw):
    return partOfCommunity(object, **kw)


@indexer(ICoPUpload)
def partOfCommunityICoPUpload(object, **kw):
    return partOfCommunity(object, **kw)


@indexer(ICoP)
def partOfCommunityICoP(object, **kw):
    return partOfCommunity(aq_parent(aq_inner(object)), **kw)


@indexer(ICoP)
def acceptsSharesICoP(object, **kw):
    if object.getParticipar_input() == 'Habilitar':
        return True
    return ''


@indexer(ICoP)
def imageCoPICoP(object, **kw):
    for parent in aq_chain(aq_inner(object)):
        if hasattr(parent, 'getImagem') and parent.getImagem():
            imagem = parent.getImagem()
            return "/".join(imagem.getPhysicalPath())
    return ''


@indexer(ICoP)
def copSubCoPNumber(object, **kw):
    subcops = 0
    if 'subcop' in object.keys():
        subcops = len(object.subcop.contentItems())
    return subcops
