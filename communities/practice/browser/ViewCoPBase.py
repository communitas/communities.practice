# -*- coding: utf-8 -*-
from Products.Five.browser import BrowserView
from zope.component import getMultiAdapter, getUtility
from Products.CMFCore.utils import getToolByName
from zope.browsermenu.interfaces import IBrowserMenu
from communities.practice.generics.generics import getBrainValues
from communities.practice.generics.generics import getCommentaries
from communities.practice.generics.generics import getCoPContext
from communities.practice.generics.generics import getCoPUpdates
from communities.practice.generics.generics import getMemberData
from communities.practice.generics.generics import getReviewStateDescription
from communities.practice.generics.generics import getReviewStateTitle
from communities.practice.generics.generics import isDiscussionAllowed
from communities.practice.generics.generics import isSubCoP
from communities.practice.generics.generics import getSuperCoP
from communities.practice.generics.cache import \
    listMemberRoleCommunitiesParticipating
from zope.component.hooks import getSite


class ViewCoPBase(BrowserView):
    """ Base View for Communities of Practice"""

    def get_cop_context(self):
        """ Sets copcontext only once to each request
        """
        if not hasattr(self, 'cop_context'):
            self.cop_context = getCoPContext(self.context)
        return self.cop_context

    def set_palette_color(self):
        """ Sets palette color using color property.
        """
        return getattr(self.context, "color", "gray")

    def set_cop_menu_selected(self, title_menu=""):
        """ Sets seleted CoPMenu using titlemenu property.
        """
        if title_menu:
            self.cop_menu_selected = title_menu
        else:
            self.get_cop_context()
            if self.context.portal_type in ['CoP', 'CoPCommunityFolder']:
                self.cop_menu_selected = 'home'
            else:
                cop_menu = getCoPContext(self.context, ['CoPMenu'])
                self.cop_menu_selected = cop_menu.titlemenu
        return self.cop_menu_selected

    def get_menu_index(self, title_menu):
        """ Used to sort CoPMenu.
        """
        ordered_menu = [
            'home', 'subcop', 'acervo',
            'calendario', 'portfolio', 'forum',
            'curso', 'formularios', 'chat',
            'tarefas', 'atividade', 'notificacoes',
            'configuracoes',
        ]
        return ordered_menu.index(title_menu) \
            if title_menu in ordered_menu else -1

    def set_cop_menu(self):
        """ Sets CoPMenu items by a catalog search.
            Sort by get_menu_index order.
        """
        self.get_cop_context()
        self.cop_menu = []
        cop_home = {
            'id': 'home',
            'url': self.cop_context.absolute_url(),
            'title': 'Início',
            'titlemenu': 'home',
            'order': self.get_menu_index('home'),
        }
        self.cop_menu.append(cop_home)

        catalog = getToolByName(self.context, 'portal_catalog')
        query = {}
        context_path = '/'.join(self.cop_context.getPhysicalPath())
        query['path'] = {'query': context_path, 'depth': 1}
        query['portal_type'] = 'CoPMenu'
        menu_itens = catalog(query)
        for item in menu_itens:
            self.cop_menu.append({
                'id': item.id,
                'url': item.getURL(),
                'title': item.Title,
                'titlemenu': item.titlemenu,
                'order': self.get_menu_index(item.titlemenu),
            })
        self.cop_menu.sort(key=lambda x: x['order'])
        return self.cop_menu

    def set_cop_banner(self):
        """ Sets image, title and description for
            CoP and CoPCommunityFolder banner
        """
        self.get_cop_context()
        self.banner = {}
        self.banner['title'] = self.cop_context.Title()
        self.banner['description'] = self.cop_context.Description()
        cop_image = self.cop_context.getImagem()
        if not cop_image:
            parent = getCoPContext(self.context, ['CoPCommunityFolder'])
            cop_image = parent.getImagem()
        if cop_image:
            self.banner['image_url'] = cop_image.absolute_url()
        else:
            self.banner['image_url'] = ''
        return self.banner

    def set_cop_participation_info(self):
        """ Sets participation status for CoP banner
        """
        portal_membership = getToolByName(self.context, 'portal_membership')
        user = portal_membership.getAuthenticatedMember()
        cop_roles = listMemberRoleCommunitiesParticipating(user.getId())
        self.participation_info = ""
        cop_list = cop_roles.get(user.getId())
        if cop_list:
            self.participation_info = cop_list.get(self.cop_context.UID())
        return self.participation_info

    def set_banner_review_state(self):
        """ Sets workflow review state name for CoP banner.
        """
        self.get_cop_context()
        self.banner_review_state = getReviewStateTitle(self.cop_context)
        return self.banner_review_state

    def set_context_actions(self, context=None):
        if not context:
            context = self.context
        self.context_actions = []
        context_state = getMultiAdapter(
            (context, self.request),
            name='plone_context_state'
        )
        if context_state.is_editable():
            self.context_actions.append({
                'id': 'edit',
                'title': 'Edit',
                'url': "%s/edit" % (context.absolute_url()),
            })
        edit_actions = context_state.actions('object_buttons')
        for action in edit_actions:
            self.context_actions.append({
                'id': action['id'],
                'title': action['title'],
                'url': action['url'],
            })
        return self.context_actions

    def set_context_workflow(self):
        self.context_workflow = {}
        self.context_workflow['review_state'] = getReviewStateTitle(
            self.context
        )
        wf_tool = getToolByName(self.context, 'portal_workflow')
        self.context_workflow['transitions'] = wf_tool.listActionInfos(
            object=self.context
        )
        for transition in self.context_workflow['transitions']:
            transition['description'] = getReviewStateDescription(
                transition['id']
            )
        return self.context_workflow

    def set_context_info(self):
        """ Set creator, modification date and
            review state for the context.
        """
        portal = getSite()
        creator_id = self.context.Creator()
        creator_name, creator_email = getMemberData(portal, creator_id)
        creator_url = "%s/author/%s" % (
            portal.absolute_url(),
            creator_id
        )
        self.context_info = {}
        self.context_info['creator_name'] = creator_name
        self.context_info['creator_url'] = creator_url
        self.context_info['modified'] = self.context.modified()
        self.context_info['url'] = self.context.absolute_url()
        return self.context_info

    def set_context_title(self):
        """ Set title and description for the context
        """
        self.context_title = {}
        self.context_title['title'] = self.context.Title() or self.context.id
        self.context_title['description'] = self.context.Description()
        return self.context_title

    def get_addable_types(self):
        """ Addadble types
        """
        browser_menu = getUtility(
            IBrowserMenu,
            name='plone_contentmenu_factory',
        )
        items = browser_menu.getMenuItems(self.context, self.request)
        self.addable_types = []
        for cop_type in items:
            title = cop_type['title']
            if title not in [u'folder_add_settings', u'folder_add_more']:
                self.addable_types.append({
                    'id': cop_type['id'],
                    'url': cop_type['action'],
                    'title': title,
                })
        return self.addable_types

    def get_filter_terms(self):
        self.filter_terms = []
        addable_types = self.get_addable_types()
        if len(addable_types) > 1:
            self.filter_terms.append(('', 'Ver todos'))
            for cop_type in addable_types:
                self.filter_terms.append((cop_type['id'], cop_type['title']))
        else:
            self.filter_terms.append(('', 'Pesquisar'))
        return self.filter_terms

    def get_selected_filter(self, selected_term=""):
        if not getattr(self, 'filter_terms', False):
            self.get_filter_terms()
        for term_id, term_title in self.filter_terms:
            if term_id == selected_term:
                return term_title
        term_title = ''
        if self.filter_terms:
            term_title = self.filter_terms[0][1]
        return term_title

    def get_cop_search(self, limit=0):
        """
            Get CoP news to show on timeline.
        """
        search_term = self.request.get("SearchableText", None)
        portal_type = self.request.get("filter_term", None)
        UID = self.request.get("UID", None)
        return getCoPUpdates(
            self.context, limit, portal_type, search_term, UID
        )

    def get_folder_content(self, limit=0):
        """ Get context content."""
        portal_membership = getToolByName(self.context, 'portal_membership')
        catalog = getToolByName(self.context, 'portal_catalog')
        search_term = self.request.get("SearchableText", None)
        filter_term = self.request.get("filter_term", None)
        sort_on = self.request.get("sort_on", 'modified')
        sort_order = self.request.get("sort_order", 'reverse')
        query = {}
        if search_term:
            query['SearchableText'] = "%s*" % search_term
        if filter_term:
            query['portal_type'] = filter_term
        if sort_on in ['sortable_title', 'Creator',
                       'modified', 'review_state', ]:
            query['sort_on'] = sort_on
        if sort_order in ['ascending', 'reverse']:
            query['sort_order'] = sort_order
        folder_path = '/'.join(self.context.getPhysicalPath())
        query['path'] = {'query': folder_path, 'depth': 1}
        show_inactive = portal_membership.checkPermission(
            'Access inactive portal content',
            self.context,
        )

        results = catalog(query, show_all=1, show_inactive=show_inactive)
        return results

    def get_state_name(self, portal_type, review_state):
        """Get object state name from state id."""
        portal_workflow = getToolByName(self.context, 'portal_workflow')
        state_title = portal_workflow.getTitleForStateOnType(
            review_state,
            portal_type,
        )
        return state_title

    def get_brain_values(self, news):
        """
            Get values for a brain list.
        """
        return getBrainValues(self.context, news)

    def is_discussion_allowed(self, cop_uid):
        return isDiscussionAllowed(self.context, cop_uid)

    def get_commentaries(self, path, qtd=0):
        """
            Busca Comentarios a partir do path.
            Pode ser limitado atraves do parametro qtd.
            Retorna no formato lista de dicionarios.
        """
        self.see_more = False
        commentaries = getCommentaries(self.context, path)
        if qtd and len(commentaries) > qtd:
            self.see_more = True
            return commentaries[len(commentaries)-qtd:]
        return commentaries

    def get_folder_contents_header(self):
        header = []
        sort_on = self.request.get('sort_on', '')
        sort_order = self.request.get('sort_order', '')
        header.append({'title': 'Título', 'sort_on': 'sortable_title'})
        header.append({'title': 'Postado por', 'sort_on': 'Creator'})
        header.append({'title': 'Modificado', 'sort_on': 'modified'})
        header.append({'title': 'Estado', 'sort_on': 'review_state'})
        for item in header:
            order = 'ascending'
            if sort_on == item['sort_on'] and sort_order == 'ascending':
                order = 'reverse'
            item['sort_order'] = order
        return header

    def get_search_parameter(self):
        return 'SearchableText'

    def is_anonymous(self):
        portal_membership = getToolByName(self.context, 'portal_membership')
        return portal_membership.isAnonymousUser()

    def get_participation_allowed(self, context_UID = None):
        context = self.context
        if context_UID:
            catalog = getToolByName(self.context, 'portal_catalog')
            brain = catalog(UID=context_UID)
            if brain:
                context = brain[0].getObject()
        super_cop_participation = True
        if isSubCoP(context):
            super_cop = getSuperCoP(context)
            portal_membership = getToolByName(
                context, 'portal_membership'
            )
            authenticated = portal_membership.getAuthenticatedMember()
            authenticated_member_id = authenticated.getId()
            member_communities = listMemberRoleCommunitiesParticipating(
                authenticated_member_id
            )
            communities_participating = member_communities.get(
                authenticated_member_id
            )
            if not communities_participating or \
               not communities_participating.get(super_cop.UID()) in \
               ['Owner', 'Moderador', 'Participante']:
                super_cop_participation = False
        participar = ""
        if context.get_participar_habilitado() and \
           super_cop_participation:
            participar = "%s/viewCoPJoin" % (context.absolute_url())
        return participar
