# -*- coding: utf-8 -*-
from Products.Five.browser import BrowserView
from communities.practice.generics.generics import \
    getCoPRole, getMemberData, getUsersByRole
from communities.practice.generics.generics import getCoPSettings
from communities.practice.generics.asyncmail import \
    createMessage, sendAsyncMail
from Products.CMFCore.utils import getToolByName
from communities.practice.generics.generics import setMembersCoPRole


class ViewCoPJoinButton(BrowserView):
    """Default View for Communities of Practice"""

    def send_participation_request(self):
        form = self.request.form
        portal_membership = getToolByName(self.context, 'portal_membership')
        portal_workflow = getToolByName(self.context, 'portal_workflow')
        review_state = portal_workflow.getInfoFor(
            self.context, "review_state", ""
        )
        if ('submit' in form.keys() or review_state == "publico") and not portal_membership.isAnonymousUser():
            cop = self.context
            user = portal_membership.getAuthenticatedMember()
            user_id = user.getId()
            if not getCoPRole(cop.UID(), user_id):
                message = form.get("message")
                status_message = ''
                if cop.get_moderar_habilitado():
                    setMembersCoPRole(
                        self.context, [user_id], "Aguardando", False
                    )
                    self.send_notification_mail(message)
                    status_message = u"""Sua solicitação de participação
                        foi encaminhada para o moderador da comunidade
                    """
                else:
                    setMembersCoPRole(self.context, [user_id], "Participante")
                    status_message = u'Você foi adicionado como participante'
                    # futuramente o envio podera ser configurado
                    # nas configuracoes de uma CoP
                    # self.send_notification_mail(message, publica = True)
                if status_message:
                    self.context.plone_utils.addPortalMessage(
                        status_message, 'info'
                    )
            return self.request.response.redirect(self.context.absolute_url())
        return ""

    def send_notification_mail(self, requestor_message, publica=False):
        """Send message for moderators."""
        community = self.context
        portal_membership = getToolByName(community, 'portal_membership')
        community_title = community.Title()
        community_url = community.absolute_url()
        # Requestor data
        requestor_id = portal_membership.getAuthenticatedMember().getId()
        requestor_data = getMemberData(community, requestor_id)
        # Moderators data
        roles = ['Owner', 'Moderador', 'Moderador_Master']
        list_moderators = getUsersByRole(community, roles)
        list_mail_to = []
        for role in roles:
            for user in list_moderators[role]:
                list_mail_to.append(user['email'])
        # message
        settings = getCoPSettings()
        mail_from = settings.username
        subject = "Novo membro da comunidade %s" % (community_title)
        message = 'Content-Type: text/plain; charset="UTF-8"'
        message += '\nMIME-Version: 1.0\n\n'

        # futuramente o envio podera ser configurado nas configuracoes de uma CoP
        # if publica:
        #    message += """O usuário %s é o mais novo membro da comunidade %s\nPara acessar a comunidade clique em: %s""" % (
        #                   requestor_data[0], community_title, community_url)
        # else:
        #    message += """O usuário %s solicitou permissão para participar da comunidade %s.\nPara permitir o acesso clique no link abaixo.\n %s/configuracoes#tab_cop_part_pendentes""" % (requestor_data[0], community_title, community_url)

        message += """
            O usuário %s solicitou permissão para participar
            da comunidade %s.\nPara permitir o acesso clique
            no link abaixo.\n %s/configuracoes#tab_cop_part_pendentes
        """ % (requestor_data[0], community_title, community_url)
        if requestor_message:
            message += "\n\nMensagem deixada por %s:\n%s\n" % (
                requestor_data[0], requestor_message
            )
        createMessage(
            mail_from, list_mail_to, subject,
            message, self.request.getURL())
        sendAsyncMail()
