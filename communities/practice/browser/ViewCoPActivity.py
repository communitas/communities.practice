# -*- coding: utf-8 -*-
from communities.practice.browser.ViewCoPBase import ViewCoPBase
from communities.practice.generics.generics import getCoPContext
from communities.practice.generics.atividade_generics import getCoPAtividadeMembers
from communities.practice.generics.atividade_generics import getCoPAtividadeContent
from communities.practice.generics.atividade_generics import getCoPAtividadeTypes
from communities.practice.generics.atividade_generics import getCoPAtividadeTotalTypes
from communities.practice.generics.atividade_generics import getCoPAtividadeLenMembers
from communities.practice.generics.atividade_generics import getStartEndDate
from zope.security import checkPermission

from urllib import quote


class ViewCoPActivity(ViewCoPBase):
    """Default view for users content.
    """

    def get_community_members(self):
        """Retorna os membros da comunidade
        """
        search_term = self.request.get('SearchableText', None)
        community = getCoPContext(self.context)
        memberlist = getCoPAtividadeMembers(community, search_term)
        return memberlist

    def get_community_content(self, member_list):
        """Retorna conteudo da comunidade ordenado por participante.
        """
        community = getCoPContext(self.context)
        period = getStartEndDate(self.request)
        folder = self.request.get('filter_term', 'comunidade')
        folder = folder if folder else 'comunidade'
        content = getCoPAtividadeContent(
            community, member_list, folder, period
        )
        return content

    def get_types(self):
        """Retorna os tipos de conteudo a serem considerados
           na view de atividades
        """
        folder = self.request.get('filter_term', 'comunidade')
        folder = folder if folder else 'comunidade'
        types = getCoPAtividadeTypes(folder, True, self.context)
        return types

    def check_permission(self):
        return checkPermission(
            'communities.practice.ObserveCoP', self.context
        )

    def get_total_types(self):
        """Retorna os totais de cada tipo de conteudo da comunidade
        """
        community = getCoPContext(self.context)
        period = getStartEndDate(self.request)
        folder = self.request.get('filter_term', 'comunidade')
        folder = folder if folder else 'comunidade'
        total_content = getCoPAtividadeTotalTypes(community, folder, period)
        return total_content

    def get_filter_terms(self):
        self.filter_terms = [
            ('comunidade', 'Comunidade'),
            ('acervo', 'Acervo'),
            ('portfolio', 'Portfolio'),
            ('forum', 'Fórum'),
            ('tarefas', 'Tarefas'),
        ]
        cop = getCoPContext(self.context)
        if cop.available_forms:
            self.filter_terms.append(('formularios', 'Formulários'))
        self.tabs_dict = {}
        for term, title in self.filter_terms:
            self.tabs_dict[term] = title
        return self.filter_terms

    def get_len_members(self):
        len_members = getCoPAtividadeLenMembers(self.context)
        return len_members

    def quote(self, message):
        return quote(message)
