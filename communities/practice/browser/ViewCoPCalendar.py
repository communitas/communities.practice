# -*- coding:utf8 -*-
from communities.practice.browser.ViewCoPBase import ViewCoPBase
from Solgema.fullcalendar.browser.views import SolgemaFullcalendarView


class ViewCoPCalendar(ViewCoPBase, SolgemaFullcalendarView):
    """Default View for Communities of Practice"""

    def set_calendar_properties(self):
        self.calendar.calendarHeight = u'500'
        self.calendar.eventType = u'CoPEvent'
        self.calendar.firstDay = 0
        self.calendar.defaultCalendarView = 'month'
        self.calendar.target_folder = '/'.join(self.context.getPhysicalPath())
