# -*- coding:utf8 -*-
from communities.practice.browser.ViewCoPBase import ViewCoPBase

class ViewCoPComment(ViewCoPBase):

    def get_context_values(self):
        context_values = {
            'path': '/'.join(self.context.getPhysicalPath()),
            'url': self.context.absolute_url(),
        }
        return context_values
