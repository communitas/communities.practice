# -*-coding:utf-8-*-
from Products.CMFCore.utils import getToolByName
from communities.practice.browser.ViewCoPBase import ViewCoPBase
from communities.practice.generics.generics import getLocalRole
from communities.practice.generics.generics import getMemberData
from communities.practice.generics.generics import getMMGroupMembers
from communities.practice.generics.cache import getCoPLocalRoles
from communities.practice.generics.cache import \
    listMemberRoleCommunitiesParticipating
from zope.component.hooks import getSite
from zope.component import getUtility
from zope.browsermenu.interfaces import IBrowserMenu


class ViewCoPCommunityFolder(ViewCoPBase):
    """Default View for Community Folder"""

    def get_filter_terms(self):
        self.filter_terms = [
            ('', 'Minhas comunidades'),
            ('all', 'Todas as comunidades'),
            ('alphabetic', 'Ordem alfabética'),
            ('latest', 'Atividade mais recente'),
        ]
        return self.filter_terms

    def set_cop_menu(self):
        """ Sets CoPMenu items manually for CoPCommunityFolder.
        """
        self.get_cop_context()
        context_url = self.cop_context.absolute_url()
        portal_membership = getToolByName(self.context, 'portal_membership')
        authenticated_user = portal_membership.getAuthenticatedMember()
        role = authenticated_user.getRoles()
        local_role = getLocalRole(self.context, authenticated_user.getId())
        moderadores_master = getMMGroupMembers(self.context)
        self.cop_menu = []
        self.cop_menu.append({
            'id': 'home',
            'url': context_url,
            'title': 'Início',
            'titlemenu': 'home',
            'order': 0,
        })
        if 'Manager' in role or 'Owner' in local_role or \
           authenticated_user.getId() in moderadores_master:
            self.cop_menu.append({
                'id': 'copcommunityfolderatividade',
                'url': '%s/viewCoPCommunityFolderActivity' % (context_url),
                'title': 'Atividade',
                'titlemenu': 'copcommunityfolderatividade',
                'order': 1,
            })
            self.cop_menu.append({
                'id': 'copcommunityneverlogged',
                'url': '%s/viewCoPMembersNeverLogged' % (context_url),
                'title': 'Nunca Logaram',
                'titlemenu': 'copcommunityneverlogged',
                'order': 2,
            })
            self.cop_menu.append({
                'id': 'configuracoes',
                'url': '%s/viewCoPCommunityFolderConfig' % (context_url),
                'title': 'Configurações',
                'titlemenu': 'configuracoes',
                'order': 3,
            })
        return self.cop_menu

    def get_addable_types(self):
        browser_menu = getUtility(
            IBrowserMenu,
            name='plone_contentmenu_factory',
        )
        items = browser_menu.getMenuItems(self.context, self.request)
        self.addable_types = []
        for cop_type in items:
            if cop_type['title'] == "Comunidade":
                self.addable_types.append({
                    'id': cop_type['id'],
                    'url': cop_type['action'],
                    'title': "Criar Comunidade",
                })
        return self.addable_types

    def get_communities(self, limit=0):
        """
            Get communities filtered by SearchableText or filter_term.
        """
        search_term = self.request.get("SearchableText", None)
        filter_term = self.request.get("filter_term", None)
        if filter_term == "all":
            self.communities = self.get_all_communities(search_term)
        elif filter_term == "alphabetic":
            self.communities = self.get_alphabetic_communities(search_term)
        elif filter_term == "latest":
            self.communities = self.get_latest_communities(search_term)
        else:
            self.communities = self.get_my_communities(search_term)
        return self.communities

    def get_communities_brain(self, search_term):
        catalog = getToolByName(self.context, 'portal_catalog')
        community_folder_path = '/'.join(self.context.getPhysicalPath())
        query = {}
        query['portal_type'] = 'CoP'
        query['path'] = {'query': community_folder_path, 'depth': 1}
        if search_term:
            search_term = "%s*" % (search_term)
            query['SearchableText'] = search_term
            query['path'] = {'query': community_folder_path}
        query['sort_on'] = 'created'
        query['sort_order'] = 'reverse'
        communities_brain = catalog(query)
        return communities_brain

    def get_all_communities(self, search_term):
        all_communities = self.get_communities_values(
            self.get_communities_brain(search_term)
        )
        return all_communities

    def get_my_communities(self, search_term):
        portal_membership = getToolByName(self.context, 'portal_membership')
        authenticated_member = portal_membership.getAuthenticatedMember()
        authenticated_id = authenticated_member.getId()
        communities_participating = listMemberRoleCommunitiesParticipating(
            authenticated_id).get(authenticated_id)
        communities_brain = self.get_communities_brain(search_term)
        my_communities = [cop for cop in communities_brain
                          if cop.UID in communities_participating.keys()]
        return self.get_communities_values(my_communities)

    def get_alphabetic_communities(self, search_term):
        communities = self.get_communities_values(
            self.get_communities_brain(search_term)
        )
        communities.sort(key=lambda community: community['title'])
        return communities

    def get_latest_communities(self, search_term):
        communities = self.get_communities_values(
            self.get_communities_brain(search_term)
        )
        communities.sort(key=lambda community: community['last_activity'])
        communities.reverse()
        return communities

    def get_sub_cop_number(self, community_path, filter_term=""):
        catalog = getToolByName(self.context, 'portal_catalog')
        query = {}
        query['portal_type'] = 'CoP'
        community_path += '/subcop'
        query['path'] = {'query': community_path, 'depth': 1}
        brain = catalog(query)
        if filter_term:
            return len(brain)

        portal_membership = getToolByName(self.context, 'portal_membership')
        authenticated_id = portal_membership.getAuthenticatedMember().getId()
        member_communities = listMemberRoleCommunitiesParticipating(
            authenticated_id
        )
        communities_participating = member_communities.get(
            authenticated_id
        )
        cop_uids = communities_participating.keys()
        sub_cop_number = 0
        for cop in brain:
            if cop.UID in cop_uids:
                sub_cop_number += 1
        return sub_cop_number

    def sub_cop_menu_available(self, community_path):
        catalog = getToolByName(self.context, 'portal_catalog')
        query = {}
        query['portal_type'] = 'CoPMenu'
        query['id'] = 'subcop'
        query['path'] = {'query': community_path, 'depth': 1}
        brain = catalog(query)
        return brain

    def get_communities_values(self, communities_brain):
        portal = getSite()
        portal_membership = getToolByName(self.context, 'portal_membership')
        portal_workflow = getToolByName(self.context, 'portal_workflow')
        authenticated_id = portal_membership.getAuthenticatedMember().getId()
        catalog = getToolByName(self.context, 'portal_catalog')
        communities = []
        for community in communities_brain:
            owner_id = community.Creator
            owner_name, owner_email = getMemberData(portal, owner_id)
            image = community.imageCoP
            owner_url = "%s/author/%s" % (
                portal.absolute_url(),
                owner_id
            )
            participation_url = ""
            if community.acceptsShares:
                participation_url = "%s/viewCoPJoin" % (community.getURL())

            review_state = community.review_state
            workflow_id = portal_workflow.getChainFor(community.portal_type)[0]
            workflow = portal_workflow.getWorkflowById(workflow_id)
            state = workflow.states.get(review_state)
            if state:
                review_state = state.title

            local_roles = getCoPLocalRoles(community)
            local_roles = local_roles.get(community.UID)
            members = len(local_roles['Participante']) + \
                len(local_roles['Moderador'])

            member_communities = listMemberRoleCommunitiesParticipating(
                authenticated_id
            )
            role = ""
            communities_participating = member_communities.get(
                authenticated_id
            )
            if communities_participating:
                role = communities_participating.get(community.UID)

            participant_role = "Autenticado"
            if role in ["Aguardando", "Participante", "Moderador",
                        "Observador", "Bloqueado"]:
                participant_role = role
            elif role == "Owner":
                participant_role = "Criador"

            community_path = community.getPath()
            last_activity = catalog(
                path=community_path,
                sort_on='modified',
                sort_order='reverse',
                sort_limit=1,
                portal_type=['CoPDocument', 'CoPATA',
                             'CoPFile', 'CoPEvent',
                             'CoPImage', 'CoPLink', ]
            )[:1]

            last_activity = last_activity[0].modified if last_activity else 0

            participation_allowed = self.get_participation_allowed(
                community.UID
            )

            community_dict = {
                "title": community.Title,
                "participation_allowed": participation_allowed,
                "creator": owner_name,
                "creator_url": owner_url,
                "review_state": review_state,
                "image": image,
                "members": members,
                "description": community.Description,
                "url": community.getURL(),
                "participation": participant_role,
                "last_activity": last_activity,
                "participation_url": participation_url,
                "accepts_shares": community.acceptsShares,
                "num_subcop": community.subCoPNumber,
                "path": community_path,
            }

            communities.append(community_dict)
        return communities
