# -*- coding:utf8 -*-
from communities.practice.browser.ViewCoPBase import ViewCoPBase
from communities.practice.generics.cache import \
    listMemberRoleCommunitiesParticipating
from Products.CMFCore.utils import getToolByName
from communities.practice.generics.generics import getSuperCoP
from communities.practice.generics.generics import isSubCoP
from communities.practice.generics.generics import encodeUTF8
from communities.practice.generics.generics import generateContentId
from communities.practice.generics.generics import createCoPMenuPost
from communities.practice.subscribers import transitionParentState
from Products.CMFPlone.utils import _createObjectByType
from Products.Archetypes.event import ObjectInitializedEvent
from zope.security import checkPermission
from zope.event import notify
import time


class ViewCoP(ViewCoPBase):
    """Default View for Communities of Practice"""

    def get_filter_terms(self):
        self.filter_terms = [
            ('', 'Ver Todos'),
            ('CoPFile', 'Arquivos'),
            ('CoPImage', 'Imagens'),
            ('CoPDocument', 'Páginas'),
            ('CoPPost', 'Posts'),
        ]
        return self.filter_terms

    def get_participation_community(self):
        catalog = getToolByName(self.context, 'portal_catalog')
        brain = catalog(UID=self.context.UID())
        if brain:
            community = brain[0]

            portal_membership = getToolByName(
                self.context, 'portal_membership'
            )
            portal_workflow = getToolByName(self.context, 'portal_workflow')
            authenticated = portal_membership.getAuthenticatedMember()
            authenticated_id = authenticated.getId()
            member_communities = listMemberRoleCommunitiesParticipating(
                authenticated_id
            )
            role = ""
            communities_participating = member_communities.get(
                authenticated_id
            )
            if communities_participating:
                role = communities_participating.get(community.UID)

            participant_role = "Autenticado"
            if role in ["Aguardando", "Participante", "Moderador",
                        "Observador", "Bloqueado"]:
                participant_role = role
            elif role == "Owner":
                participant_role = "Criador"

            review_state = community.review_state
            workflow_id = portal_workflow.getChainFor(community.portal_type)[0]
            workflow = portal_workflow.getWorkflowById(workflow_id)
            state = workflow.states.get(review_state)
            if state:
                review_state = state.title

            participation_cop = {}
            participation_cop['accepts_shares'] = community.acceptsShares
            participation_cop['participation_url'] = "%s/viewCoPJoin" % (
                community.getURL()
            )
            participation_cop['participation'] = participant_role
            participation_cop['review_state'] = review_state
            participation_cop['participation_allowed'] = \
                self.get_participation_allowed(community.UID)
        return participation_cop

    def create_post(self):
        """ Create CoPPost by form request in Timeline."""
        form = self.request.form
        post_message = encodeUTF8(form.get('post_message'))
        if post_message:
            post_id = generateContentId(self.context, time.time())
            menu = self.context.posts
            post = _createObjectByType(
                'CoPPost',
                menu,
                id=post_id,
                title='post',
                description=post_message[:600],
                )
            post.unmarkCreationFlag()
            transitionParentState(post, False)
            post.setDescription(post_message)
            post.reindexObject()
            notify(ObjectInitializedEvent(post))
            return post.UID()
        return ""

    def can_add_cop_post(self):
        """ Checks whether CoPMenu Posts exists and whether a user
            can add CoPPost in Timeline.
        """
        if 'posts' not in self.context:
            createCoPMenuPost(self.context)
        return checkPermission(
            'communities.practice.AddCoPPost',
            self.context.posts
            )
