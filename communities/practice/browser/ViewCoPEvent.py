# -*- coding: utf-8 -*-
from zope.component import getMultiAdapter
from Acquisition import aq_parent

from communities.practice.browser.ViewCoPBase import ViewCoPBase
from communities.practice.generics.generics import canDeleteCoPFolder

class ViewCoPEvent(ViewCoPBase):
    """Default View for CoPEvent"""

    def get_factory_url(self):

        calendar = aq_parent(self.context)
        url = '%s/createObject?type_name=CoPATA&event_uid=%s .cop-contents'%(
            calendar.absolute_url(), self.context.id
        )
        return url
