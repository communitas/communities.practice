# -*- coding: utf-8 -*-
from Products.Five.browser import BrowserView
from communities.practice.generics.generics import \
    getMemberData, getUsersByRole
from communities.practice.generics.asyncmail import \
    createMessage, sendAsyncMail
from Products.CMFCore.utils import getToolByName
from communities.practice.generics.generics import getCoPContext
from communities.practice.generics.generics import getCoPSettings
from communities.practice.generics.generics import setMembersCoPRole
from communities.practice.generics.generics import getReviewStateTitle
from communities.practice.generics.generics import isSubCoP
from communities.practice.generics.generics import getSuperCoP


class ViewCoPLeaveButton(BrowserView):
    """Default View for Communities of Practice"""

    def get_review_state(self):
        review_state = getReviewStateTitle(self.context)
        return review_state.get('id', '')

    def send_request(self):
        form = self.request.form
        portal_membership = getToolByName(self.context, 'portal_membership')
        if 'submit' in form.keys() and not portal_membership.isAnonymousUser():
            cop = self.context
            portal_workflow = getToolByName(self.context, 'portal_workflow')
            review_state = portal_workflow.getInfoFor(
                self.context, "review_state", ""
            )
            authenticated_id = \
                portal_membership.getAuthenticatedMember().getId()
            status_message = ''
            if review_state == "publico":
                if cop.getOwner().getId() != authenticated_id:
                    is_sub_cop = isSubCoP(cop)
                    portal_groups = getToolByName(self, 'portal_groups')
                    group_id = cop.UID()
                    if not is_sub_cop and group_id in portal_groups.getGroupIds():
                        portal_groups.removePrincipalFromGroup(
                            authenticated_id, group_id
                        )
                    setMembersCoPRole(cop, [authenticated_id], "Excluir")
                    status_message = unicode(
                        "Você não participa mais da comunidade %s" % (
                            cop.Title()
                        ),
                        'utf-8'
                    )
            else:
                message = form.get("message")
                self.send_notification_mail(message)
                setMembersCoPRole(cop, [authenticated_id], "Bloqueado")
                status_message = u"""Sua solicitação para deixar de
                    participar da comunidade foi encaminhada aos moderadores.
                """
            if isSubCoP(cop):
                destination = getSuperCoP(cop)
            else:
                destination = getCoPContext(
                    self.context, ['CoPCommunityFolder']
                )
            self.context.plone_utils.addPortalMessage(
                status_message, 'info'
            )
            return self.request.response.redirect(destination.absolute_url())
        return ""

    def send_notification_mail(self, requestor_message):
        """Send message for moderators."""
        community = self.context
        portal_membership = getToolByName(community, 'portal_membership')
        community_title = community.Title()
        community_url = community.absolute_url()
        # Requestor data
        requestor_id = portal_membership.getAuthenticatedMember().getId()
        requestor_data = getMemberData(community, requestor_id)
        # Moderators data
        roles = ['Owner', 'Moderador', 'Moderador_Master']
        list_moderators = getUsersByRole(community, roles)
        list_mail_to = []
        for role in roles:
            for user in list_moderators[role]:
                list_mail_to.append(user['email'])
        # message
        settings = getCoPSettings()
        mail_from = settings.username
        subject = "Pedido para deixar a comunidade %s" % (community_title)
        message = "Content-Type: text/plain; charset='UTF-8'"
        message += "\nMIME-Version: 1.0\n\n"
        message += """O usuário %s solicitou permissão para deixar de participar
            da comunidade %s. O acesso deste usuário à comunidade foi bloqueado
            \nPara permitir que ele deixe de participar clique
            no link abaixo.\n %s/configuracoes
        """ % (requestor_data[0], community_title, community_url)
        if requestor_message:
            message += "\n\nMensagem deixada por %s:\n%s\n" % (
                requestor_data[0], requestor_message
            )
        createMessage(
            mail_from, list_mail_to, subject,
            message, self.request.getURL()
        )
        sendAsyncMail()
