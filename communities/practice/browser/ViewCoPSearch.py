# -*- coding: utf-8 -*-
from communities.practice.browser.ViewCoPBase import ViewCoPBase


class ViewCoPSearch(ViewCoPBase):
    """Default view for search into CoPs."""

    def get_filter_terms(self):
        self.filter_terms = [
            ('', 'Ver Todos'),
            ('CoPFile', 'Arquivos'),
            ('CoPImage', 'Imagens'),
            ('CoPDocument', 'Páginas'),
            ('CoPPost', 'Posts'),
        ]
        return self.filter_terms
