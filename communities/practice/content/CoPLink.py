# -*- coding: utf-8 -*-

from AccessControl import ClassSecurityInfo
from Products.Archetypes.atapi import *
from zope.interface import implements

from Products.CMFDynamicViewFTI.browserdefault import BrowserDefaultMixin

from Products.ATContentTypes.content.link import ATLink
from Products.ATContentTypes.content.link import ATLinkSchema

from communities.practice.interfaces import ICoPLink
from communities.practice.content.CoPFieldsBase import COP_SCHEMA
from communities.practice.config import *

schema = Schema((


),
)

CoPLink_schema = ATLinkSchema.copy() + \
    schema.copy() + COP_SCHEMA.copy()

for field in ['creators','contributors','excludeFromNav','allowDiscussion']:
    CoPLink_schema[field].write_permission = "ManagePortal"

class CoPLink(ATLink):
    """
    """
    security = ClassSecurityInfo()
    implements(ICoPLink)

    meta_type = 'CoPLink'
    _at_rename_after_creation = True

    schema = CoPLink_schema

    # Methods

registerType(CoPLink, PROJECTNAME)
