# -*- coding: utf-8 -*-

from AccessControl import ClassSecurityInfo
from Products.Archetypes.atapi import Schema
from Products.Archetypes.atapi import StringField
from Products.Archetypes.atapi import ImageField
from Products.Archetypes.atapi import SelectionWidget
from Products.Archetypes.atapi import registerType
from Products.Archetypes.atapi import AnnotationStorage
from Products.Archetypes.atapi import LinesField
from Products.Archetypes.atapi import DisplayList
from zope.interface import implements

from Products.ATContentTypes.content.folder import ATFolder
from Products.ATContentTypes.content.folder import ATFolderSchema

from communities.practice.interfaces import ICoPCommunityFolder
from communities.practice.config import OPCOES
from communities.practice.config import PROJECTNAME
from communities.practice.generics.vocabularies import AVAILABLE_COLORS
from communities.practice.generics.vocabularies import generateVocabulary

schema = Schema((

    StringField(
        name='criacao_comunidades',
        default=OPCOES[0],
        widget=SelectionWidget(
            label=u"""Disponibilizar a criação de comunidades
                por membros do portal?
            """,
            format="radio",
            label_msgid='ComunidadePratica_label_criacao_comunidades',
            i18n_domain='ComunidadePratica',
        ),
        required=1,
        vocabulary=[
            OPCOES[0], OPCOES[1],
        ]
    ),

    ImageField(
        name='imagem',
        widget=ImageField._properties['widget'](
            label="Imagem",
            description="""Imagem padrão para ser utilizada quando não é
                cadastrada uma imagem para Comunidade.
            """,
            label_msgid='ComunidadePratica_label_imagem',
            description_msgid='ComunidadePratica_help_imagem',
            i18n_domain='ComunidadePratica',
        ),
        required=1,
        storage=AnnotationStorage(),
        original_size=(300, 300),
    ),

    LinesField(
        name='color',
        default=AVAILABLE_COLORS[0],
        widget=SelectionWidget(
            label=u'Cores da lista de comunidades',
            label_msgid='ComunidadePratica_label_colors_input',
            i18n_domain='ComunidadePratica',
        ),
        required=1,
        vocabulary=AVAILABLE_COLORS,
    ),
),
)

CoPCommunityFolder_schema = ATFolderSchema.copy() + \
    schema.copy()

manager_fields = [
    'creators', 'contributors', 'excludeFromNav',
    'allowDiscussion', 'nextPreviousEnabled',
]
for field in manager_fields:
    CoPCommunityFolder_schema[field].write_permission = "ManagePortal"

description_field = CoPCommunityFolder_schema['description']
description_field.widget.maxlength = 600


class CoPCommunityFolder(ATFolder):
    """
    """
    security = ClassSecurityInfo()
    implements(ICoPCommunityFolder)

    meta_type = 'CoPCommunityFolder'
    _at_rename_after_creation = True

    schema = CoPCommunityFolder_schema

    # Methods
    def at_post_create_script(self):
        """ Habilitar/Desabilitar criacao de comunidades
        """
        if self.get_criacao_habilitado():
            self.manage_setLocalRoles('AuthenticatedUsers', ['Contributor', ])

    def at_post_edit_script(self):
        """ Habilitar/Desabilitar criacao de comunidades
        """
        if self.get_criacao_habilitado():
            self.manage_setLocalRoles('AuthenticatedUsers', ['Contributor', ])
        else:
            self.manage_delLocalRoles(['AuthenticatedUsers'])

    security.declarePrivate('get_criacao_habilitado')
    def get_criacao_habilitado(self):
        """Retorna True ou False se a opcao criacao_comunidaes
           esta habilitada ou desabilitada
        """
        if self.getCriacao_comunidades() == OPCOES[0]:
            return True
        else:
            return False


registerType(CoPCommunityFolder, PROJECTNAME)
