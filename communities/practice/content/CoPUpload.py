# -*- coding: utf-8 -*-

from AccessControl import ClassSecurityInfo
from Products.Archetypes.atapi import *
from zope.interface import implements
from communities.practice.interfaces import ICoPUpload
from communities.practice.content.CoPFieldsBase import COP_SCHEMA

from Products.CMFDynamicViewFTI.browserdefault import BrowserDefaultMixin

from Products.ATContentTypes.content.folder import ATFolder
from Products.ATContentTypes.content.folder import ATFolderSchema
from communities.practice.config import PROJECTNAME

schema = Schema((


),
)

CoPUpload_schema = ATFolderSchema.copy() + \
    schema.copy() + COP_SCHEMA.copy()

for field in ['creators','contributors','excludeFromNav','allowDiscussion', 'nextPreviousEnabled']:
    CoPUpload_schema[field].write_permission = "ManagePortal"

class CoPUpload(ATFolder):
    """
    """
    security = ClassSecurityInfo()

    implements(ICoPUpload)

    meta_type = 'CoPUpload'
    _at_rename_after_creation = True

    schema = CoPUpload_schema

    # Methods

registerType(CoPUpload, PROJECTNAME)

