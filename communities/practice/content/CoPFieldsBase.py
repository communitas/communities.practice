# -*- coding: utf-8 -*-

from Products.Archetypes.atapi import Schema
from Products.Archetypes.atapi import BooleanField
from Products.Archetypes.atapi import BooleanWidget

COP_SCHEMA = Schema((

    BooleanField(
          name='timeline_listed',
          default=True,
          widget=BooleanWidget(
              label=u'Deseja que esse item seja listado na TimeLine?',
              description='',
              label_msgid='ComunidadePratica_label_timeline_listed',
              i18n_domain='ComunidadePratica',
          ),
      ),
),
)
