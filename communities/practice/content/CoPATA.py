# -*- coding: utf-8 -*-
from AccessControl import ClassSecurityInfo
from Products.Archetypes.atapi import *
from Products.ATContentTypes.interfaces import IATEvent
from zope.interface import implements
from Products.CMFDynamicViewFTI.browserdefault import BrowserDefaultMixin
from archetypes.referencebrowserwidget.widget import ReferenceBrowserWidget
from communities.practice.config import *
from communities.practice.interfaces import ICoPATA
from communities.practice.content.CoPFieldsBase import COP_SCHEMA
from communities.practice.generics.generics import getCoPParticipants
from communities.practice.generics.generics import getMemberData

schema = Schema((

   ReferenceField(
        name='evento',
        widget=ReferenceBrowserWidget(
            label='Evento',
            label_msgid='communities_practice_label_evento',
            i18n_domain='communities_practice',
            force_close_on_insert=True,
            startup_directory_method='getStartupDirectory',
        ),
        allowed_types=('CoPEvent',),
        multiValued=1,
        relationship='copata_copevent',
    ),
    StringField(
        name='title',
        widget=StringField._properties['widget'](
            label=u'Título',
            label_msgid='communities_practice_label_title',
            i18n_domain='communities_practice',
        ),
        required=1,
        accessor="Title",
    ),
    TextField(
        name='pauta',
        allowable_content_types=('text/plain', 'text/structured', 'text/html', 'application/msword',),
        widget=RichWidget(
            label='Pauta',
            label_msgid='communities_practice_label_pauta',
            i18n_domain='communities_practice',
        ),
        default_output_type='text/html',
    ),
    TextField(
        name='discussao',
        allowable_content_types=('text/plain', 'text/structured', 'text/html', 'application/msword',),
        widget=RichWidget(
            label=u'Discussão',
            label_msgid='communities_practice_label_discussao',
            i18n_domain='communities_practice',
        ),
        default_output_type='text/html',
    ),
    TextField(
        name='encaminhamentos',
        allowable_content_types=('text/plain', 'text/structured', 'text/html', 'application/msword',),
        widget=RichWidget(
            label='Encaminhamentos',
            label_msgid='communities_practice_label_encaminhamentos',
            i18n_domain='communities_practice',
        ),
        default_output_type='text/html',
    ),
    LinesField('participantes',
               languageIndependent=True,
               searchable=True,
               widget=MultiSelectionWidget(
                      description='',
                      label=u'Participantes',
                      format='checkbox',
                      ),
               vocabulary='getVocabularyParticipantes',
    ),
    LinesField('outros_participantes',
               languageIndependent=True,
               searchable=True,
               widget=LinesWidget(
                      description='',
                      label=u'Outros Participantes',
                      ),
    ),

),
)

CoPATA_schema = BaseSchema.copy() + \
    schema.copy() + COP_SCHEMA.copy()

for field in ['creators','contributors','allowDiscussion']:
    CoPATA_schema[field].write_permission = "ManagePortal"

CoPATA_schema.moveField('evento', before="title")

class CoPATA(BaseContent, BrowserDefaultMixin):
    """
    """
    security = ClassSecurityInfo()
    implements(IATEvent, ICoPATA)

    meta_type = 'CoPATA'
    _at_rename_after_creation = True

    schema = CoPATA_schema

    #Esconder o campo
    TimeLineField = schema['timeline_listed']
    TimeLineField.widget.visible = {'edit': 'visible', 'view': 'invisible'}

    EventReference = schema['evento']
    EventReference.widget.visible = {'edit': 'invisible', 'view': 'visible'}


    def getVocabularyParticipantes(self):
        """
        """
        vocabulary = DisplayList()
        cop = self.getParentNode()
        while not cop.Type() == u"Comunidade":
            cop = cop.getParentNode()

        participantes = getCoPParticipants(cop)
        for participante in participantes:
            member_data = getMemberData(cop, participante)
            if member_data:
                vocabulary.add(participante, member_data[0])
        return vocabulary

    def getStartupDirectory(self):
        """
        """
        directory = '/'.join(self.getParentNode().getPhysicalPath())
        return directory


registerType(CoPATA, PROJECTNAME)
