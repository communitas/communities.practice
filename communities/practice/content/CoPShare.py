# -*- coding: utf-8 -*-

from AccessControl import ClassSecurityInfo
from Products.Archetypes.atapi import *
from zope.interface import implements

from Products.CMFDynamicViewFTI.browserdefault import BrowserDefaultMixin

from Products.ATContentTypes.content.link import ATLink
from Products.ATContentTypes.content.link import ATLinkSchema

from communities.practice.interfaces import ICoPShare
from communities.practice.content.CoPFieldsBase import COP_SCHEMA
from communities.practice.config import *

schema = Schema((

    StringField(
        name = "parent_uid",
        widget=StringField._properties['widget'](
            visible = False,
        ),
    ),

),
)

CoPShare_schema = ATLinkSchema.copy() + \
    schema.copy() + COP_SCHEMA.copy()

for field in ['creators','contributors','excludeFromNav','allowDiscussion']:
    CoPShare_schema[field].write_permission = "ManagePortal"

class CoPShare(ATLink):
    """
    """
    security = ClassSecurityInfo()
    implements(ICoPShare)

    meta_type = 'CoPShare'
    _at_rename_after_creation = True

    schema = CoPShare_schema

    #Esconder o campo description
    DescriptionField = schema['description']
    DescriptionField.widget.visible = {'edit': 'invisible', 'view': 'visible'}

    #Esconder o campo remoteUrl
    RemoteUrl = schema['remoteUrl']
    RemoteUrl.widget.visible = {'edit': 'invisible', 'view': 'invisible'}

    # Methods

registerType(CoPShare, PROJECTNAME)
