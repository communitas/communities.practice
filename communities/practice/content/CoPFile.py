# -*- coding: utf-8 -*-

from AccessControl import ClassSecurityInfo
from Products.Archetypes.atapi import *
from zope.interface import implements

from Products.CMFDynamicViewFTI.browserdefault import BrowserDefaultMixin
from Products.validation import validation
from Products.validation.validators.SupplValidators import MaxSizeValidator

from Products.ATContentTypes.interfaces import IATFile

from plone.app.blob.content import ATBlob
from plone.app.blob.content import ATBlobSchema
from plone.app.blob.subtypes.file import ExtensionBlobField

from communities.practice.interfaces import ICoPFile
from communities.practice.content.CoPFieldsBase import COP_SCHEMA
from communities.practice.config import *

schema = Schema((
    ExtensionBlobField('file',
                required = True,
                primary = True,
                searchable = True,
                default = '',
                accessor = 'getFile',
                mutator = 'setFile',
                index_method = 'getIndexValue',
                languageIndependent = True,
                storage = AnnotationStorage(migrate=True),
                validators = (('isNonEmptyFile', 1),
                              MaxSizeValidator("checkFileMaxSize",maxsize=5)),
                widget = FileWidget(label = u'File',
                                    description=u'',
                                    show_content_type = False,)),

),
)

CoPFile_schema = ATBlobSchema.copy() + \
    schema.copy() + COP_SCHEMA.copy()

for field in ['creators','contributors','excludeFromNav','allowDiscussion']:
    CoPFile_schema[field].write_permission = "ManagePortal"

class CoPFile(ATBlob):
    """
    """
    security = ClassSecurityInfo()
    implements(IATFile, ICoPFile)

    meta_type = 'CoPFile'
    _at_rename_after_creation = True

    schema = CoPFile_schema

    #Esconder o campo
    TimeLineField = schema['timeline_listed']
    TimeLineField.widget.visible = {'edit': 'visible', 'view': 'invisible'}

registerType(CoPFile, PROJECTNAME)
