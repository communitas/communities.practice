# -*- coding: utf-8 -*-

from AccessControl import ClassSecurityInfo
from Products.Archetypes.atapi import *
from zope.interface import implements

from Products.CMFDynamicViewFTI.browserdefault import BrowserDefaultMixin

from Products.ATContentTypes.interfaces import IATImage

from plone.app.blob.content import ATBlob
from plone.app.blob.content import ATBlobSchema
from plone.app.blob.subtypes.image import ExtensionBlobField
from Products.ATContentTypes.configuration import zconf
from Products.validation.validators.SupplValidators import MaxSizeValidator
from Products.validation import validation
from validators import FileExtensionValidator
validation.register(FileExtensionValidator('isValidExtension',extension=['jpg','jpeg','png','bmp','gif']))

from communities.practice.interfaces import ICoPImage
from communities.practice.content.CoPFieldsBase import COP_SCHEMA
from communities.practice.config import *

schema = Schema((
        ExtensionBlobField('image',
            required = True,
            primary = True,
            accessor = 'getImage',
            mutator = 'setImage',
            sizes = None,
            languageIndependent = True,
            storage = AnnotationStorage(migrate=True),
            swallowResizeExceptions = zconf.swallowImageResizeExceptions.enable,
            pil_quality = zconf.pil_config.quality,
            pil_resize_algo = zconf.pil_config.resize_algo,
            original_size = None,
            max_size = zconf.ATImage.max_image_dimension,
            default_content_type = 'image/png',
            allowable_content_types = ('image/gif', 'image/jpeg', 'image/png'),
            validators = (('isNonEmptyFile', 1),
                          MaxSizeValidator("checkFileMaxSize",maxsize=2),
                          ('isValidExtension'),),
            widget = ImageWidget(label = u'Image',
                                 description=u'',
                                 show_content_type = False,)),
))

CoPImage_schema = ATBlobSchema.copy() + \
    schema.copy() + COP_SCHEMA.copy()

for field in ['creators','contributors','excludeFromNav','allowDiscussion']:
    CoPImage_schema[field].write_permission = "ManagePortal"

class CoPImage(ATBlob):
    """
    """
    security = ClassSecurityInfo()
    implements(IATImage, ICoPImage)

    meta_type = 'CoPImage'
    _at_rename_after_creation = True

    schema = CoPImage_schema

    # Methods

registerType(CoPImage, PROJECTNAME)
