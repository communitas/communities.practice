# -*- coding: utf-8 -*-
from AccessControl import ClassSecurityInfo
from Acquisition import aq_inner, aq_parent
from plone.portlets.interfaces import IPortletAssignmentMapping
from plone.portlets.interfaces import IPortletManager
from zope.interface import implements
from zope.container.interfaces import INameChooser
from zope.component import adapter, getMultiAdapter, getUtility
from Products.Archetypes.atapi import AnnotationStorage
from Products.Archetypes.atapi import DisplayList
from Products.Archetypes.atapi import ImageField
from Products.Archetypes.atapi import IntegerField
from Products.Archetypes.atapi import KeywordWidget
from Products.Archetypes.atapi import LinesField
from Products.Archetypes.atapi import MultiSelectionWidget
from Products.Archetypes.atapi import registerType
from Products.Archetypes.atapi import SelectionWidget
from Products.Archetypes.atapi import Schema
from Products.Archetypes.atapi import StringField
from Products.Archetypes.atapi import TextAreaWidget
from Products.Archetypes.atapi import TextField
from Products.Archetypes.interfaces import IObjectInitializedEvent
from Products.ATContentTypes.content.folder import ATFolder
from Products.ATContentTypes.content.folder import ATFolderSchema

from communities.practice import config
from communities.practice.generics.generics import getCoPSettings
from communities.practice.generics.generics import getProductInstalled
from communities.practice.generics.generics import isSubCoP
from communities.practice.generics.vocabularies import AVAILABLE_COLORS
from communities.practice.generics.vocabularies import \
    AVAILABLE_FORMS_VOCABULARY
from communities.practice.generics.vocabularies import generateVocabulary
from communities.practice.interfaces import ICoP
from communities.practice.portlets import portletCoPMembers


schema = Schema((

    StringField(
        name='title',
        widget=StringField._properties['widget'](
            label=u'Nome da Comunidade',
            label_msgid='ComunidadePratica_label_title',
            i18n_domain='ComunidadePratica',
        ),
        required=1,
        accessor="Title",
        searchable=True,
    ),

    TextField(
        name='description',
        widget=TextAreaWidget(
            label=u'Descrição',
            description=u"""
                Escreva sobre o(s) assuntos a serem
                abordados pela comunidade.
            """,
            label_msgid="ComunidadePratica_label_description",
            description_msgid="ComunidadePratica_help_description",
            i18n_domain='ComunidadePratica',
            maxlength=600,
        ),
        accessor="Description",
        searchable=True,
        required=1
    ),

    LinesField(
        name='subject',
        widget=KeywordWidget(
            label=u'Categorização',
            description=u'Defina Palavras-Chave para sua comunidade.',
            label_msgid="ComunidadePratica_label_categorization",
            description_msgid="ComunidadePratica_help_categorization",
            i18n_domain='ComunidadePratica',
        ),
        accessor="Subject",
        searchable=True,
        multiValued=1
    ),

    ImageField(
        name='imagem',
        widget=ImageField._properties['widget'](
            label="Imagem",
            description=u'Imagem simbolo da Comunidade',
            label_msgid='ComunidadePratica_label_imagem',
            description_msgid='ComunidadePratica_help_imagem',
            i18n_domain='ComunidadePratica',
        ),
        storage=AnnotationStorage(),
        original_size=(300, 300),
    ),

    StringField(
        name=config.INPUTS_FORM_IDS[5],
        default=config.OPCOES[1],
        widget=SelectionWidget(
            label=u'Subcomunidades',
            format="radio",
            label_msgid='ComunidadePratica_label_subcop_input',
            i18n_domain='ComunidadePratica',
        ),
        required=1,
        vocabulary=[
            config.OPCOES[0],
            config.OPCOES[1],
        ],
    ),

    IntegerField(
        name='subcop_level',
        widget=IntegerField._properties['widget'](
            label=u'Nivel da Comunidade',
            description=u'Nivel da Comunidade na hierarquia',
            label_msgid='ComunidadePratica_label_subcop_level',
            i18n_domain='ComunidadePratica',
        ),
    ),

    StringField(
        name=config.INPUTS_FORM_IDS[0],
        default=config.OPCOES[0],
        widget=SelectionWidget(
            label=u'Acervo',
            format="radio",
            label_msgid='ComunidadePratica_label_acervo_input',
            i18n_domain='ComunidadePratica',
        ),
        required=1,
        vocabulary=[
            config.OPCOES[0],
            config.OPCOES[1],
        ],
    ),

    StringField(
        name=config.INPUTS_FORM_IDS[1],
        default=config.OPCOES[0],
        widget=SelectionWidget(
            label=u'Calendario',
            format="radio",
            label_msgid='ComunidadePratica_label_calendario_input',
            i18n_domain='ComunidadePratica',
        ),
        required=1,
        vocabulary=[
            config.OPCOES[0],
            config.OPCOES[1],
        ],
    ),

    StringField(
        name=config.INPUTS_FORM_IDS[2],
        default=config.OPCOES[0],
        widget=SelectionWidget(
            label=u'Forum',
            format="radio",
            label_msgid='ComunidadePratica_label_forum_input',
            i18n_domain='ComunidadePratica',
        ),
        required=1,
        vocabulary=[
            config.OPCOES[0],
            config.OPCOES[1],
        ],
    ),

    StringField(
        name=config.INPUTS_FORM_IDS[3],
        default=config.OPCOES[0],
        widget=SelectionWidget(
            label=u'Portfolio',
            format="radio",
            label_msgid='ComunidadePratica_label_portfolio_input',
            i18n_domain='ComunidadePratica',
        ),
        required=1,
        vocabulary=[
            config.OPCOES[0],
            config.OPCOES[1],
        ],
    ),

    StringField(
        name=config.INPUTS_FORM_IDS[4],
        default=config.OPCOES[1],
        widget=SelectionWidget(
            label=u'Tarefas',
            format="radio",
            label_msgid='ComunidadePratica_label_tarefas_input',
            i18n_domain='ComunidadePratica',
        ),
        required=1,
        vocabulary=[
            config.OPCOES[0],
            config.OPCOES[1],
        ],
    ),

    LinesField(
        name='available_forms',
        widget=MultiSelectionWidget(
            label=u'Formulários habilitados nesta comunidade',
            format="checkbox",
            label_msgid='ComunidadePratica_label_tarefas_input',
            i18n_domain='ComunidadePratica',
        ),
        vocabulary="get_available_forms",
    ),

    LinesField(
        name='color',
        default=AVAILABLE_COLORS[0],
        widget=SelectionWidget(
            label=u'Cores da comunidade',
            label_msgid='ComunidadePratica_label_colors_input',
            i18n_domain='ComunidadePratica',
        ),
        required=1,
        vocabulary=AVAILABLE_COLORS,
    ),

    StringField(
        name=config.INPUTS_FORM_IDS[8],
        default=config.OPCOES[1],
        widget=SelectionWidget(
            label=u"""
                Exibir lista de participantes para pessoas que não fazem parte da comunidade?
            """,
            format="radio",
            label_msgid='ComunidadePratica_label_portlet_input',
            i18n_domain='ComunidadePratica',
        ),
        required=1,
        vocabulary=[
            config.OPCOES[0],
            config.OPCOES[1],
        ],
    ),

    StringField(
        name=config.INPUTS_FORM_IDS[6],
        default=config.OPCOES[0],
        widget=SelectionWidget(
            label=u"""
                Disponibilizar a opção aos membros efetuarem
                pedidos para participar da comunidade?
            """,
            format="radio",
            label_msgid='ComunidadePratica_label_participar_input',
            i18n_domain='ComunidadePratica',
        ),
        required=1,
        vocabulary=[
            config.OPCOES[0],
            config.OPCOES[1],
        ],
    ),

    StringField(
        name=config.INPUTS_FORM_IDS[9],
        default=config.OPCOES[0],
        widget=SelectionWidget(
            label=u"""
            Moderar pedidos de participação da comunidade?
            Caso desabilitado, participante será incluído automaticamente.""",
            format="radio",
            label_msgid='ComunidadePratica_label_moderar_input',
            description_msgid='ComunidadePratica_help_moderar_input',
            i18n_domain='ComunidadePratica',
        ),
        required=1,
        vocabulary=[
            config.OPCOES[0],
            config.OPCOES[1],
        ],
    ),

    StringField(
        name=config.INPUTS_FORM_IDS[7],
        default=config.OPCOES[0],
        widget=SelectionWidget(
            label=u"""
                Disponibilizar a gestão de participação
                para os moderadores da comunidade?
            """,
            format="radio",
            label_msgid='ComunidadePratica_label_gestao_input',
            i18n_domain='ComunidadePratica',
        ),
        required=1,
        vocabulary=[
            config.OPCOES[0],
            config.OPCOES[1],
        ],
    ),

),
)

CoP_schema = ATFolderSchema.copy() + \
    schema.copy()
manager_fields = [
    'creators', 'contributors', 'excludeFromNav',
    'allowDiscussion', 'nextPreviousEnabled',
]
for field in manager_fields:
    CoP_schema[field].write_permission = "ManagePortal"


class CoP(ATFolder):
    """CoP type class
    """
    security = ClassSecurityInfo()

    implements(ICoP)

    meta_type = 'CoP'
    _at_rename_after_creation = True

    schema = CoP_schema

    # Hide field subcop_level
    SubCoPField = schema['subcop_level']
    SubCoPField.widget.visible = {'edit': 'invisible', 'view': 'invisible'}

    # Methods
    def __init__(self, id):
        super(CoP, self).__init__(id)
        self.schema["available_forms"].widget.visible = \
            getProductInstalled("cop.forms")

    security.declarePrivate('get_portlet_habilitado')
    def get_portlet_habilitado(self):
        """Retorna True ou False se a opcao portlet_input
           esta habilitada ou desabilitada
        """
        if self.getPortlet_input() == config.OPCOES[0]:
            return True
        else:
            return False

    security.declarePrivate('get_participar_habilitado')
    def get_participar_habilitado(self):
        """Retorna True ou False se a opcao participar_input
           esta habilitada ou desabilitada
        """
        if self.getParticipar_input() == config.OPCOES[0]:
            return True
        else:
            return False

    security.declarePrivate('get_moderar_habilitado')
    def get_moderar_habilitado(self):
        """Retorna True ou False se a opcao moderar_input
           esta habilitada ou desabilitada
        """
        if self.getModerar_input() == config.OPCOES[0]:
            return True
        else:
            return False

    security.declarePrivate('get_gestao_habilitado')
    def get_gestao_habilitado(self):
        """Retorna True ou False se a opcao gestao_input
           esta habilitada ou desabilitada
        """
        if self.getGestao_input() == config.OPCOES[0]:
            return True
        else:
            return False

    def get_objects_titlemenu(self):
        objects = {}
        for obj in self.objectValues():
            objects[obj.titlemenu] = obj
        return objects

    def get_available_forms(self):
        vocabulary = DisplayList()
        settings = getCoPSettings()
        all_forms = generateVocabulary(AVAILABLE_FORMS_VOCABULARY)
        for available in settings.available_forms:
            term = all_forms.getTerm(available)
            vocabulary.add(term.value, term.title)
        return vocabulary


registerType(CoP, config.PROJECTNAME)


@adapter(ICoP, IObjectInitializedEvent)
def add_portletCoPMembers(obj, event):
    parent = aq_parent(aq_inner(obj))
    if ICoP.providedBy(parent):
        return

    if not isSubCoP(obj):
        column = getUtility(IPortletManager, name=u"plone.rightcolumn")

        manager = getMultiAdapter((obj, column,), IPortletAssignmentMapping)
        assignment = portletCoPMembers.Assignment(some_field="Participantes")
        chooser = INameChooser(manager)
        manager[chooser.chooseName(None, assignment)] = assignment
