# -*- coding: utf-8 -*-
from AccessControl import ClassSecurityInfo
from AccessControl import Unauthorized
from Products.Archetypes.atapi import registerType
from Products.Archetypes.atapi import Schema
from Products.ATContentTypes.interfaces import IATFolder
from Products.ATContentTypes.content.folder import ATFolder
from Products.ATContentTypes.content.folder import ATFolderSchema
from zope.interface import implements

from communities.practice.config import PROJECTNAME
from communities.practice.interfaces import ICoPFolder


schema = Schema((


),
)


CoPFolder_schema = ATFolderSchema.copy() + \
    schema.copy()

manager_fields = [
    'creators',
    'contributors',
    'excludeFromNav',
    'allowDiscussion',
    'nextPreviousEnabled',
]
for field in manager_fields:
    CoPFolder_schema[field].write_permission = "ManagePortal"


class CoPFolder(ATFolder):
    """
    """
    security = ClassSecurityInfo()
    implements(IATFolder, ICoPFolder)

    meta_type = 'CoPFolder'
    _at_rename_after_creation = True

    schema = CoPFolder_schema

    # Methods
    def manage_cutObjects(self, ids=None, REQUEST=None):
        from communities.practice.generics.generics import canCutObjects
        if not canCutObjects(self, ids, REQUEST):
            raise Unauthorized
        return super(CoPFolder, self).manage_cutObjects(ids, REQUEST)

registerType(CoPFolder, PROJECTNAME)
