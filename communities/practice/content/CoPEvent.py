# -*- coding: utf-8 -*-

from AccessControl import ClassSecurityInfo
from Products.Archetypes.atapi import *
from zope.interface import implements

from Products.CMFDynamicViewFTI.browserdefault import BrowserDefaultMixin
from Products.ATContentTypes.interfaces import IATEvent
from Products.ATContentTypes.content.event import ATEvent
from Products.ATContentTypes.content.event import ATEventSchema

from communities.practice.interfaces import ICoPEvent
from communities.practice.content.CoPFieldsBase import COP_SCHEMA
from communities.practice.config import *

schema = Schema((
    ReferenceField(
         name='ata',
         allowed_types=('CoPATA',),
         multiValued=1,
         relationship='copevent_copata',
     ),
),
)

CoPEvent_schema = ATEventSchema.copy() + \
    schema.copy() + COP_SCHEMA.copy()

for field in ['creators','contributors','excludeFromNav','allowDiscussion']:
    CoPEvent_schema[field].write_permission = "ManagePortal"

CoPEvent_schema.moveField('ata', before="title")

class CoPEvent(ATEvent):
    """
    """
    security = ClassSecurityInfo()
    implements(IATEvent, ICoPEvent)

    meta_type = 'CoPEvent'
    _at_rename_after_creation = True

    schema = CoPEvent_schema

    #Esconder o campo
    TimeLineField = schema['timeline_listed']
    TimeLineField.widget.visible = {'edit': 'visible', 'view': 'invisible'}

    ATAReference = schema['ata']
    ATAReference.widget.visible = {'edit': 'invisible', 'view': 'visible'}

registerType(CoPEvent, PROJECTNAME)
