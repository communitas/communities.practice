# -*- coding: utf-8 -*-
from Products.CMFCore.utils import getToolByName

from zope.interface import implements
from Products.validation.i18n import PloneMessageFactory as _
from Products.validation.i18n import recursiveTranslate
from Products.validation.i18n import safe_unicode
from Products.validation.interfaces.IValidator import IValidator

        
class FileExtensionValidator(object):
    implements(IValidator)

    def __init__ (self, name, title='', description='', extension=['pdf'], showError=True):
        self.name = name
        self.title = title
        self.description = description
        self.extension = extension
        self.showError = showError

    def __call__ (self, value, *args, **kwargs):
        """ 
        Verifica se a extensao do arquivo corresponde
        a string extension passada por parametro
        """
        if value.filename[value.filename.rfind('.') + 1:].lower() in self.extension:
            return False
        
        message = "Tipo de Arquivo inválido. Verifique se seu arquivo é de um dos tipos: %s"% (', '.join(self.extension))
        return message

class DuplicadoIdValidator(object):
    implements(IValidator)

    def __init__(self, name, title='', description='', showError=True):
        self.name = name
        self.title = title or name
        self.description = description
        self.showError = showError

    def __call__(self, value, *args, **kwargs):
        #get obj
        instance = kwargs.get('instance', None)
        
        #testa se nao eh a edicao do objeto
        if instance.isTemporary() is False:
            return True

        #tem que ser assim pois utilizar o aq_parent(aq_inner(instance)) retorna a url temporaria
        request = getattr(instance, 'REQUEST', None)
        parent = request.get('PARENTS')[0]
        parentPath = '/'.join(parent.getPhysicalPath())
        catalog = getToolByName(instance, 'portal_catalog')

        obj_search = catalog(Creator=instance.Creator(),
                             Type = instance.Type(),
                             path={'query':parentPath})
        if obj_search:
            return u"Você já possui um %s cadastrado!" % (instance.Type())
        else:
            return False
