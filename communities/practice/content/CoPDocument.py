# -*- coding: utf-8 -*-

from AccessControl import ClassSecurityInfo
from Products.Archetypes.atapi import *
from zope.interface import implements

from Products.ATContentTypes.content.document import ATDocument
from Products.ATContentTypes.content.document import ATDocumentSchema
from communities.practice.config import PROJECTNAME
from communities.practice.interfaces import ICoPDocument
from communities.practice.content.CoPFieldsBase import COP_SCHEMA
from communities.practice.generics.generics import limit_description, cleanhtml

schema = Schema((

),
)

CoPDocument_schema = ATDocumentSchema.copy() + \
    schema.copy() + COP_SCHEMA.copy()

for field in ['creators','contributors','excludeFromNav','allowDiscussion']:
    CoPDocument_schema[field].write_permission = "ManagePortal"

class CoPDocument(ATDocument):
    """
    """
    security = ClassSecurityInfo()
    implements(ICoPDocument)

    meta_type = 'CoPDocument'
    _at_rename_after_creation = True

    schema = CoPDocument_schema

    # Methods
    def at_post_create_script(self):
        """ Add description caso seja em branco
        """
        if self.getText():
            if not self.Description():
                self.setDescription(limit_description(cleanhtml(self.getText()),600))
                self.reindexObject()

registerType(CoPDocument, PROJECTNAME)
