# -*- coding: utf-8 -*-
from Products.CMFPlone.utils import _createObjectByType
from Products.CMFPlone.utils import getToolByName
from Acquisition import aq_parent
from Products.Archetypes.event import ObjectInitializedEvent
from AccessControl import Unauthorized

from communities.practice.config import *
from communities.practice.generics.generics import allowTypes
from communities.practice.generics.generics import getCoPContext
from communities.practice.generics.generics import getMemberData
from communities.practice.generics.generics import getProductInstalled
from communities.practice.generics.generics import isSubCoP
from communities.practice.generics.generics import setSuperCoPRoles
from communities.practice.generics.generics import getSubCoPDepth
from communities.practice.generics.generics import getRootCoP
from communities.practice.generics.generics import getSuperCoP
from communities.practice.generics.generics import canDeleteCoPFolder
from communities.practice.generics.generics import createCoPMenuPost
from communities.practice.generics.generics import getMMGroup
from communities.practice.generics.generics import getOMGroup
from communities.practice.generics.cache import listMemberRoleCommunitiesParticipating
from communities.practice.generics.cache import cleanCoPCache
from communities.practice.generics.cache import cleanMemberCache
from communities.practice.generics.cache import updateCommunityCache
from communities.practice.generics.generics import setMembersCoPRole

class AddContents(object):

    def __init__(self, obj):
        self.obj = obj

    def create_content(self):
        """Creates additional content depending on what is filled on form
        """
        content_obj = self.obj.get_objects_titlemenu()
        content = content_obj.keys()
        allowTypes(self.obj, ['CoPMenu'])

        #cria subcomunidades e subgrupo das xitas da arvore
        setCoPGroup(self.obj)
        if self.get_habilitar(self.obj.getSubcop_input()):
            createCoPGroup(self.obj)
            if 'subcop' not in content:
                subcop = _createObjectByType('CoPMenu',
                                             self.obj,
                                             id='subcop',
                                             title=u'Subcomunidades',
                                             description=u'Aqui são criadas as Subcomunidade.',
                                             titlemenu='subcop')
                #habilita os tipos de objetos possíveis
                allowTypes(subcop, ['CoP'])
                subcop.manage_addProperty('layout','viewCoPSubCoPMenu','string')
                subcop.unmarkCreationFlag()

        #cria pasta acervo - repositorio de documentos
        if self.get_habilitar(self.obj.getAcervo_input()) and 'acervo' not in content:
            acervo = _createObjectByType('CoPMenu',
                                         self.obj,
                                         id='acervo',
                                         title=u'Acervo',
                                         description=u'Acervo de documentos produzidos e utilizados pela Comunidade',
                                         titlemenu='acervo')
            #habilita types possíveis nesta pasta (pode ser alterado via gerenciador)
            allowTypes(acervo, ['CoPImage','CoPDocument','CoPFile','CoPLink','CoPFolder'])
            acervo.unmarkCreationFlag()
            createQuickUploadPortlet(acervo)

            #criacao da pagina de boas vindas
            boas_vindas = _createObjectByType('CoPDocument',
                                               acervo,
                                               id='boas_vindas')
            boas_vindas.setTitle(u'Boas Vindas')
            boas_vindas.setDescription(u'Seja Bem-Vindo a Comunidade.')
            boas_vindas.reindexObject()
            boas_vindas.unmarkCreationFlag()

        #cria posts
        if 'posts' not in content:
            createCoPMenuPost(self.obj)

        #cria calendario
        if self.get_habilitar(self.obj.getCalendario_input()) and 'calendario' not in content:
            calendario = _createObjectByType('CoPMenu',
                                             self.obj,
                                             id='calendario',
                                             title=u'Calendário',
                                             description=u'Aqui estão registrados todos os eventos importantes para esta Comunidade.',
                                             titlemenu='calendario')
            #habilita os tipos de objetos possíveis
            allowTypes(calendario, ['CoPEvent', 'CoPATA'])
            calendario.unmarkCreationFlag()
            setSolgemaCalendarView(calendario)

        #cria Portfolio
        if self.get_habilitar(self.obj.getPortfolio_input()) and 'portfolio' not in content:
            portfolio = _createObjectByType('CoPMenu',
                                             self.obj,
                                             'portfolio',
                                             title=u'Portfólio',
                                             description=u'',
                                             titlemenu='portfolio')
            allowTypes(portfolio, ['CoPPortfolio'])
            portfolio.manage_addProperty('layout','viewCoPMenuPortfolio','string')
            portfolio.unmarkCreationFlag()
            createQuickUploadPortlet(portfolio)

        #cria Forum
        if self.get_habilitar(self.obj.getForum_input()) and 'forum' not in content:
            forum = _createObjectByType('CoPMenu',
                                        self.obj,
                                        'forum',
                                        title=u'Fórum',
                                        description=u'',
                                        titlemenu='forum')
            allowTypes(forum, ['PloneboardForum'])
            forum.unmarkCreationFlag()

            #cria um PloneboardForum padrao, que eh a view do CoPMenu
            pb_forum = _createObjectByType('PloneboardForum',
                                           forum,
                                           'forum',
                                           title=u'Fórum',
                                           description=u'')
            pb_forum.unmarkCreationFlag()
            forum.manage_addProperty('default_page', 'forum', 'string')

        #cria pasta tarefas
        if self.get_habilitar(self.obj.getTarefas_input()) and 'tarefas' not in content:
            tarefas = _createObjectByType('CoPMenu',
                                          self.obj,
                                          'tarefas',
                                          title=u'Tarefas',
                                          description=u'',
                                          titlemenu='tarefas')
            allowTypes(tarefas, ['CoPUpload'])
            tarefas.unmarkCreationFlag()

        #cria pasta configuracao
        if 'configuracoes' not in content:
            configuracoes = _createObjectByType('CoPMenu',
                                                self.obj,
                                                'configuracoes',
                                                title=u'Configurações',
                                                description=u'',
                                                titlemenu='configuracoes')
            allowTypes(configuracoes, [])
            configuracoes.manage_addProperty('layout','viewCoPConfig','string')
            configuracoes.unmarkCreationFlag()

        #cria pasta notificacao
        if 'notificacoes' not in content:
            notificacoes = _createObjectByType('CoPMenu',
                                                self.obj,
                                                'notificacoes',
                                                title=u'Notificações',
                                                description=u'',
                                                titlemenu='notificacoes')
            allowTypes(notificacoes, [])
            notificacoes.manage_addProperty('layout','viewCoPNotification','string')
            self.obj.portal_workflow.doActionFor(notificacoes, 'privado')
            notificacoes.unmarkCreationFlag()

        #cria pasta atividade
        if 'atividade' not in content:
            atividade = _createObjectByType('CoPMenu',
                                            self.obj,
                                            'atividade',
                                            title=u'Atividade',
                                            description=u'',
                                            titlemenu='atividade')
            allowTypes(atividade, [])
            atividade.manage_addProperty('layout','viewCoPActivity','string')
            self.obj.portal_workflow.doActionFor(atividade, 'privado')
            atividade.unmarkCreationFlag()

        #cria pasta formularios
        if self.obj.getAvailable_forms() and getProductInstalled("cop.forms"):
            if 'formularios' not in content:
                formularios = _createObjectByType('CoPMenu',
                                                   self.obj,
                                                   'formularios',
                                                   title=u'Formulários',
                                                   description=u'',
                                                   titlemenu='formularios')
            else:
                formularios = content_obj['formularios']
            allowTypes(formularios, self.obj.getAvailable_forms())
            formularios.unmarkCreationFlag()
        allowTypes(self.obj, [])

        setMembersCoPRole(self.obj, [self.obj.Creator()],"Moderador")
        updateCommunityCache(self.obj, self.obj.Creator(), 'Owner')

        setSuperCoPRoles(self.obj)
        if isSubCoP(self.obj):
            self.obj.__ac_local_roles_block__ = True
            self.obj.reindexObjectSecurity()

    def get_habilitar(self, input_value):
        """ return True or False depending on input_value
        """
        return input_value == OPCOES[0]

    def edit_content(self):
        """Edit content depending on what is filled on form
        """
        self.create_content()

        content_obj = self.obj.get_objects_titlemenu()
        content = content_obj.keys()

        portal_workflow = getToolByName(self.obj, 'portal_workflow')

        review_state_CoP = portal_workflow.getInfoFor(self.obj, "review_state", "")

        #Desabilita subcop
        if 'subcop' in content:
            obj_subcop = content_obj['subcop']
            review_state = portal_workflow.getInfoFor(obj_subcop, 'review_state', '')
            if not self.get_habilitar(self.obj.getSubcop_input()):
                if review_state != 'privado':
                    portal_workflow.doActionFor(obj_subcop, 'privado')
            elif review_state != review_state_CoP:
                portal_workflow.doActionFor(obj_subcop, review_state_CoP)

        #Desabilita pasta acervo - repositorio de documentos
        if 'acervo' in content:
            obj_acervo = content_obj['acervo']
            review_state = portal_workflow.getInfoFor(obj_acervo, 'review_state', '')
            if not self.get_habilitar(self.obj.getAcervo_input()):
                if review_state != 'privado':
                    portal_workflow.doActionFor(obj_acervo, 'privado')
            elif review_state != review_state_CoP:
                portal_workflow.doActionFor(obj_acervo, review_state_CoP)

        #Desabilita pasta calendario
        if 'calendario' in content:
            obj_calendario = content_obj['calendario']
            review_state = portal_workflow.getInfoFor(obj_calendario, 'review_state', '')
            if not self.get_habilitar(self.obj.getCalendario_input()):
                if review_state != 'privado':
                    portal_workflow.doActionFor(obj_calendario, 'privado')
            elif review_state != review_state_CoP:
                portal_workflow.doActionFor(obj_calendario, review_state_CoP)

        #Desabilita pasta portfolio
        if 'portfolio' in content:
            obj_portfolio = content_obj['portfolio']
            review_state = portal_workflow.getInfoFor(obj_portfolio, 'review_state', '')
            if not self.get_habilitar(self.obj.getPortfolio_input()):
                if review_state != 'privado':
                    portal_workflow.doActionFor(obj_portfolio, 'privado')
            elif review_state != review_state_CoP:
                portal_workflow.doActionFor(obj_portfolio, review_state_CoP)

        #Desabilita pasta forum
        if 'forum' in content:
            obj_forum = content_obj['forum']
            review_state = portal_workflow.getInfoFor(obj_forum, 'review_state', '')
            if not self.get_habilitar(self.obj.getForum_input()):
                if review_state != 'privado':
                    portal_workflow.doActionFor(obj_forum, 'privado')
                    #torna privados todos forums e conversations filhos
                    portal_catalog = getToolByName(obj_forum, "portal_catalog")
                    pb_content = portal_catalog(
                        path="/".join(obj_forum.getPhysicalPath()),
                        portal_type=["PloneboardForum","PloneboardConversation"],
                    )
                    for brain in pb_content:
                        pb_object = brain.getObject()
                        review_state = portal_workflow.getInfoFor(pb_object, 'review_state', '')
                        if review_state != "privado":
                            portal_workflow.doActionFor(pb_object, 'privado')
            elif review_state != review_state_CoP:
                portal_workflow.doActionFor(obj_forum, review_state_CoP)

        #Desabilita pasta tarefas
        if 'tarefas' in content:
            obj_tarefas = content_obj['tarefas']
            review_state = portal_workflow.getInfoFor(obj_tarefas, 'review_state', '')
            if not self.get_habilitar(self.obj.getTarefas_input()):
                if review_state != 'privado':
                    portal_workflow.doActionFor(obj_tarefas, 'privado')
            elif review_state != review_state_CoP:
                portal_workflow.doActionFor(obj_tarefas, review_state_CoP)

        #Desabilita pasta formularios
        if 'formularios' in content:
            obj_formularios = content_obj['formularios']
            review_state = portal_workflow.getInfoFor(obj_formularios, 'review_state', '')
            if not self.obj.getAvailable_forms() or not getProductInstalled("cop.forms"):
                if review_state != 'privado':
                    portal_workflow.doActionFor(obj_formularios, 'privado')
                    allowTypes(obj_formularios, self.obj.getAvailable_forms())
            elif review_state != review_state_CoP:
                portal_workflow.doActionFor(obj_formularios, review_state_CoP)

#EVENTS
def initialCreatedContents(obj, event):
    """Criacao inicial dos conteudos dentro da CoP
    """
    initial = AddContents(obj)
    initial.create_content()

def editionCreatedContents(obj, event):
    """Edicao dos conteudos dentro da CoP
    """
    edit = AddContents(obj)
    edit.edit_content()

def transitionParentState(obj, event):
    """Altera o estado do item criado para respeitar o estado da CoP onde esta contido
    """
    cop = getCoPContext(obj, ['CoP',])
    if cop:
        portal_workflow = cop.portal_workflow
        review_state_CoP = portal_workflow.getInfoFor(cop, 'review_state')
        action = False
        workflow_id = portal_workflow.getChainFor(obj.portal_type)[0]
        workflow = portal_workflow.getWorkflowById(workflow_id)
        #obtem uma lista das transicoes possiveis
        possible_transitions = []
        for transition in portal_workflow.getTransitionsFor(obj):
            possible_transitions.append(transition['id'])

        #procura qual das transicoes possiveis leva ao estado da CoP
        all_transitions = dict(workflow.transitions.items())
        for transition_id in possible_transitions:
            if all_transitions[transition_id].new_state_id == review_state_CoP:
                action = transition_id
                break

        if action:
            portal_workflow.doActionFor(obj, action)

def createEventRelationship(obj, event):
    eventId = event.object.REQUEST.form.get('CoPEvent', '')
    calendar = getCoPContext(obj, ['CoPMenu',])
    catalog = getToolByName(obj, 'portal_catalog')
    query = {}
    calendar_path = '/'.join(calendar.getPhysicalPath())
    query['path'] = {'query': calendar_path, 'depth': 1}
    query['portal_type'] = 'CoPEvent'
    query['id'] = eventId
    brain = catalog(query)
    evento = brain[0].getObject() if brain else None
    obj.setEvento(evento)
    evento.setAta(obj)


def setTitleCoPPortfolio(obj, event):
    """Ao criar o portfolio, sobrescrever o que foi escrito no title com o nome do criador
    """
    member_data = getMemberData(obj, obj.Creator())
    member_fullname = ""
    if member_data:
        member_fullname = member_data[0]
    if member_fullname:
        obj.setTitle(member_fullname)

def membersCommunitiesCache(obj, event):
    """Cacheia as comunidades em que o membro que esta logando participa
    """
    member_id = obj.getId()
    return listMemberRoleCommunitiesParticipating(member_id)

def cleanMembersCommunitiesCache(obj, event):
    """Limpa o cache do membro que esta deslogando
    """
    member_id = obj.getId()
    return cleanMemberCache(member_id)

def createCoPGroup(obj):
    """ Cria grupo de observadores para RootCoP.
    """
    if not isSubCoP(obj):
        portal_groups = getToolByName(obj, 'portal_groups')
        group_id = obj.UID()
        if not group_id in portal_groups.getGroupIds():
            portal_groups.addGroup(group_id)
            group_name = obj.Title()
            portal_groups.editGroup(group_id, title = group_name)
            obj.manage_setLocalRoles(group_id, ['Xita'])
        list_local_roles = obj.get_local_roles()
        for user_id, role in list_local_roles:
            if not user_id == group_id:
                portal_groups.addPrincipalToGroup(user_id, group_id)

def createMastersGroups(obj, event):
    """Cria grupos de moderadores master e
       observadores master para um CoPCommunityFolder
    """
    portal_groups = getToolByName(obj, 'portal_groups')
    #grupo de moderadores master
    group_id = "MM%s" % obj.UID()
    if not group_id in portal_groups.getGroupIds():
        portal_groups.addGroup(group_id)
        group_name = "MM Folder %s" % obj.Title()
        portal_groups.editGroup(group_id, title = group_name)
    obj.manage_setLocalRoles(group_id, ["Moderador_Master"])
    #grupo de observadores master
    group_id = "OM%s" % obj.UID()
    if not group_id in portal_groups.getGroupIds():
        portal_groups.addGroup(group_id)
        group_name = "OM Folder %s" % obj.Title()
        portal_groups.editGroup(group_id, title = group_name)
    obj.manage_setLocalRoles(group_id, ["Observador_Master"])
    return True

def setCoPGroup(obj):
    """ Seta grupo no local roles da CoP e SubCoPs.
    """
    if not isSubCoP(obj):
        group_id = obj.UID()
    else:
        root_cop = getRootCoP(obj)
        group_id = root_cop.UID()
    portal_groups = getToolByName(obj, 'portal_groups')
    if group_id in portal_groups.getGroupIds():
        obj.manage_setLocalRoles(group_id, ['Xita'])

    #adiciona permissao para grupos master
    mm_group = getMMGroup(obj)
    if mm_group:
        obj.manage_setLocalRoles(mm_group.getId(), ['Moderador_Master'])
    om_group = getOMGroup(obj)
    if om_group:
        obj.manage_setLocalRoles(om_group.getId(), ['Observador_Master'])
    obj.reindexObjectSecurity()

def setSubCoPLevel(obj, event):
    depth = getSubCoPDepth(obj)
    obj.setSubcop_level(depth)
    obj.reindexObject()

def deleteCoPGroup(obj, event):
    """ Exclui grupo de observadores da RootCoP que é excluida
    """
    group_id = obj.UID()
    portal_groups = getToolByName(obj, 'portal_groups')
    if group_id in portal_groups.getGroupIds():
        portal_groups.removeGroup(group_id)
        return True
    return False

def deleteMastersGroups(obj, event):
    """ Exclui os grupos masters do CoPCommunityFolder excluido
    """
    portal_groups = getToolByName(obj, 'portal_groups')
    result = False
    mm_group_id = "MM%s" % obj.UID()
    if mm_group_id in portal_groups.getGroupIds():
        portal_groups.removeGroup(mm_group_id)
        result = True
    om_group_id = "OM%s" % obj.UID()
    if om_group_id in portal_groups.getGroupIds():
        portal_groups.removeGroup(om_group_id)
        result = True
    return result


def reindexSuperCoP(obj, event):
    """ Atualiza indices e metadados da SuperCoP.
        Entre os metadados a serem atualizados esta
        o numero de subcomunidades de uma SuperCoP.
    """
    from zope.component import queryUtility
    from Products.CMFCore.interfaces import IPropertiesTool

    # We need to disable link integrity check,
    # because it cannot handle several delete calls in
    # one request
    ptool = queryUtility(IPropertiesTool)
    props = getattr(ptool, 'site_properties', None)
    old_check = props.getProperty('enable_link_integrity_checks', False)
    link_integrity_events_counter = getattr(obj.REQUEST, 'link_integrity_events_counter', 1)
    if link_integrity_events_counter > 1 or not old_check or type(event) == ObjectInitializedEvent:
        if isSubCoP(obj):
            super_cop = getSuperCoP(obj)
            if super_cop:
                super_cop.reindexObject()

def updateCoPLastModification(obj, event):
    """ Reindex do objeto comentado para atualizacao
        do index copLastModification
    """
    commented_obj = aq_parent(aq_parent(obj))
    if commented_obj.portal_type == 'CoPShare':
        commented_obj.setTimeline_listed(True)
    commented_obj.reindexObject()


def checkCanDelete(obj, event):
    """ Checks if authenticated member can delete CoPFolder
    """
    if not canDeleteCoPFolder(obj):
        raise Unauthorized

def removeCoPFromCache(obj, event):
    """Removes deleted CoP from cache
    """
    from zope.component import queryUtility
    from Products.CMFCore.interfaces import IPropertiesTool

    # We need to disable link integrity check,
    # because it cannot handle several delete calls in
    # one request
    ptool = queryUtility(IPropertiesTool)
    props = getattr(ptool, 'site_properties', None)
    old_check = props.getProperty('enable_link_integrity_checks', False)
    link_integrity_events_counter = getattr(obj.REQUEST, 'link_integrity_events_counter', 1)
    if link_integrity_events_counter > 1 or not old_check:
        cop_UID = obj.UID()
        return cleanCoPCache(cop_UID)

def createQuickUploadPortlet(obj):
    if obj.portal_quickinstaller.isProductInstalled('collective.quickupload'):
        from collective.quickupload.portlet import quickuploadportlet
        from zope.component import getMultiAdapter
        from zope.component import getUtility
        from plone.portlets.interfaces import IPortletAssignmentMapping
        from plone.portlets.interfaces import IPortletManager
        from zope.container.interfaces import INameChooser
        column = getUtility(IPortletManager, name=u"plone.rightcolumn")
        manager = getMultiAdapter((obj, column,), IPortletAssignmentMapping)
        assignment = quickuploadportlet.Assignment(header="Inserir Arquivos")
        chooser = INameChooser(manager)
        manager[chooser.chooseName(None, assignment)] = assignment


def setSolgemaCalendarView(calendar):
    if calendar.portal_quickinstaller.isProductInstalled('Solgema.fullcalendar'):
        calendar.manage_addProperty('layout','viewCoPCalendar','string')
