# -*- coding: utf-8 -*-
from communities.practice.config import USE_MEMCACHED
from plone.memoize.interfaces import ICacheChooser
from plone.memoize.ram import MemcacheAdapter
from plone.memoize.ram import RAMCacheAdapter
from zope import component
from zope.interface import directlyProvides
from zope.ramcache.interfaces.ram import IRAMCache
import os
import threading

thread_local = threading.local()

def choose_cache(fun_name):
    if USE_MEMCACHED:
        import memcache
        global servers
        
        client=getattr(thread_local, "client", None)
        if client is None:
            servers = os.environ.get(
                "MEMCACHE_SERVER", "127.0.0.1:11211").split(",")
            client = thread_local.client = memcache.Client (servers, debug = 0)

        return MemcacheAdapter(client)
    else:
        return RAMCacheAdapter(component.queryUtility(IRAMCache),
                               globalkey=fun_name)

directlyProvides(choose_cache, ICacheChooser)
