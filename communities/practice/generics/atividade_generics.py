# -*- coding: utf-8 -*-
import re
from datetime import datetime
from datetime import timedelta
from DateTime import DateTime

from Products.CMFPlone.utils import getToolByName

from communities.practice.generics.cache import getCoPLocalRoles
from communities.practice.generics.generics import encodeUTF8
from communities.practice.generics.generics import exportCSVDownload
from communities.practice.generics.generics import getCoPContext
from communities.practice.generics.generics import getSubCoPDepth
from communities.practice.generics.vocabularies import \
    AVAILABLE_FORMS_VOCABULARY
from communities.practice.content.CoP import CoP


TYPES_ATIVIDADE = {}
TYPES_ATIVIDADE['comunidade'] = [
    ('CoPFile', u'Arquivos'),
    ('CoPImage', u'Imagens'),
    ('CoPDocument', u'Páginas'),
    ('CoPEvent', u'Eventos'),
    ('CoPATA', u'Atas'),
    ('CoPLink', u'Links'),
    ('Discussion Item', u'Comentários'),
    ('PloneboardComment', u'Fórum'),
]
TYPES_ATIVIDADE['acervo'] = [
    ('CoPFile', u'Arquivos'),
    ('CoPImage', u'Imagens'),
    ('CoPDocument', u'Páginas'),
    ('CoPLink', u'Links'),
]
TYPES_ATIVIDADE['portfolio'] = [
    ('CoPFile', u'Arquivos'),
    ('CoPImage', u'Imagens'),
    ('CoPDocument', u'Páginas'),
]
TYPES_ATIVIDADE['forum'] = [
    ('PloneboardConversation', u'Tópicos'),
    ('PloneboardComment', u'Comentários'),
]
TYPES_ATIVIDADE['tarefas'] = [
    ('CoPFile', u'Arquivos'),
]
TYPES_ATIVIDADE['formularios'] = AVAILABLE_FORMS_VOCABULARY


def getCoPAtividadeMembers(community, search_term=None):
    """Returns a list of tuples with (id, fullname) of Community members
    """
    members = []
    roles = ['Participante', 'Moderador', 'Bloqueado']
    members_by_role = getCoPLocalRoles(community, False).get(community.UID())
    for role in roles:
        members += members_by_role[role]

    if search_term:
        filter_list = []
        for member in members:
            filtered = re.search(search_term, member[1], re.I)
            if filtered:
                filter_list.append(tuple(member))
        return filter_list

    memberlist = []
    for member in members:
        memberlist.append(tuple(member))
    return memberlist


def getCoPAtividadeContent(community, member_list, folder="comunidade", period=[]):
    """Retorna conteudo de cada membro da comunidade
       ordenado por participante.
       Pode ser passado por parametro pasta onde sera
       feita a pesquisa (tarefas, portfolio).
    """
    community_path = "/".join(community.getPhysicalPath())
    catalog_path = community_path
    if folder != 'comunidade':
        catalog_path = "%s/%s" % (community_path, folder)
    cop_depth = getSubCoPDepth(community)
    portal_catalog = getToolByName(community, "portal_catalog")

    """content(dict) tem tupla( 'id', 'nome') como chave
    e como valor um dicionario onde chave eh uma tupla com
    tipo de objetos( 'tipo', 'label') e valor total(int).
    """
    content = {}
    query = {}
    query["path"] = catalog_path
    query["subCoPDepth"] = cop_depth
    if period:
        query["created"] = {"query": period, 'range': 'min:max'}
    for user in member_list:
        content[user] = {}
        total_participante = 0
        types = getCoPAtividadeTypes(folder, False, community)
        for tipo in types:
            query["Creator"] = user[0]
            query["portal_type"] = tipo[0]
            query.pop("id", None)
            total_tipo = len(portal_catalog(query))
            content[user][tipo] = total_tipo
            total_participante += total_tipo
        content[user][('total', 'Total')] = total_participante
    return content


def getCoPAtividadeTypes(folder='comunidade', add_total=True, context=None):
    """Retorna os tipos considerados pela view atividades
    """
    types = list(TYPES_ATIVIDADE.get(folder))
    # para atividades dos formularios, filtrar os tipos permitidos
    if context:
        cop = getCoPContext(context)
        if folder == 'comunidade':
            forms = []
            for form, description in TYPES_ATIVIDADE.get("formularios"):
                if form in cop.available_forms:
                    forms.append(form)
            if forms:
                types.append((tuple(forms), u"Formulários"))
        elif folder == "formularios":
            copy_types = list(types)
            for item in copy_types:
                if item[0] not in cop.available_forms:
                    types.remove(item)

    if add_total and folder != "tarefas":
        types.append(('total', 'Total'))
    return types


def getCoPCommunityFolderAtividadeTypes():
    """Retorna os tipos considerados pela view de
       atividades no CoPCommunityFolder
    """
    types = list(TYPES_ATIVIDADE.get("comunidade"))
    forms = []
    for form, description in TYPES_ATIVIDADE.get("formularios"):
        forms.append(form)
    if forms:
        types.append((tuple(forms), u'Formulários'))
    types.append(('total', 'Total'))
    return types


def getCoPAtividadeTotalTypes(community, folder="comunidade", period=[]):
    """Retorna conteudo da comunidade ordenado por participante.
       Pode ser passado por parametro pasta onde sera feita a pesquisa
       (tarefas, portfolio, acervo, forum, formularios).
    """
    if not isinstance(community, CoP):
        community = community.getObject()
    catalog_path = "/".join(community.getPhysicalPath())
    if folder != 'comunidade' and folder != "copcommunityfolder":
        catalog_path = "%s/%s" % (catalog_path, folder)
    cop_depth = getSubCoPDepth(community)

    total_content = {}
    if folder == "copcommunityfolder":
        types = getCoPCommunityFolderAtividadeTypes()
    else:
        types = getCoPAtividadeTypes(folder, False, community)
    for tipo in types:
        total_content[tipo] = 0
    total_geral = 0
    query = {}
    query["path"] = catalog_path
    query["subCoPDepth"] = cop_depth
    if period:
        query["created"] = {"query": period, 'range': 'min:max'}
    for tipo in types:
        query["portal_type"] = tipo[0]
        query.pop("id", None)
        total_tipo = len(community.portal_catalog(query))
        total_content[tipo] += total_tipo
        total_geral += total_tipo
    if folder != "tarefas":
        total_content[('total', 'Total')] = total_geral
    return total_content


def getCoPAtividadeLenMembers(obj):
    cop = getCoPContext(obj)
    cop_local_roles = getCoPLocalRoles(cop, False)
    cop_local_roles = cop_local_roles.get(cop.UID())
    return len(cop_local_roles['Participante']) + \
        len(cop_local_roles['Moderador'])


def getCoPAtividadeCommunities(copcommunityfolder):
    """Retorna uma lista de dicionarios com os dados das CoPs de um folder
    """
    list_communities = []
    portal_catalog = getToolByName(copcommunityfolder, "portal_catalog")
    folder_path = "/".join(copcommunityfolder.getPhysicalPath())
    communities = portal_catalog(
        path=folder_path,
        portal_type="CoP",
    )
    for community in communities:
        community_dict = {}
        community_dict["title"] = community.Title
        community_dict["parent"] = ""
        if community.partOfCommunity:
            community_dict["parent"] = "Subcomunidade de %s" % (
                community.partOfCommunity[1],
            )
        community_dict["created"] = community.created.strftime("%d/%m/%Y")
        cop_local_roles = getCoPLocalRoles(community).get(community.UID)
        community_dict["participants"] = \
            len(cop_local_roles["Participante"]) + \
            len(cop_local_roles["Moderador"])
        community_dict["url"] = "%s/atividade" % community.getURL()
        community_dict["brain"] = community
        list_communities.append(community_dict)

    return list_communities


def exportCoPCommunityFolderAtividade(request, copcommunityfolder):
    """Exporta os dados da view de atividades do CoPCommunityFolder
    """
    filename = "Atividades_%s_%s.csv" % (
        copcommunityfolder.Title(), datetime.now().strftime("%d_%m_%Y"),
    )
    header = [
        "Comunidade",
        "Data Criacao",
        "Participantes",
    ]
    # o resto do header e dinamico conforme as colunas (types)
    types = getCoPCommunityFolderAtividadeTypes()
    for tipo, descricao in types:
        header.append(encodeUTF8(descricao))

    # caso esteja filtrando por data
    period = getStartEndDate(request)

    # conteudo de cada comunidade
    data = []
    communities = getCoPAtividadeCommunities(copcommunityfolder)
    for community in communities:
        total_content = getCoPAtividadeTotalTypes(
            community["brain"], "copcommunityfolder", period,
        )
        if total_content[("total", u"Total")]:
            row = []
            row.append(community["title"])
            row.append(community["created"])
            row.append(community["participants"])
            for tipo in types:
                row.append(total_content[tipo])
            data.append(row)

    exportCSVDownload(request, header, data, filename)
    return False


def getStartEndDate(request):
    """
    """
    # data inicial padrao eh o inicio dos tempos
    start_date = DateTime("2000/01/01")
    # data final padrao eh amanha
    end_date = DateTime(datetime.today() + timedelta(days=1))

    start = request.get('start_date_copactivity', False)
    end = request.get('end_date_copactivity', False)

    if start:
        start_date = DateTime(start)
    if end:
        end_date = DateTime(end) + 1

    return [start_date, end_date]
