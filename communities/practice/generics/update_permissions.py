# -*- coding: utf-8 -*-

# Credits to https://github.com/RedTurtle/redturtle.deletepolicy
# Monkey patch for default manage_cutObjects behaviour. 
# We don't want to have ModifyPortalContent permission to be allow to manage_cutObjects.
# So now we are using 'Delete objects'

import logging
from Globals import InitializeClass
from Products.CMFCore import permissions
from AccessControl.Permissions import delete_objects
from communities.practice.content.CoPMenu import CoPMenu
from communities.practice.content.CoPFolder import CoPFolder

logger = logging.getLogger("communities.practice")


def _update_permissionsCut(_class):
    old_permissions = dict(_class.__ac_permissions__).copy()
    _class.__ac_permissions__ = ((delete_objects, ('manage_cutObjects',)),) + tuple(old_permissions.items())
    InitializeClass(_class)
    
_update_permissionsCut(CoPMenu)
_update_permissionsCut(CoPFolder)
logger.warning("*** Monkeypatched default permission for cut objects (from ModifyPortalContent to DeleteObject) ***")


def _update_permissionsPaste(_class):
    old_permissions = dict(_class.__ac_permissions__).copy()
    _class.__ac_permissions__ = ((permissions.AddPortalContent, ('manage_pasteObjects',)),) + tuple(old_permissions.items())
    InitializeClass(_class)
    
_update_permissionsPaste(CoPMenu)
_update_permissionsPaste(CoPFolder)
logger.warning("*** Monkeypatched default permission for paste objects (from ModifyPortalContent to AddPortalContent) ***")
