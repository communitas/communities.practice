# -*- coding: utf-8 -*-
import textwrap
import string
import re
import csv

from itertools import chain

from AccessControl import getSecurityManager
from Acquisition import aq_parent, aq_inner, aq_chain
from plone.app.discussion.interfaces import IDiscussionSettings
from plone.i18n.normalizer.interfaces import IIDNormalizer
from plone.registry.interfaces import IRegistry
from Products.CMFCore.utils import getToolByName
from Products.CMFPlone.utils import _createObjectByType
from Products.CMFPlone.utils import normalizeString
from zope.component import getMultiAdapter
from zope.component import getUtility
from zope.component import queryUtility
from zope.component.hooks import getSite
from zope.event import notify

from communities.practice.event import CoPLocalRolesChangedEvent
from communities.practice.event import CoPMasterRolesChangedEvent


def cleanhtml(raw_html):
    """Remove HTML tags from a string
    """
    cleanr = re.compile('<.*?>')
    cleantext = re.sub(cleanr, '', raw_html)
    return cleantext


def limit_description(description, limit=139):
    """Restrict a description text down to a certain character limit."""
    wrapped = textwrap.wrap(unicode(description, "utf-8"), width=limit)
    if wrapped:
        first_line = wrapped[0].strip(string.punctuation)
        return (first_line + u"\u2026").encode("utf-8")
    return ""


def getCoPContext(obj, get_type=['CoP', 'CoPCommunityFolder']):
    """ Bottom-up function that returns first community or communities folder.
        You can force search to return only community or communities folder.
    """
    for parent in aq_chain(aq_inner(obj)):
        if hasattr(parent, 'portal_type'):
            if parent.portal_type in get_type:
                return parent
    return False


def searchPortalUsers(context, request, searchString='', ignore=[]):
    """
        Search for users, returning list data member
    """
    portal = getSite()
    userResults = []
    users_list = []
    acl = getToolByName(context, 'acl_users')
    searchView = getMultiAdapter(
        (aq_inner(context), request),
        name='pas_search',
    )

    userResults = searchView.merge(
        chain(
            *[searchView.searchUsers(**{field: searchString})
                for field in ['login', 'fullname', 'email']]
        ),
        'userid',
    )

    for user in userResults:
        user_id = user['id']
        if user_id in ignore:
            continue
        user = acl.getUserById(user_id)
        # play safe, though this should never happen
        if user:
            user_id = user.getId()
            fullname = encodeUTF8(
                user.getProperty('fullname', 'Fullname missing')
            )
            email = user.getProperty('email', None)
            author_url = "%s/author/%s" % (portal.absolute_url(), user_id)
            if not fullname:
                fullname = user_id
            users_list.append({
                "id": user_id,
                "full_name": fullname.capitalize(),
                "email": email,
                "author_url": author_url,
            })
    users_list.sort(
        key=lambda x: x is not None and x['full_name'] is not None
        and normalizeString(x['full_name']) or ''
    )
    return users_list


def getMemberData(obj, user_id):
    """ Recebe um objeto e uma string com o ID do membro.
        Retorna uma tupla com fullname e email"""
    portal = obj.portal_url.getPortalObject()
    user = portal.portal_membership.getMemberById(user_id)
    if user:
        fullname = encodeUTF8(
            user.getProperty('fullname', 'Fullname missing')
        )
        email = user.getProperty('email', None)
        if not fullname:
            fullname = user_id
        return (fullname, email)
    return False


def getMembersData(obj, users_list_id):
    """ Recebe um objeto e uma lista com o ID dos membros.
        Retorna um lista de dicionarios com
        id, full_name, email, img_url e author_url
    """
    portal = obj.portal_url.getPortalObject()
    users_list = []
    for user_data in users_list_id:
        user = portal.portal_membership.getMemberById(user_data)
        if user:
            fullname = encodeUTF8(
                user.getProperty('fullname', 'Fullname missing')
            )
            email = user.getProperty('email', None)
            img_url = portal.portal_membership.\
                getPersonalPortrait(user.getId()).absolute_url()
            author_url = "%s/author/%s" % (
                portal.absolute_url(), user.getId(),
            )
            if not fullname:
                fullname = user.getId()
            users_list.append({
                "id": user.getId(),
                "full_name": fullname,
                "email": email,
                "img_url": img_url,
                "author_url": author_url,
            })
    return users_list


def encodeUTF8(texto):
    """codifica uma string para utf-8
    """
    try:
        encoded = texto.encode('utf-8')
    except:
        encoded = texto
    return encoded


def allowTypes(obj, list_types=[]):
    """Locally allow inclusion of a list of types in an object
    """
    obj.setConstrainTypesMode(True)
    obj.setLocallyAllowedTypes(list_types)
    obj.setImmediatelyAddableTypes(list_types)
    obj.reindexObject()


def getCoPParticipants(cop):
    """ Receives CoP object and returns list of participants.
    """
    participants = []
    list_local_roles = cop.get_local_roles()
    for user_id, roles in list_local_roles:
        if (("Participante" in roles) or
                ("Moderador" in roles) or
                ("Owner" in roles)):
            participants.append(user_id)
    return participants


def getCoPRole(cop_uid, user_id):
    """ Receives CoP UID and user id and returns
        user role from community cache.
    """
    from communities.practice.generics.cache import \
        listMemberRoleCommunitiesParticipating
    member_communities = listMemberRoleCommunitiesParticipating(user_id)
    member_communities = member_communities.get(user_id)
    role = member_communities.get(cop_uid, "")
    return role


def getLocalRole(context, user_id):
    """ Receives context and user id and returns user role from local roles.
    """
    return context.get_local_roles_for_userid(user_id)


def getReviewStateTitle(context, review_state_id=""):
    """ Gets review state title for context or review_state_id
    """
    review_state_title = ""
    portal_workflow = getToolByName(context, 'portal_workflow')
    if not review_state_id:
        review_state_id = portal_workflow.getInfoFor(context, 'review_state')
    workflows_ids = portal_workflow.getChainFor(context.portal_type)
    if workflows_ids:
        workflow_id = workflows_ids[0]
        workflow = portal_workflow.getWorkflowById(workflow_id)
        state = workflow.states.get(review_state_id)
        if state:
            review_state_title = state.title
    return {'id': review_state_id, 'title': review_state_title}


def getReviewStateDescription(review_state_id):
    descriptions = {
        'publico': 'qualquer pessoa autenticada ou não no portal',
        'restrito': 'apenas participantes desta comunidade',
        'privado': 'apenas o criador e moderadores da comunidade',
        'autenticado': 'apenas usuários autenticados no portal',
        'compartilhado_na_arvore':
        'apenas participantes da hierarquia de subcomunidades',
        'aguardando': 'portfólio aguardando permissão de acesso',
        'bloqueado': 'portfólio bloqueado para o usuário',
        'fechado': 'apenas moderadores podem encerrar a tarefa',
    }
    return descriptions.get(review_state_id, '')


def getBrainValues(local, brain_list, datetime_type=""):
    """ Receives a catalog brain list.
        Returns a list of dictionaries.
    """
    portal_url = local.portal_url()
    portal_membership = getToolByName(local, 'portal_membership')
    news = []
    for item in brain_list:
        user = getMemberData(local, item.Creator)
        if user:
            title = encodeUTF8(item.Title)
            if not title:
                title = item.id
            descricao = encodeUTF8(item.Description)
            id_criador = item.Creator
            imagem_criador = portal_membership.\
                getPersonalPortrait(id_criador).absolute_url()
            url = item.getURL()
            path = item.getPath()
            tipo_item = item.portal_type
            label_tipo = item.Type
            review_state = item.review_state
            size = item.getObjSize
            partOfCommunity = item.partOfCommunity
            subject = item.Subject
            mensagem = "postou"

            if label_tipo == 'Pagina':
                label_tipo = 'Página'
            url_imagem = ''
            if tipo_item in ["CoPImage", "CoPProfileImage"]:
                url_imagem = "%s" % url
            remote_url = ''
            if tipo_item == "CoPLink":
                remote_url = item.getRemoteUrl
            if tipo_item == "CoPShare":
                mensagem = "compartilhou"
            datetime = item.created
            if datetime_type == "modified":
                datetime = item.modified

            news.append({
                "titulo": title,
                "UID": item.UID,
                "id": item.id,
                "nome_criador": user[0],
                "id_criador": id_criador,
                "descricao": descricao,
                "url": url,
                "tipo": tipo_item,
                "label_tipo": label_tipo,
                "imagem_criador": imagem_criador,
                "partOfCommunity": partOfCommunity,
                "subject": subject,
                "timestamp": datetime,
                "url_imagem": url_imagem,
                "url_criador": "%s/author/%s" % (portal_url, id_criador),
                "url_icone": "%s/%s" % (portal_url, item.getIcon),
                "remote_url": remote_url,
                "path": path,
                "estado": review_state,
                "tamanho": size,
                "mensagem": mensagem
            })
    return news


def getCoPUpdates(
        local, limit=0, types='',
        searchable_text='', UID='', path='',
        creator='', timeline_listed=True):
    """ Search types(string list) into local(obj).
        Limit = 0 returns all values.
        Returns dict list from getBrainValues.
    """
    catalog = getToolByName(local, 'portal_catalog')
    if not path:
        path = '/'.join(local.getPhysicalPath())
    if not types:
        types = [
            "CoPEvent", "CoPLink", "CoPImage",
            "CoPShare", "CoPFile", "CoPDocument",
            "CoPATA", "CoPPost", "PloneboardConversation",
            "BFCheckList", "BFGoodPracticeReport", "BFVisitAcknowledge",
            "CoPCaseDescription", "CoPTutors",
        ]
    query = {}
    query['timeline_listed'] = timeline_listed
    query['path'] = path
    query['portal_type'] = types
    if searchable_text:
        searchable_text = "%s*" % (searchable_text)
        query['SearchableText'] = searchable_text
    if limit:
        query['sort_limit'] = limit
    if UID:
        query['UID'] = UID
    if creator:
        query['Creator'] = creator
    query['sort_on'] = 'copLastModification'
    query['sort_order'] = 'reverse'
    items = catalog(query)
    return items


def getUsersByRole(obj, user_type=["Participante"]):
    """Recebe o objeto onde sera feita a pesquisa e
       uma lista de strings com o tipo de usuario.
       Retorna um dicionario onde a chave é o tipo de usuario e o
       valor é uma lista de usuarios com
       (id, nome, email, url_img, url_author).
    """
    portal = obj.portal_url.getPortalObject()
    portal_membership = getToolByName(obj, "portal_membership")
    users_list = {}
    for role in user_type:
        if role == "Moderador_Master":
            list_ids = getMMGroupMembers(obj)
        elif role == "Observador_Master":
            list_ids = getOMGroupMembers(obj)
        else:
            list_ids = obj.users_with_local_role(role)

        users_list[role] = []
        for user in list_ids:
            user_full = getMemberData(obj, user)
            if user_full and user_full[1]:
                img_url = portal_membership.\
                    getPersonalPortrait(user).absolute_url()
                author_url = "%s/author/%s" % (portal.absolute_url(), user)
                users_list[role].append({
                    "id": user,
                    "full_name": user_full[0],
                    "email": user_full[1],
                    "img_url": img_url,
                    "author_url": author_url,
                })
    return users_list


def getCommunitiesParticipating(user, local):
    """ Busca as comunidades, a partir do local, que o usuario participa.
    """
    portal = getToolByName(local, 'portal_url').getPortalObject()
    catalog = getToolByName(portal, 'portal_catalog')
    options = {}
    options['portal_type'] = "CoP"
    options['path'] = '/'.join(local.getPhysicalPath())
    communities = catalog(options)
    communities_participating = []
    for brain_community in communities:
        community = brain_community.getObject()
        local_roles = community.get_local_roles_for_userid(user.getId())
        if (local_roles and 'Bloqueado' not in local_roles and
                'Aguardando' not in local_roles):
            communities_participating.append(community)
    return communities_participating


def getCommentaries(context, path):
    """ Busca Comentarios a partir do path. Pode ser limitado
        atraves do parametro qtd.
        Retorna no formato lista de dicionarios.
    """
    catalog = getToolByName(context, 'portal_catalog')
    query = {}
    query['sort_on'] = 'created'
    query['path'] = path
    query['portal_type'] = ['Discussion Item', 'PloneboardComment']
    commentaries = catalog(query)
    brain_values = getBrainValues(context, commentaries)
    return brain_values


def isDiscussionAllowed(context, cop_uid):
    """ Verifica se comentarios sao permitidos para o objeto.
    """
    from communities.practice.generics.cache import \
        listMemberRoleCommunitiesParticipating

    can_reply = getSecurityManager().\
        checkPermission('Reply to item', aq_inner(context))
    portal_membership = getToolByName(context, 'portal_membership')
    authenticated_member = portal_membership.getAuthenticatedMember()
    authenticated_id = authenticated_member.getId()
    if cop_uid:
        communities_participating = \
            listMemberRoleCommunitiesParticipating(authenticated_id)
        communities_participating = \
            communities_participating.get(authenticated_id)
        authenticated_role = communities_participating.get(cop_uid)
        can_reply = can_reply and \
            authenticated_role in ["Participante", "Moderador", "Owner"]
    # Check if anonymous comments are allowed in the registry
    registry = queryUtility(IRegistry)
    settings = registry.forInterface(IDiscussionSettings, check=False)
    anonymous_discussion_allowed = settings.anonymous_comments
    globally_enabled = settings.globally_enabled

    return globally_enabled and (can_reply or anonymous_discussion_allowed)


def getProductInstalled(product_name):
    """
    """
    portal = getSite()
    installed = portal.portal_quickinstaller.isProductInstalled(product_name)
    return installed


def changeCoPOwnership(community, user_id, permission="Excluir"):
    """ Recebe objeto CoP e id do usuário.
        Altera Owner e Creator de todos os tipos
        container criados pelo usuario.
        permission pode receber: Excluir, Bloqueado ou Observador
    """
    from communities.practice.generics.cache import updateCommunityCache
    cop_owner = community.getOwner()
    cop_owner_id = cop_owner.getId()

    catalog = getToolByName(community, "portal_catalog")
    query = {}
    query['Creator'] = user_id
    query['path'] = '/'.join(community.getPhysicalPath())
    query['portal_type'] = ['CoPFolder', 'CoP', 'CoPMenu', 'CoPUpload']
    cop_objects = catalog(query)
    for cop_obj in cop_objects:
        obj = cop_obj.getObject()
        # apaga papeis do cara removido
        obj.manage_delLocalRoles([user_id])
        # seta o owner da CoP como creator do objeto
        obj.setCreators(cop_owner_id)
        # seta o owner da CoP como owner do objeto
        obj.changeOwnership(cop_owner, recursive=False)
        # pega os roles atuais do owner da CoP
        cop_owner_roles = list(obj.get_local_roles_for_userid(cop_owner_id))
        # se ele nao tiver papel de owner no objeto,
        # adiciona o owner na lista de papeis
        if 'Owner' not in cop_owner_roles:
            cop_owner_roles.append('Owner')
        # se for uma CoP
        if obj.portal_type == "CoP":
            # o owner tem que ser Moderador tambem
            if "Moderador" not in cop_owner_roles:
                cop_owner_roles.append('Moderador')
            # atualizar o cache
            updateCommunityCache(obj, cop_owner_id, 'Owner')
            updateCommunityCache(obj, cop_owner_id, 'Moderador')

            old_owner_roles = []
            # se for Observador ou Bloqueado
            if permission != "Excluir":
                # atualiza o papel do old owner
                old_owner_roles.append(permission)
                obj.manage_setLocalRoles(user_id, old_owner_roles)
                updateCommunityCache(obj, user_id, permission)
            else:
                # tira o old owner do cache
                updateCommunityCache(obj, user_id, None)
        # seta o local roles do owner da CoP no objeto
        obj.manage_setLocalRoles(cop_owner_id, cop_owner_roles)
        obj.reindexObject()
        obj.reindexObjectSecurity()


def blockPortfolio(community, user_id):
    """Bloqueia portfolio de usuarios excluidos da CoP.
    """
    catalog = getToolByName(community, "portal_catalog")
    portal_workflow = getToolByName(community, "portal_workflow")
    query = {}
    query['portal_type'] = 'CoPPortfolio'
    query['Creator'] = user_id
    query['path'] = '/'.join(community.getPhysicalPath())
    brain = catalog(query)
    for portfolio in brain:
        obj = portfolio.getObject()
        if (portal_workflow.getInfoFor(obj, 'review_state') in
                ['publico', 'restrito']):
            portal_workflow.doActionFor(obj, 'privado')
            portal_workflow.doActionFor(obj, 'bloqueado')
            obj.reindexObjectSecurity()
            return
        if portal_workflow.getInfoFor(obj, 'review_state') == 'privado':
            portal_workflow.doActionFor(obj, 'bloqueado')
            obj.reindexObjectSecurity()


def setMasterRole(community_folder, user_id, permission):
    """ Insere usuario no grupo de masters.
    """
    mm_group = getMMGroup(community_folder)
    if permission == "Moderador_Master":
        mm_group.addPrincipalToGroup(user_id, mm_group.getId())
    else:
        mm_group.removePrincipalFromGroup(user_id, mm_group.getId())
    om_group = getOMGroup(community_folder)
    if permission == "Observador_Master":
        om_group.addPrincipalToGroup(user_id, om_group.getId())
    else:
        om_group.removePrincipalFromGroup(user_id, om_group.getId())
    notify(CoPMasterRolesChangedEvent(community_folder, user_id, permission))
    return True


def setMembersCoPRole(community, member_ids, permission, recursive=True):
    """ Insere permissao para a comunidade recebida por parametro e
        propaga para as SubCoPs conforme tipo do papel. Ticket #747.
    """
    from communities.practice.generics.cache import updateCommunityCache

    portal_catalog = getToolByName(community, 'portal_catalog')
    query = {}
    query['portal_type'] = 'CoP'
    query['path'] = {'query': '/'.join(community.getPhysicalPath())}
    if not recursive:
        query['path']['depth'] = 0
    if permission == "Aguardando":
        brain = []
    else:
        brain = portal_catalog(query)

    for cop_brain in brain:
        cop = cop_brain.getObject()
        for user_id in member_ids:
            name, email = getMemberData(community, user_id)
            old_local_roles = list(cop.get_local_roles_for_userid(user_id))
            new_roles = []

            if permission == "Participante":
                # teste invertido para ser compativel com coverage
                if "Bloqueado" not in old_local_roles:
                    continue  # para SubCoP onde usuario nao esta
                    # bloqueado mantem o papel atual
                else:
                    # propagacao do Participante somente nas subs
                    # onde estava bloqueado
                    new_roles.append(permission)
                    updateCommunityCache(cop, user_id, permission)
            elif permission == "Excluir":
                updateCommunityCache(cop, user_id, None)
                changeCoPOwnership(cop, user_id, permission)
                blockPortfolio(cop, user_id)
                deleteCoPShareUser(community, user_id)
            elif permission == "Moderador":
                if "Owner" in old_local_roles:
                    new_roles.append("Owner")
                new_roles.append(permission)
                updateCommunityCache(cop, user_id, permission)
            elif permission == "Bloqueado" and old_local_roles:
                new_roles.append(permission)
                updateCommunityCache(cop, user_id, permission)
                changeCoPOwnership(cop, user_id, permission)
                blockPortfolio(cop, user_id)
            elif permission == "Observador":
                new_roles.append(permission)
                updateCommunityCache(cop, user_id, permission)
                changeCoPOwnership(cop, user_id, permission)
                blockPortfolio(cop, user_id)

            if new_roles:
                cop.manage_setLocalRoles(user_id, new_roles)
            else:
                cop.manage_delLocalRoles([user_id])
            # dispara o evento de modificacao de local roles
            notify(CoPLocalRolesChangedEvent(cop, user_id, permission))

    # Garante alteracao na comunidade onde foi setado o
    # papel para participante ou aguardando.
    if permission in ["Participante", "Aguardando"]:
        for user_id in member_ids:
            community.manage_setLocalRoles(user_id, [permission])
            updateCommunityCache(community, user_id, permission)
            # dispara o evendo de modificacao de local roles
            notify(CoPLocalRolesChangedEvent(community, user_id, permission))

    community.reindexObjectSecurity()


def isSubCoP(community):
    """ Recebe objeto CoP.
        Retorna True para SubCoP e False para RootCoP.
    """
    parent = aq_parent(aq_inner(community))
    if parent.portal_type == "CoPMenu":
        return True
    return False


def setSuperCoPRoles(community):
    """ Recebe objeto CoP que esta sendo criado.
        Metodo busca por papeis da lista no nivel superior
        e os insere no local roles da SubCoP.
    """
    from communities.practice.generics.cache import updateCommunityCache
    roles_list = [
        "Moderador",
        "Observador",
    ]
    roles_to_cache = ["Moderador", "Observador"]
    cop_context = getCoPContext(aq_parent(community))
    super_cop_roles = cop_context.get_local_roles()
    for user_id, super_roles in super_cop_roles:
        roles = [role for role in super_roles if role in roles_list]
        if roles:
            local_roles = community.get_local_roles_for_userid(user_id)
            if local_roles:
                roles = list(set(list(local_roles) + roles))
            community.manage_setLocalRoles(user_id, roles)
            role_cache = [role for role in roles if role in roles_to_cache]
            if role_cache:
                updateCommunityCache(community, user_id, role_cache[0])
            name, email = getMemberData(community, user_id)
            notify(CoPLocalRolesChangedEvent(community, user_id, roles[0]))
    community.reindexObjectSecurity()


def hasSuperRole(community, user_id):
    """ Recebe objeto CoP e id do usuario.
        Retorna True se usuario e' moderador ou
        observador da CoP do nivel superior.
    """
    super_cop = getSuperCoP(community)
    if super_cop:
        local_roles = super_cop.get_local_roles_for_userid(user_id)
        if ("Moderador" in local_roles or
                "Owner" in local_roles or
                "Observador" in local_roles or
                "Bloqueado" in local_roles):
            return True
    return False


def getSubCoPDepth(community):
    """ Recebe objeto CoP. Retorna nivel na hierarquia de comunidades.
        Retorna 0 para CoP e maior que 0 para SubCoP.
    """
    depth = -1
    parents = aq_chain(aq_inner(community))
    for parent in parents:
        portal_type = getattr(parent, 'portal_type', '')
        if portal_type == 'CoP':
            depth += 1
    return depth


def getSuperCoP(community):
    """ Recebe SubCoP e retorna CoP do nivel superior.
        Retorna False se nao existir CoP acima.
    """
    super_cop = aq_parent(aq_parent(aq_inner(community)))
    if super_cop.portal_type == "CoP":
        return super_cop
    return False


def getRootCoP(community):
    """ Retorna primeira CoP da hierarquia
    """
    for parent in aq_chain(aq_inner(community))[::-1]:
        if hasattr(parent, 'portal_type'):
            if parent.portal_type == 'CoP':
                return parent
    return False


def deleteCoPShareUser(community, user_id):
    """ Deleta todos os CoPShare do user_id na community
    """
    portal_catalog = getToolByName(community, "portal_catalog")
    query = {}
    query['portal_type'] = 'CoPShare'
    query['Creator'] = user_id
    query['path'] = '/'.join(community.getPhysicalPath())
    copshares = portal_catalog(query)
    for copshare in copshares:
        obj = copshare.getObject()
        parent = aq_parent(aq_inner(obj))
        parent.manage_delObjects([obj.getId()])
    return False


def addParticipantsInBlock(community, user_list):
    """ Recebe comunidade e lista de usuários.
        Adiciona participantes nas comunidades em cascada.
        Do nível mais baixo para o mais alto.
    """
    permissao = 'Participante'
    object_list = aq_chain(community)
    for obj in object_list:
        if getattr(obj, 'portal_type', '') == 'CoP':
            setMembersCoPRole(obj, user_list, permissao)
    root_cop = getRootCoP(community)
    portal_groups = getToolByName(community, 'portal_groups')
    group_id = root_cop.UID()
    if group_id in portal_groups.getGroupIds():
        for user in user_list:
            portal_groups.addPrincipalToGroup(user, group_id)


def generateContentId(context, string):
    """ Normaliza string e verifica se e' valida para o contexto.
        Caso nao seja incrementa numeracao ao final da string.
    """
    normalizer = getUtility(IIDNormalizer)
    new_id = normalizer.normalize(string)

    v_id = 2
    while new_id in context.objectIds():
        new_id = normalizer.normalize(string + str(v_id))
        v_id += 1
    return new_id


def canDeleteCoPFolder(copfolder):
    """Verifica se a CoPFolder pode ser deletada pelo usuario logado
    """
    portal_membership = getToolByName(copfolder, "portal_membership")
    authenticated_member = portal_membership.getAuthenticatedMember()
    authorized = ['Moderador', 'Owner', 'Manager', 'Moderador_Master']
    roles = authenticated_member.getRolesInContext(copfolder)
    return bool([i for i in roles if i in authorized])


def exportCSVDownload(request, header, data, filename):
    """ Exporta em CSV e faz download na maquina local, com um cabecalho
        'header', uma lista de dados 'data' com nome de arquivo 'filename'
    """
    writer = csv.writer(request.response)
    request.response.setHeader('Content-Type', 'text/csv', 'charset=UTF-8')
    request.response.setHeader(
        'Content-Disposition', 'attachment; filename="%s"' % filename,
    )
    writer.writerow(header)
    for row in data:
        writer.writerow(row)
    return False


def createCoPMenuPost(cop):
    """Create CoPMenuPost into CoP."""
    posts = _createObjectByType(
        'CoPMenu',
        cop,
        id='posts',
        )
    posts.setTitle(u'Posts')
    posts.setDescription(
        u'Aqui estão armazenados todos os posts desta Comunidade.'
    )
    posts.setTitlemenu('posts')
    posts.unmarkCreationFlag()
    allowTypes(posts, ['CoPPost'])
    cop_owner = cop.getOwner()
    cop_owner_id = cop_owner.getId()
    portal_membership = getToolByName(cop, "portal_membership")
    authenticated_id = portal_membership.getAuthenticatedMember().getId()
    if authenticated_id != cop_owner_id:
        # change creator
        posts.setCreators(cop_owner_id)
        # change owner
        posts.changeOwnership(cop_owner, recursive=False)
        roles = list(posts.get_local_roles_for_userid(cop_owner_id))
        if 'Owner' not in roles:
            roles.append('Owner')
        posts.manage_delLocalRoles([authenticated_id])
        posts.manage_setLocalRoles(cop_owner_id, roles)

    posts.reindexObject()
    posts.reindexObjectSecurity()


def canCutObjects(context, ids=None, REQUEST=None):
    """ Verifica se os objetos com ids podem ser excluidos pelo
        usuario logado no context
    """
    # Se nao for container da CoP, retorna True para manter o
    # padrao do Plone
    if context.portal_type not in ["CoPMenu", "CoPFolder"]:
        return True
    if type(ids) is str:
        ids = [ids]
    portal_membership = getToolByName(context, "portal_membership")
    checkPermission = portal_membership.checkPermission
    for id in ids:
        cut_object = context._getOb(id, None)
        # se o objeto nao existe, retorna True e deixa o Plone se virar
        if not cut_object:
            return True
        # se nao tiver permissao de Modify no conteudo recortado,
        # retorna False para impedir a acao
        if not checkPermission('Modify portal content', cut_object):
            return False
    return True


def getMMGroup(context):
    """Retorna o grupo de Moderador_Master de um context
    """
    mm_group = None
    portal_groups = getToolByName(context, 'portal_groups')
    community_folder = getCoPContext(context, ["CoPCommunityFolder"])
    if community_folder:
        mm_group = portal_groups.getGroupById("MM%s" % community_folder.UID())
    return mm_group


def getMMGroupMembers(context):
    """Retorna os membros do grupo de Moderador_Master
       de um context
    """
    members = []
    mm_group = getMMGroup(context)
    if mm_group:
        members = mm_group.getGroupMemberIds()
    return members


def getOMGroup(context):
    """Retorna o grupo de Observador_Master de um context
    """
    om_group = None
    portal_groups = getToolByName(context, 'portal_groups')
    community_folder = getCoPContext(context, ["CoPCommunityFolder"])
    if community_folder:
        om_group = portal_groups.getGroupById("OM%s" % community_folder.UID())
    return om_group


def getOMGroupMembers(context):
    """Retorna os membros do grupo de Observador_Master
       de um context
    """
    members = []
    om_group = getOMGroup(context)
    if om_group:
        members = om_group.getGroupMemberIds()
    return members


def getCoPSettings():
    """Retorna as configuracoes do control panel da CoPPla
    """
    from communities.practice.interfaces import ICoPSettings
    registry = getUtility(IRegistry)
    settings = registry.forInterface(ICoPSettings, check=False)
    return settings


def deleteEventCheck(obj):
    """ Returns True if link integrity check
        has already been done
    """
    from zope.component import queryUtility
    from Products.CMFCore.interfaces import IPropertiesTool

    # We need to disable link integrity check,
    # because it cannot handle several delete calls in
    # one request
    deleted = False
    ptool = queryUtility(IPropertiesTool)
    props = getattr(ptool, 'site_properties', None)
    old_check = props.getProperty('enable_link_integrity_checks', False)
    link_integrity_events_counter = getattr(
        obj.REQUEST,
        'link_integrity_events_counter',
        1,
    )
    return link_integrity_events_counter > 1 or not old_check
