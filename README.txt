==========================================
CoPPLA - Communities of Practice Platform
==========================================

The CoPPLA product provides a generic platform for building virtual communities of practice for social learning applications through a set of communication and collaboration tools integrated into a common environment for sharing knowledge.

This platform provides management features of communities, from its creation to maintain the content and participants, and visualization in a friendly and intuitive layout. The collaborative toolkit delivered with the platform is configurable and involves the manipulation of collections of all types of documents (texts, videos, images, web pages, links), events schedule and agenda, discussion forums, collaborative spaces for exchanging experiences (Portfolio), collaborative spaces for learning experiences (Task Space) and communication tools.

Users have the ability to create and manage their communities as a space for sharing knowledge involving learning activities. The product can be installed on any Plone portal with the authentication features already included. A set of workflows define access permissions through roles assigned to members of the community.

The platform makes the content creation and the manipulation of objects more flexible and intuitive. For security, access to objects occur through control of permissions defined by the type of community access (private, restricted and public).

Features
^^^^^^^^

The CoPPLA platform consist of a communities.practice product, a modified Ploneboard [1] product and a set of community creation and configuration features. The CoPPLA product offers a pull of tools in order to make interaction and colaboration more usefull:

Collection: space for a pull or library of community documents. Here is permitted to add Imagens, to upload Files, to create text documents, to add folders and URL external links;

Calendar: organize date events in the community;

Portfolio: space for personal production and repository of individual Members' documents. The repository can be organizing through folders and accept objects of file, Image and document types. At first level the user must create a Portfolio folder using the user's name.

Forum: space of communty discussion in order to hold conversations (topics) of the member's interest. It is a modified Ploneboard product with workflow, view and config modifications in order to cope with our CoPPLA features; 

Task Space: it is a repository of member's outcomes and completed works. It is also a space for collaborative tasks such as polls, quizzes an collaborative projects;

Activity Monitoring: CoPPLA platform offers a very simple way for community activity monitoring, showing posts/objects created and modified all over the community, listing by member activity. It also shows the list of activities of each member in the community;

Notifications: this feature offers two way of member notification. There is a direct message feature from community creator or moderators to all community members. CoPPLA also offers a digest-mail notification of all activity in the community in a daily or weekly basis.

Settings: community members can update some personal settings such as photo, daily or weekly digest notification and other information.

[1] http://plone.org/products/ploneboard


Initial contributors
^^^^^^^^^^^^^^^^^^^^
- Luciano Camargo Cruz    <luciano@lccruz.net>
- Alexandre Ribeiro       <alexandremorettoribeiro@gmail.com>
- Joao Luis Tavares       <joaoluis.tavares@gmail.com>

Current contributors
^^^^^^^^^^^^^^^^^^^^
- Luciano Camargo Cruz    <luciano@lccruz.net>
- Alexandre Ribeiro       <alexandremorettoribeiro@gmail.com>
- Joao Luis Tavares       <joaoluis.tavares@gmail.com>
- Joao Toss Molon         <jtmolon@gmail.com>
- Matheus Pereira          <matheper@gmail.com>
- Felipi Medeiros Macedo  <felipimedeirosmacedo@gmail.com>


Repository
^^^^^^^^^^

- Code repository: https://bitbucket.org/communitas/communities.practice


